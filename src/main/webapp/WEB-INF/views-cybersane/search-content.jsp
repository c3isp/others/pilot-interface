<script src="${pageContext.request.contextPath}/uis/cybersane/js/search.js"
	type="text/javascript"></script>

<div id="search-page" class="main-content mt-3">
	<a id="content-header" href="#"
		class="btn btn-sm animated-button thar-four">DATA MANAGER</a>

	<div id="search-card" class="row justify-content-center m-0">
		<div class="card mt-5 w-75 shadow p-3 mb-5 rounded card-style">
			<div class="card-header row no-gutters">
				<div class="col-sm"></div>
				<div class="col-sm text-center">Search</div>
				<div class="col-sm"></div>
			</div>

			<div class="card-body">
				<hr>

				<form id="data-manager-search-form">
					<%@include file="search-form.jsp"%>

					<div class="row mt-4 justify-content-center">
						<div class="col-3"></div>
						<div class="col-3 text-center">
							<button id="submit" type="submit" class="file-upload-btn">Submit</button>
						</div>
						<div class="col-3"></div>
					</div>

				</form>
			</div>

			<div id="search-error"
				class="row mt-5 justify-content-center alert alert-danger"
				style="display: none" role="alert"></div>
			<div id="search-info"
				class="row mt-5 justify-content-center alert alert-info"
				style="display: none" role="alert"></div>
			<div id="search-success"
				class="row mt-5 justify-content-center alert alert-success"
				style="display: none" role="alert"></div>
			<div id="search-warning"
				class="row mt-5 justify-content-center alert alert-warning"
				style="display: none" role="alert"></div>

		</div>
	</div>


	<div id="table-div" class="row justify-content-center m-0"
		style="display: none;">
		<div class="card mt-5 w-75 shadow p-3 mb-5 rounded card-style">
			<div class="card-header row no-gutters">
				<div class="col-sm"></div>
				<div class="col-sm text-center">Results</div>
				<div class="col-sm"></div>
			</div>
			<div class="card-body">
				<div class=col-2></div>
				<div class="col">
					<table id="search-table" class="table shadow"></table>
				</div>
				<div class=col-2></div>

				<div id="new-search" class="row justify-content-center m-0 mt-5"
					style="display: none;">
					<div class="col-sm-3"></div>
					<div class="col-sm-3">
						<button id="new-search-button" class="file-upload-btn">New
							Search</button>
					</div>
					<div class="col-sm-3"></div>
				</div>
	
				<div id="searchResult-error" class="row mt-5 justify-content-center alert alert-danger"	style="display: none" role="alert"></div>
				<div id="searchResult-info" class="row mt-5 justify-content-center alert alert-info" style="display: none" role="alert"></div>
				<div id="searchResult-success" class="row mt-5 justify-content-center alert alert-success" style="display: none" role="alert"></div>
				<div id="searchResult-warning" class="row mt-5 justify-content-center alert alert-warning" style="display: none" role="alert"></div>
	
				<div id="panel" style="display: none;"></div>
			</div>
		</div>
	</div>
</div>



