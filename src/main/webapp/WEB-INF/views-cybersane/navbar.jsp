<nav class="navbar navbar-expand-lg fixed-top">
	<div class="d-flex justify-content-start">
		<a class="navbar-brand ml-3" href="${pageContext.request.contextPath}/home"> 
		<img src="${pageContext.request.contextPath}/uis/cybersane/images/logo_medium.png" class="img-fluid img-thumbnail" alt="C3ISP logo" height="150" width="150">
		</a>
	</div>
	
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon color-white"></span>
		</button>

	<div class="collapse navbar-collapse" id="navbarText">
		<div class="d-flex ml-auto">
			<i id="profile" class="fa fa-user color-white fa-2x pointer" data-toggle="popover"></i>
			<i class="fa fa-envelope color-white fa-2x ml-3 pointer" ></i>
			<i class="fas fa-sign-in-alt color-white fa-2x ml-3 mr-3 pointer" onclick="redirectTo('/logout')"></i>
		</div>
	</div>
</nav>
