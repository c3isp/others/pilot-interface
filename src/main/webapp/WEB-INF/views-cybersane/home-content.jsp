<div id="search-page" class="main-content mt-3">
<a id="content-header" href="#" class="btn btn-sm animated-button thar-four">HOMEPAGE</a>
<div id="search-card" class="row justify-content-center m-0" >
<div class="card mt-5 w-75 shadow p-3 mb-5 rounded card-style">
<div class="card-header row no-gutters">
<div class="col"></div>
<div class="col text-center">
C3ISP PORTAL FOR SHARENET
</div>
<div class="col"></div>
</div>
<div class="card-body">
<div class="content col text-center">
<h3> To start using the ShareNet system choose one action from the left side menu</h3>
</div>
</div>
</div>
</div>

<div id="search-card" class="row justify-content-center m-0" >
<div class="card mt-5 w-75 shadow p-3 mb-5 rounded card-style">

<div class="card-body">

<section>

<h1 class="page-title">About ShareNet</h1>
<div class="content">
<p><strong><a href="https://www.cybersane-project.eu/system/sharenet/" style="color:#1EC8BB">ShareNet</a></strong> provides threat intelligence and information sharing capabilities within the CIIs and other involved parties such as industry cooperation groups and Computer Security Incident Response Teams - CSIRTs. Within the  <strong><a href="https://www.cybersane-project.eu/" style="color:#1EC8BB">CyberSANE</a></strong>  system, it is one of the main core elements for the proper identification of new attack patterns from the open web and for sharing this data both at internal and external level considering the compliance of data-sharing agreements required to be properly enforced.</p>
</div>
<p align="center"><img src="${pageContext.request.contextPath}/images/ShareNetDesc.png" class="img-fluid img-thumbnail" alt="ShareNet" height="1215" width="2503"></iframe></p>

<div id="content-wrap">
<h1 class="page-title">About CyberSANE</h1><div class="region region-content">
<div id="block-system-main" class="block block-system">



<div class="content">

<div class="content">
<div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" >
<p><strong><a href="https://www.cybersane-project.eu/" style="color:#1EC8BB">CyberSANE</a></strong> enhances the security and resilience of Critical Information Infrastructures (CIIs) by providing a dynamic collaborative, warning and response system supporting and guiding security officers and operators to recognise, identify, dynamically analyse, forecast, treat and respond to Advanced Persistent Threats (APTs) and handle their daily cyber incidents using structured and unstructured data such as logs, network traffic, or data coming from social networks.</p>

<!-- <p align="center"><iframe width="560" height="315" src="https://www.youtube.com/watch?v=MNFytOoq_2Q" ></iframe></p> -->

<p><strong>CyberSANE</strong> introduces a holistic and privacy-aware approach in handling security incidents, addressing the complexity of these nets consisting of cyber assets hosted in cross-border, heterogeneous CIIs characterised by:

<ul>
  <li>Complex, highly distributed, and large-scale cyber systems, including the Internet of Things (IoT) and cyber-physical with respect to the number of entities involved</li>
  <li>Heterogeneity of the underlying networks interconnecting the physical-cyber systems</li>
  <li>Different levels of exposure to attacks</li>
</ul>
</p>

<p><strong>CyberSANE</strong> components rely on existing systems and tools including security monitoring sensors, web mining and intelligence solution, security information and event management approaches developed by project's partners that provide easy reconfigurable, flexible, adapted and modular environments which can be used as the basis to implement and integrate the main interrelated-elements of <strong>CyberSANE</strong> system.</p><p>&nbsp;</p>


<!-- <h1 class="page-title">About ShareNet</h1>
<div class="content">
<p><strong>ShareNet</strong> provides threat intelligence and information sharing capabilities within the CIIs and other involved parties such as industry cooperation groups and Computer Security Incident Response Teams - CSIRTs. Within the <strong>CyberSANE</strong> system, it is one of the main core elements for the proper identification of new attack patterns from the open web and for sharing this data both at internal and external level considering the compliance of data-sharing agreements required to be properly enforced.</p>
</div>
<p align="center"><img src="${pageContext.request.contextPath}/images/ShareNetDesc.png" class="img-fluid img-thumbnail" alt="ShareNet" height="1215" width="2503"></iframe></p> -->
<!-- <p align="center"><iframe width="560" height="315" src="https://www.youtube.com/embed/DnplPKZ-rnA" ></iframe></p> -->
</div></div></div></div>

</div>

</div> <!-- /.block -->
</div>
 <!-- /.region -->
</div>
</section> <!-- /#main -->
</div>

<aside id="sidebar" role="complementary">
 <div class="region region-sidebar-first">
<div id="block-block-9" class="block block-block">


</div> <!-- /.block -->
</div>
 <!-- /.region -->
</aside> 

 
<div id="block-block-4" class="block block-block">

<!-- <div class="content">
<p><strong>ShareNet</strong> provides threat intelligence and information sharing capabilities within the CIIs and other involved parties such as industry cooperation groups and Computer Security Incident Response Teams - CSIRTs. Within the <strong>CyberSANE</strong> system, it is one of the main core elements for the proper identification of new attack patterns from the open web and for sharing this data both at internal and external level considering the compliance of data-sharing agreements required to be properly enforced.</p>
</div>

<div class="content">
<p><strong>Knowledge Sharing</strong>; Implements all required functionalities for sharing the data such as enforce attribute-based authorization, data enforce obligations, data manipulation operation, among others.</p>
</div>


<div class="content">
<p><strong>Attack-Pattern Collection</strong>; Used as one of the main points for identifying new attack patterns from the internet, which can be properly registered in the <strong>CyberSANE</strong> platform through LiveNet component in order to allow real-time security incident detection.</p>
</div>

<div class="content">
<p><strong>Data Sharing Agreements</strong>; Allows the creation and maintenance of agreements between two or more entities, which can be either internal or external to <strong>CyberSANE</strong>, concerning the sharing of data or information to enable proper description and enforcement of all the mandatory rules, terms and conditions set for and agreed upon.</p>
</div>


<div class="content">
<p><strong>Protected Data Storage</strong>; Stores security incident data in a protected and secure way, ensuring its confidentiality and integrity at all implemented and supported <strong>CyberSANE</strong> scenarios.</p>
</div> -->
 


</div>
</div>
</div>
</div>



<!-- <div class="row m-3" id="canvas-container"> -->
<!-- <div class="col-8" id="canvas-wrapper" style="padding: 0; border: 1px solid #3b4a5b; border-radius: 5px 0 0 5px;"> -->
<!-- <svg id="canvas"></svg> -->
<!-- </div> -->
<!-- <div id="legend" class="col-4 d-flex flex-column sidebar" style="border: 1px solid #3b4a5b; border-left: 0px; border-radius: 0 5px 5px 0;"> -->
<!-- <h2>Legend</h2> -->
<!-- <ul id="legend-content"></ul> -->
<!-- </div> -->
<!-- </div> -->
<!-- <div class="row m-3"> -->
<!-- <div id="selected" class="col-12 sidebar" style="border: 1px solid #3b4a5b; border-radius: 5px;"> -->
<!-- <h2>Selected Node</h2> -->
<!-- <div id="selection"> -->
<!-- </div> -->
<!-- </div> -->
<!-- </div> -->

<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/d3/3.5.17/d3.min.js" charset="utf-8"></script> -->

<!-- <script src="js/cti-stix-visualization-master/stix2viz/stix2viz/stix2viz.js"></script> -->
<!-- <script src="js/dashboard.js"></script> -->


