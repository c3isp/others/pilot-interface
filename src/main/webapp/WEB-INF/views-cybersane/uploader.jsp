<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="it.cnr.iit.certuploader.proxy.DSAProxy"%>
<%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.js" type="text/javascript"></script>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-teal.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<script type="text/javascript" src="scripts/spam_utils.js"></script>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Loading Functions</title>

</head>
<body>
	<%
	  String searchString = "{ \"combining_rule\":"
	      + "\"and\",\"criteria\": [{ \"attribute\": \"status\", \"operator\":"
	      + " \"eq\", \"value\": \"AVAILABLE\"},{\"attribute\":"
	      + " \"partyNames\", \"operator\": \"in\", \"value\": \"CNR\"}]}";
	  //load list of DSA IDs from DSA Manager
	  DSAProxy dsaProxy = new DSAProxy(
	      "/dsa-store-api/v1", "user",
	      "password");
	  List<String> dsaList = dsaProxy.searchDSA(searchString);
	%>


	<hr>
	<div class="w3-center">
	<h2>Mail Upload Form</h2>
	</div>
	<div class="w3-row-padding">
	
	<h4>Metadata</h4>
	<p>Please insert metadata about the threat you want to share</p>
	<input class="w3-input" type="text" name="event_type" id="event_type" size="20" 
	placeholder="Event type e.g. spam, dga"/> 
	<label>Event type</label>
	<br />
	
	<input class="w3-input" type="text" name="organization" id="organization" size="20" 
	placeholder="Organization e.g. CNR, CERT, Registro..."/> 
	<label>Organization</label>
	<br />
	
	<input class="w3-input" type="text" name="start_time" id="start_time" size="40" 
	placeholder="Start time of the event"/> 
	<label>Start time</label>
	<br />
	
	<input class="w3-input" type="text" name="end_time" id="end_time" size="40" 
	placeholder="End time of the event"/> 
	<label>End time</label>
	<br />

		<hr>
		<div class="w3-center">
			<h4>Available DSA List</h4>
			<div>

				<%
				  if (dsaList == null || dsaList.size() == 0) {
					System.out.println("NO DSA");
				  } else {
				%>
				<select class="w3-container w3-theme">
					<%
					  for (String dsa : dsaList) {
					%>
					<option value="<%=dsa%>"><%=dsa%></option>
					<%
					  }
					%>
					<%
					  }
					%>
				</select>
				<%
				  
				%>

			</div>
		</div>
	</div>



	<div class="w3-row-padding">

		<div class="w3-center">

			<form class="w3-container w3-card-4"
				id="file-search" method="post" id="file-search" name="fileSearch"
				enctype="multipart/form-data">
				<h4>Mail Form</h4>
				<div>
					<div style="display: inline-block">
						<label for="file-path">Select mails to be loaded into
							the Portal <br />
						</label> <input class="w3-input" type="text" name="file-path" size="60"
							autocomplete="off"
							placeholder="max 5 MB"
							style="cursor: pointer" disabled required /> <label> <input
							type="file" class="w3-input" name="file" id="uploadFile"
							multiple />
						</label>
					</div>

					<div id="submit-data-div">
						<input type="submit" class="w3-btn w3-theme"
							id="submit-data-button" name="op" 
							value="Load" class="form-submit" />
					</div>
				</div>
			</form>
		</div>
	</div>

</body>
</html>
