<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="${pageContext.request.contextPath}/uis/cybersane/js/streaming.js" type="text/javascript"></script>

<div id="streaming-page" class="main-content mt-3 mb-6">
	<a id="content-header" href="#" class="btn btn-sm animated-button thar-four">STREAMING</a>
	
	<div class="row justify-content-center m-0 mb-2">
		<div class="card mt-5 w-75 shadow p-3 mb-5 rounded card-style">
			<div class="card-header text-center ">
	    		Streaming
			</div>
		    <div class="card-body">
				<form id="streaming-form">
			    	<div class="form-row ">
						<label class="my-1 col-sm-2 col-form-label" for="folder-path">Folder:<sup class="required-field">*</sup></label>
			    		<div class="col-sm-4">
			    			<input id="folder-path" name="folder-path" class="form-control" type="text" value="" placeholder="/tmp/log/e-corridor" required>
			    		</div>
			    		<small class="col-sm form-text text-muted">Choose the folder you want to monitor.</small>
			    	</div>
			    	<div class="form-row ">
						<label class="my-1 col-sm-2 col-form-label" for="time-interval">Time interval:</label>
			    		<div class="col-sm-4">
			    			<input id="time-interval" name="time-interval" class="form-control" type="number" value="" placeholder="60">
			    		</div>
			    		<small class="col-sm form-text text-muted">If new file are present, the creation of a new Dpo will be done every number of minutes specified by this parameter.</small>
			    	</div>
			    	
			    	<c:if test="${not empty dsaList}">
  					  <jsp:include page="dsa-selection.jsp">
  					    <jsp:param name="dsaList" value="${dsaList}" />
  					  </jsp:include>
  					</c:if>
			    	
  					<div id="streaming-submit-div" class="row mt-5 justify-content-center">
  						<div class="col-3">
		  					<button type="submit" id="streaming-form-submit" class="file-upload-btn">Save</button>
  						</div>
  					</div>
  					
			    	<c:choose>
  					  <c:when test="${not empty error}">
  						<div id="streaming-error" class="row mt-5 justify-content-center alert alert-danger" role="alert">${error}</div>
  						<script>$(".form-row").hide();
  								$("hr").hide();
  								$("#streaming-submit-div").hide();
  						</script>	
  					  </c:when>
  					  <c:otherwise>
  					  	<div id="streaming-error" class="row mt-5 justify-content-center alert alert-danger" style="display: none" role="alert"></div>
  					  </c:otherwise>
  					</c:choose>	
  					
			    	<div id="streaming-error" class="row mt-5 justify-content-center alert alert-danger" style="display: none" role="alert"></div>
  					<div id="streaming-info"  class="row mt-5 justify-content-center alert alert-info" style="display: none" role="alert"></div>
  					<div id="streaming-success"  class="row mt-5 justify-content-center alert alert-success" style="display: none" role="alert"></div>
  					
			    	
			    </form>
		    </div>
		</div>
	</div>
</div>
