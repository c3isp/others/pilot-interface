<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<%@page import="it.cnr.iit.certuploader.proxy.DSAProxy"%>
<%@page import="java.util.*"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head lang="en">

<%@include file="header.jsp" %>

</head>
<body id="page-top">

  <div class="container-fluid page-container">
    
  

      <div class="row">
        <div class="col text-center mt-5">
          <img src="${pageContext.request.contextPath}/uis/ecorridor/images/logo_medium.png" alt="Application logo" height="200">
        </div>
      </div>

      <div class="row mt-5">
        <div class="col-3"></div>
        <div class="col text-center">
          
          <h1 style="font-weight: bolder;">Authorization error!</h1>
          <div class="alert-danger" style="padding: 5px; border: 1px solid #a00; border-radius: 5px;">
            
            <h4>You are not allowed to download the DPO.</h4>
          </div>
        </div>
        <div class="col-3"></div>
      </div>

      <div class="row mt-5">
        <div class="col text-center">
          <a href="javascript:history.back()">Return to portal</a>
        </div>
      </div>
      
      <div class="footer">
        <%@include file="footer.jsp" %>    
      </div>
      
  </div>

</body>

</html>
