<div id="login-page" class="main-content mt-3" style="display: none;">

	<form id="login-form">
		<div class="row">
			<label class="col-sm col-form-label">Username:</label>
		</div>
		<div class="row">
			<div class="col-sm">
				<input type="text" id="login-username" name="username" class="form-control" required>
			</div>
		</div>
		<div class="row mt-3">
			<label class="col-sm col-form-label">Password:</label>
		</div>
		<div class="row">
			<div class="col-sm">
	    		<input type="password" id="login-password" name="password" class="form-control" required>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-sm mt-3">
	    		<p>You're not registered? Please <a id="registration" href="#" data-toggle="modal" data-target="#register-page">click here to register</a></p>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-sm mt-3">
	    		<button id="login-submit" class="login-submit" onclick="loginSubmit()">Submit</button>
			</div>
		</div>
	</form>
	
</div>