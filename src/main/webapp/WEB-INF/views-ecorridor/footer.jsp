<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- <div class="card text-center border-0"> -->
<!--   <div class="card-body dark-bg color-white"> -->
<!--   	<div class="row"> -->
<!--   		<div class="col-2"> -->
<%--   			<img class="footer-img" src="${pageContext.request.contextPath}/uis/ecorridor/images/logo-cnr.png" height="60px"> --%>
<!--   		</div> -->
<!--   		<div class="col"> -->
<!--   			<p class="card-text">This project is funded under the European Union's Horizon 2020 Programme (H2020) - Grant Agreement n. 700294</p> -->
<!--   		</div> -->
<!--   		<div class="col-2"> -->
<%--   			<img class="footer-img" src="${pageContext.request.contextPath}/uis/ecorridor/images/logo-iit.png" height="60px"> --%>
<!--   		</div> -->
<!--   	</div> -->
<!--   </div> -->
<!-- </div> -->

<script src="${pageContext.request.contextPath}/vendor/bootstrap/assets/js/vendor/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/uis/ecorridor/js/prototypes.js"></script>
<script src="${pageContext.request.contextPath}/uis/ecorridor/js/utils.js"></script>
<script src="${pageContext.request.contextPath}/uis/ecorridor/js/log_reg.js"></script>
<script src="${pageContext.request.contextPath}/uis/ecorridor/js/validate_form.js"></script>
<script src="${pageContext.request.contextPath}/uis/ecorridor/js/error.js"></script>
<script src="${pageContext.request.contextPath}/uis/ecorridor/js/animations.js"></script>
<script src="${pageContext.request.contextPath}/uis/ecorridor/js/ajax.js"></script>
<script src="${pageContext.request.contextPath}/uis/ecorridor/js/popover.js"></script>
<c:if test = "${page == 'upload-content' || page == 'search-content' || page == 'read-content' }">
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
</c:if>
