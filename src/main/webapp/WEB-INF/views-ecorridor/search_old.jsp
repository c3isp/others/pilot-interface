<!--  

<div id="search-page" class="main-content mt-3">
	<a id="content-header" href="#" class="btn btn-sm animated-button thar-four">DATA MANAGER</a>
	
	<div id="search-card" class="row justify-content-center m-0">
		<div class="card mt-5 w-50 shadow p-3 mb-5 rounded card-style">
			<div class="card-header row no-gutters">
				<div class="col"></div>
				<div class="col text-center">
		    		Search
				</div>
				<div class="col text-right">
		    		<i id="search-reduce" class="fas fa-arrow-right pointer" role="button" data-toggle="tooltip" data-placement="top" title="Reduce this card"></i>
				</div>
			</div>
			
		    <div class="card-body">
		    
		    	<hr>
		    	
		    	<form id="data-manager-search-form">
					<div class="form-group row mt-3">			
						<label for="colFormLabel" class="col-sm-3 col-form-label">File location<sup class="required-field">*</sup></label>
						<div class="col-sm-2">
							<div class="form-check">
							    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="remote" checked>
							    <label class="form-check-label" for="inlineRadio1">Remote</label>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-check">
							    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="local">
							    <label class="form-check-label" for="inlineRadio2">Local</label>
							</div>
						</div>
						<div class="col-sm">
						    <small class="text-muted">
						        Search your files locally or remotely.
						    </small>
    					</div>			
					</div>
					
					<hr>
					
					<div class="form-group row mt-4 mb-4">
  				    	<label for="colFormLabel" class="col-sm-3 col-form-label">Data Type<sup class="required-field">*</sup></label>
    					<div class="col-sm-3">
      						<select class="custom-select" id="inlineFormCustomSelect" aria-describedby="data-type-help" required>
						        <option selected>Choose...</option>
						        <option value="cnr">.cef</option>
						        <option value="cert">.txt</option>
						        <option value="registro">.log</option>
						    </select>
    					</div>
    					<div class="col-sm-1"></div>
    					<div class="col-sm">
						    <small id="data-type-help" class="text-muted">
						        Select the data type of files you want to retrieve.
						    </small>
    					</div>
  					</div>
  					
  					<hr>
  					
  					<div class="form-group row mt-4">
  						<label for="search-from" class="col-sm-3 col-form-label">Search from<sup class="required-field">*</sup></label>
  						<div class="col-sm-4">
						    <input type="text" id="search-from" class="form-control" aria-describedby="search-from-help" placeholder="hh:mm - dd/mm/yy" required>
  						</div>
  						<div class="col-sm">
						    <small id="search-from-help" class="text-muted">
						    	Select a time interval in which your files might be.
						    </small>
  						</div>
				    </div>
				    
				    <div class="form-group row mt-3">
  						<label for="search-from" class="col-sm-3 col-form-label">To<sup class="required-field">*</sup></label>
  						<div class="col-sm-4">
						    <input type="text" id="search-to" class="form-control" placeholder="hh:mm - dd/mm/yy" required>
  						</div>
				    </div>
  					
  					<div class="form-group row mt-3">
  						<label for="search-from" class="col-sm-3 col-form-label">Keyword</label>
  						<div class="col-sm-4">
						    <input type="text" id="search-keyword" class="form-control" aria-describedby="search-keyword-help" placeholder="Keyword">
  						</div>
  						<div class="col-sm">
						    <small id="search-keyword-help" class="text-muted">
						    	Select a proper keyword that describes the files.
						    </small>
  						</div>
				    </div>
				    
  					<div class="row mt-5 justify-content-center">
  						<div class="col-3">
		  					<button type="submit" class="file-upload-btn">Submit</button>
  						</div>
  					</div>
		    	</form>
		    </div>
		</div>
	</div>
	
	<div id="search-table" class="row justify-content-center m-0 mt-5" style="display: none;">
		<div class=col-2></div>
		<div class="col">
			<table class="table shadow">
			    <thead>
				    <tr>
					    <th scope="col">#</th>
					    <th scope="col">DPO-iD</th>
					    <th scope="col">Timestamp</th>
					    <th scope="col">Download</th>
				    </tr>
			    </thead>
			    <tbody>
				    <tr>
					    <td>1</td>
					    <td>Mark</td>
					    <td>Otto</td>
					    <td>@mdo</td>
				    </tr>
				    <tr>
					    <td>2</td>
					    <td>Jacob</td>
					    <td>Thornton</td>
					    <td>@fat</td>
				    </tr>
				    <tr>
					    <td>3</td>
					    <td>Larry</td>
					    <td>the Bird</td>
					    <td>@twitter</td>
				    </tr>
				    <tr>
					    <td>3</td>
					    <td>Larry</td>
					    <td>the Bird</td>
					    <td>@twitter</td>
				    </tr>
				    <tr>
					    <td>3</td>
					    <td>Larry</td>
					    <td>the Bird</td>
					    <td>@twitter</td>
				    </tr>
				    <tr>
					    <td>3</td>
					    <td>Larry</td>
					    <td>the Bird</td>
					    <td>@twitter</td>
				    </tr>
			    </tbody>
			</table>
		</div>
		<div class=col-2></div>
	</div>
	
	<div class="search-lateral-panel" style="display: none;">
		Search
	</div>
</div>

  


-->