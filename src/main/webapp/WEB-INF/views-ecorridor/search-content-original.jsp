<div id="search-page" class="main-content mt-3">
	<a id="content-header" href="#" class="btn btn-sm animated-button thar-four">DATA MANAGER</a>
	
	<div id="search-card" class="row justify-content-center m-0" >
		<div class="card mt-5 w-75 shadow p-3 mb-5 rounded card-style">
			<div class="card-header row no-gutters">
				<div class="col"></div>
				<div class="col text-center">
		    		Search
				</div>
				<div id="search-reduce" class="col text-right">
		    		<i class="fas fa-arrow-right pointer" role="button" data-toggle="tooltip" data-placement="top" title="Reduce this card"></i>
				</div>
			</div>
			
		    <div class="card-body">
		    
		    	<hr>
		    	
		    	<form id="data-manager-search-form">
					<div class="form-group row mt-3">			
						<label class="col-sm-3 col-form-label">File location<sup class="required-field">*</sup></label>
						<div class="col-sm-2">
							<div class="form-check">
							    <input class="form-check-input" type="radio" name="searchLocation" id="radio-remote" value="remote" checked>
							    <label class="form-check-label" for="radio-remote">Remote</label>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-check">
							    <input class="form-check-input" type="radio" name="searchLocation" id="radio-local" value="local">
							    <label class="form-check-label" for="radio-local">Local</label>
							</div>
						</div>
						<div class="col-sm">
						    <small class="text-muted">
						        Search your files locally or remotely.
						    </small>
    					</div>			
					</div>
					
					<hr>
					
					<div class="form-group row mt-4 mb-4">
  				    	<label class="col-sm-3 col-form-label">Data Type</label>
    					<div class="col-sm-3">
      						<select class="custom-select" id="search-data-type" name="searchDataType" aria-describedby="search-data-type-help">
						        <option value="">Choose...</option>
						        <option value="ssh">ssh</option>
						        <option value="dns">dns</option>
						        <option value="e-mail">e-mail</option>
						        <option value="apache">apache</option>
						        <option value="netflow">netflow</option>
						    </select>
    					</div>
    					<div class="col-sm-1"></div>
    					<div class="col-sm">
						    <small id="search-data-type-help" class="text-muted">
						        Select the data type of files you want to retrieve.
						    </small>
    					</div>
  					</div>
  					
  					<hr>
  					
  					<div id="search-plus-div" class="row mt-4 mb-4">
  						<div class="col-sm"></div>
  						<div class="col-sm-2" data-toggle="tooltip" data-placement="top" title="Add search attribute">
  							<button id="add-start-time" type="button" class="plus-buttons add-start-time" style="display:none;">Start Time</button>
  							<button id="add-end-time" type="button" class="plus-buttons add-end-time" style="display:none;">End Time</button>
  							<button id="add-params" type="button" class="round-button center">+</button>
  							<button id="add-organization" type="button" class="plus-buttons add-organization" style="display:none;">Organization</button>
  							<button id="add-security" type="button" class="plus-buttons add-security" style="display:none;">Security Level</button>
  							<button id="add-dpo" type="button" class="plus-buttons add-dpo" style="display:none;">DPO ID</button>
  						</div>
  						<div class="col-sm"></div>
  					</div>
  					
  					<!--  
  					<div class="row mt-4 mb-4">
					    <button type="button" class="col-sm file-upload-btn">Organization</button>
					    <button type="button" class="col-sm file-upload-btn ml-1">DPO ID</button>
					    <button type="button" class="col-sm file-upload-btn ml-1">Start Time</button>
					    <button type="button" class="col-sm file-upload-btn ml-1">End Time</button>
					    <button type="button" class="col-sm file-upload-btn ml-1">Security Level</button>
					</div>
  					-->
  					
  					<div class="form-group row mt-5 mb-5" id="search-organization-div" style="display: none;">
  						<label class="col-sm-2 col-form-label">Organization name is</label>
  						<div class="col-sm-2">
  							<select class="custom-select" id="search-organization-select" name="searchOrganizationCS" aria-describedby="search-organization-help">
						        <option value="">Choose...</option>
						        <option value="eq">equal</option>
						        <option value="ne">not equal</option>
						    </select>
  						</div>
  						<label class="col-sm-1 col-form-label text-center">to</label>
  						<div class="col-sm-3">
  							<input type="text" id="search-organization" name="searchOrganization" class="form-control" maxlength=256 placeholder="isp@cnr, registro.it, ...">
  						</div>
  						<div class="col-sm">
						    <small id="search-organization-help" class="text-muted">
						    	Specify a condition selector related to the organization name.
						    </small>
  						</div>
  						<div class="col-sm-1" data-toggle="tooltip" data-placement="top" title="Remove this attribute">
  							<i id="search-minus-button" class="fas fa-minus-circle minus-button"></i>
  						</div>
  					</div>
  					
  					<div class="form-group row mt-5 mb-5" id="search-dpo-div" style="display: none;">
  						<label for="search-dpo-id" class="col-sm-3 col-form-label">DPO-ID is equal to</label>
  						<div class="col-sm-3">
  							<input type="text" id="search-dpo-id" name="searchDpoId" class="form-control" maxlength=256 placeholder="AV1234, ...">
  						</div>  					
  						<div class="col-sm-1"></div>	
  						<div class="col-sm">
						    <small id="search-dpo-help" class="text-muted">
						    	Specify the DPO-ID.
						    </small>
  						</div>
  						<div class="col-sm-1" data-toggle="tooltip" data-placement="top" title="Remove this attribute">
  							<i class="fas fa-minus-circle minus-button"></i>
  						</div>
  					</div>
  					
  					
  					<div class="form-group row mt-5 mb-5" id="search-start-time-div" style="display: none;">
  						<label class="col-sm-2 col-form-label">With a start time</label>
  						<div class="col-sm-3">
  							<select class="custom-select" id="search-start-time-select" name="searchStartTimeCS">
						        <option value="">Choose...</option>
						        <option value="le">less than</option>
						        <option value="lte">less than equal to</option>
						        <option value="eq">equal to</option>
						        <option value="gte">greater than equal to</option>
						        <option value="gt">greater then</option>
						        <option value="ne">not equal to</option>
						    </select>
  						</div>
  						<div class="col-sm-3">
  							<input type="text" id="search-start-time" name="searchStartTime" class="form-control" aria-describedby="search-start-time-help" placeholder="yyyy-mm-ddThh:mm:00:0Z">
  						</div>
  						<div class="col-sm">
						    <small id="search-start-time-help" class="text-muted">
						    	Specify a condition selector related to the start time.
						    </small>
  						</div>
  						<div class="col-sm-1" data-toggle="tooltip" data-placement="top" title="Remove this attribute">
  							<i class="fas fa-minus-circle minus-button"></i>
  						</div>
  					</div>
  					
  					<div class="form-group row mt-5 mb-5" id="search-end-time-div" style="display: none;">
  						<label class="col-sm-2 col-form-label">With a end time</label>
  						<div class="col-sm-3">
  							<select class="custom-select" id="search-end-time-select" name="searchEndTimeCS">
						        <option value="">Choose...</option>
						        <option value="le">less than</option>
						        <option value="lte">less than equal to</option>
						        <option value="eq">equal to</option>
						        <option value="gte">greater than equal to</option>
						        <option value="gt">greater then</option>
						        <option value="ne">not equal to</option>
						    </select>
  						</div>
  						<div class="col-sm-3">
  							<input type="text" id="search-end-time" name="searchEndTime" class="form-control" aria-describedby="search-end-time-help" placeholder="yyyy-mm-ddThh:mm:00:0Z">
  						</div>
  						<div class="col-sm">
						    <small id="search-end-time-help" class="text-muted">
						    	Specify a condition selector related to the end time.
						    </small>
  						</div>
  						<div class="col-sm-1" data-toggle="tooltip" data-placement="top" title="Remove this attribute">
  							<i class="fas fa-minus-circle minus-button"></i>
  						</div>
  					</div>
  					
  					<div class="form-group row mt-5 mb-5" id="search-security-div" style="display: none;">
  						<label class="col-sm-2 col-form-label">Security level is</label>
  						<div class="col-sm-2">
  							<select class="custom-select" id="search-security-select" name="searchSecurityCS" aria-describedby="search-security-help">
						        <option value="">Choose...</option>
						        <option value="eq">equal</option>
						        <option value="ne">not equal</option>
						    </select>
  						</div>
  						<label class="col-sm-1 col-form-label text-center">to</label>
  						<div class="col-sm-2">
	  						<select class="custom-select" id="search-level-select" name="searchSecurityLevel" aria-describedby="search-security-help">
						        <option value="">Choose...</option>
						        <option value="INFORMATIONAL">Informational</option>
						        <option value="WARNING">Warning</option>
						        <option value="MINOR">Minor</option>
						        <option value="MAJOR">Major</option>
						        <option value="CRITICAL">Critical</option>
						    </select>
  						</div>
  						<div class="col-sm-1"></div>
  						<div class="col-sm">
						    <small id="search-security-help" class="text-muted">
						    	Specify a condition selector related to the organization name.
						    </small>
  						</div>
  						<div class="col-sm-1" data-toggle="tooltip" data-placement="top" title="Remove this attribute">
  							<i class="fas fa-minus-circle minus-button"></i>
  						</div>
  					</div>
  					
  					<hr>
				    
  					<div class="row mt-4 justify-content-center">  						
  						<div class="col-3">
		  					<button id="search-submit" type="submit" class="file-upload-btn">Submit</button>
  							<label for="search-submit"><small>Press to submit</small></label>
  						</div>
  						<div class="col-3">
		  					<button id="and-search-button" type="submit" class="file-upload-btn">AND</button>
		  					<label for="and-search-button"><small>Press to add a conjunctive search</small></label>
  						</div>
  						<div class="col-3">
		  					<button id="or-search-button" type="submit" class="file-upload-btn">OR</button>
		  					<label for="or-search-button"><small>Press to add a disjunctive search</small></label>
  						</div>
  					</div>
  					
  					<div id="search-error" class="row mt-3 justify-content-center alert alert-danger" style="display: none" role="alert"></div>
  					<div id="search-info"  class="row mt-3 justify-content-center alert alert-info" style="display: none" role="alert"></div>
  					<div id="search-success"  class="row mt-3 justify-content-center alert alert-success" style="display: none" role="alert"></div>
  					
		    	</form>
		    </div>
		</div>
	</div>
	
	<div id="search-table-div" class="row justify-content-center m-0 mt-5" style="display: none;">
		<div class=col-2></div>
		<div class="col">
			<table id="search-table" class="table shadow"></table>
		</div>
		<div class=col-2></div>
	</div>
	
	<div id="new-search" class="row justify-content-center m-0 mt-5" style="display: none;">
		<div class="col-sm-3"></div>
		<div class="col-sm-3">
			<button id="new-search-button" class="file-upload-btn">New Search</button>
		</div>
		<div class="col-sm-3"></div>
	</div>
	
	<div id="panel" style="display: none;"></div>
</div>

  


