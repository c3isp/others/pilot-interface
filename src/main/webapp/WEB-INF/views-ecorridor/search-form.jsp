<!-- <div class="form-group row mt-3">			 -->
<!-- 	<label class="col-sm-2 col-form-label">File location<sup class="required-field">*</sup></label> -->
<!-- 	<div class="col-sm-5"> -->
<!-- 		<input id="isi_type" type="checkbox" checked data-toggle="toggle" data-on="Local ISI" data-off="Centralized ISI" data-onstyle="success" data-offstyle="danger" required> -->
<!-- 	</div> -->
<!-- 	<div class="col-sm"> -->
<!-- 	    <small class="text-muted"> -->
<!-- 	        Search your files locally or remotely. -->
<!-- 	    </small> -->
<!-- 	</div>			 -->
<!-- </div> -->
	
<div class="form-group row mt-3">			
	<label class="col-sm-2 col-form-label">Search operator<sup class="required-field">*</sup></label>
	<div class="col-sm-2">
		<div class="form-check">
		    <input class="form-check-input" type="radio" name="combining_rule" id="radio-and" value="and">
		    <label class="form-check-label" for="radio-remote">AND</label>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-check">
		    <input class="form-check-input" type="radio" name="combining_rule" id="radio-or" value="or" checked>
		    <label class="form-check-label" for="radio-local">OR</label>
		</div>
	</div>
	<div class="col-sm">
	    <small class="text-muted">
	        Select the operator to put in AND or in OR all your search criteria.
	    </small>
	</div>			
</div>
	
<div class="form-group row mt-4 mb-4">
   	<label class="col-sm-2 col-form-label">Search criteria<sup class="required-field">*</sup></label>
		<div class="col-sm-3">
  			<select class="custom-select" id="search-param" name="search-criteria" aria-describedby="event-type-help" required>
	        <option value="">Choose...</option>
	        <option value="event_type">Event type</option>
	        <option value="organization">Organization name</option>
	        <option value="id">Dpo ID</option>
	        <option value="start_time">Start Time</option>
	        <option value="end_time">End Time</option>
	        <option value="security_level">Security Level</option>
	    </select>
		</div>
		<div class="col-sm-1"></div>
		<div class="col-sm-1 m-auto">
		<button id="add-search-param" type="button" class="round-button" style="width: 35px; height: 35px; margin-left: -10px;"><small >+</small></button>
	</div>
		<small class="col-sm text-muted m-auto">Click to add a new parameter. Please select at least a search criteria.</small>						        
</div>		
	
<div id="event-type-div" class="form-group row criterion" style="display: none;">
   	<label class="col-sm-2 col-form-label">Event Type<sup class="required-field">*</sup></label>
		<div class="col-sm-3">
  			<input type="text" id="event-type" name="value" attribute="event_type" aria-describedby="event-type-help" class="form-control" maxlength=256 placeholder="MAIL, dns, ..." autocomplete="on"/>
		</div>
		<div class="col-sm-2"></div>
		<div class="col-sm">
	    <small id="event-type-help" class="text-muted">
    	    Select the event type of files you want to retrieve.
    	</small>
		</div>
		<div class="col-sm-1" data-toggle="tooltip" data-placement="top" title="Remove this attribute">
		<i class="fas fa-minus-circle minus-button"></i>
	</div>
</div>
	
<div class="form-group row criterion" id="organization-div" style="display: none;">
	<label class="col-sm-2 col-form-label">Organization is<sup class="required-field">*</sup></label>
	<div class="col-sm-2">
		<select class="custom-select" id="organization-select" attribute="organization" name="operator" aria-describedby="organization-help">
	        <option value="">Choose...</option>
	        <option value="eq">equal</option>
	        <option value="ne">not equal</option>
	    </select>
	</div>
	<label class="col-sm-1 col-form-label text-center">to</label>
	<div class="col-sm-3">
		<input type="text" id="organization" name="value" attribute="organization" class="form-control" maxlength=256 placeholder="isp@cnr, registro.it, ...">
	</div>
	<div class="col-sm">
	    <small id="organization-help" class="text-muted">
	    	Specify a condition selector related to the organization name.
	    </small>
	</div>
	<div class="col-sm-1" data-toggle="tooltip" data-placement="top" title="Remove this attribute">
		<i id="minus-button" class="fas fa-minus-circle minus-button"></i>
	</div>
</div>
			
<div class="form-group row criterion" id="dpo-div" style="display: none;">
	<label for="dpo-id" class="col-sm-2 col-form-label">DPO-ID equal to<sup class="required-field">*</sup></label>
	<div class="col-sm-3">
		<input type="text" id="dpo" name="value" attribute="id" class="form-control" maxlength=256 placeholder="AV1234, ...">
	</div> 
	<div class="col-sm-2"></div>	
	<div class="col-sm">
	    <small id="dpo-help" class="text-muted">
	    	Specify the DPO-ID.
	    </small>
	</div>
	<div class="col-sm-1" data-toggle="tooltip" data-placement="top" title="Remove this attribute">
		<i class="fas fa-minus-circle minus-button"></i>
	</div>
</div>
			
<div class="form-group row criterion" id="start-time-div" style="display: none;">
	<label class="col-sm-2 col-form-label">With a start time<sup class="required-field">*</sup></label>
	<div class="col-sm-3">
		<select class="custom-select" id="start-time-select" attribute="start_time" name="operator">
 	       <option value="">Choose...</option>
	        <option value="le">less than</option>
	        <option value="lte">less than equal to</option>
	        <option value="eq">equal to</option>
	        <option value="gte">greater than equal to</option>
	        <option value="gt">greater then</option>
	        <option value="ne">not equal to</option>
	    </select>
	</div>
	<div class="col-sm-3">
		<input type="text" id="start-time" attribute="start_time" name="value" class="form-control" aria-describedby="search-start-time-help" placeholder="yyyy-mm-ddThh:mm:00.0Z">
	</div>
	<div class="col-sm">
	    <small id="start-time-help" class="text-muted">
	    	Specify a condition selector related to the start time.
	    </small>
	</div>
	<div class="col-sm-1" data-toggle="tooltip" data-placement="top" title="Remove this attribute">
		<i class="fas fa-minus-circle minus-button"></i>
	</div>
</div>
			
<div class="form-group row criterion" id="end-time-div" style="display: none;">
	<label class="col-sm-2 col-form-label">With an end time<sup class="required-field">*</sup></label>
	<div class="col-sm-3">
		<select class="custom-select" id="end-time-select" attribute="end_time" name="operator">
	        <option value="">Choose...</option>
	        <option value="le">less than</option>
	        <option value="lte">less than equal to</option>
	        <option value="eq">equal to</option>
	        <option value="gte">greater than equal to</option>
	        <option value="gt">greater then</option>
	        <option value="ne">not equal to</option>
	    </select>
	</div>
	<div class="col-sm-3">
		<input type="text" id="end-time" name="value" attribute="end_time" class="form-control" aria-describedby="end-time-help" placeholder="yyyy-mm-ddThh:mm:00.0Z">
	</div>
	<div class="col-sm">
	    <small id="end-time-help" class="text-muted">
	    	Specify a condition selector related to the end time.
	    </small>
	</div>
	<div class="col-sm-1" data-toggle="tooltip" data-placement="top" title="Remove this attribute">
		<i class="fas fa-minus-circle minus-button"></i>
	</div>
</div>
			
<div class="form-group row criterion" id="security-div" style="display: none;">
	<label class="col-sm-2 col-form-label">Security level is<sup class="required-field">*</sup></label>
	<div class="col-sm-2">
		<select class="custom-select" id="security-select" attribute="security_level" name="operator" aria-describedby="security-help">
	        <option value="">Choose...</option>
	        <option value="eq">equal</option>
	        <option value="ne">not equal</option>
	    </select>
	</div>
	<label class="col-sm-1 col-form-label text-center">to</label>
	<div class="col-sm-2">
		<select class="custom-select" id="level-select" attribute="security_level" name="value" aria-describedby="security-help">
	        <option value="">Choose...</option>
	        <option value="INFORMATIONAL">Informational</option>
	        <option value="WARNING">Warning</option>
	        <option value="MINOR">Minor</option>
	        <option value="MAJOR">Major</option>
	        <option value="CRITICAL">Critical</option>
	    </select>
	</div>
	<div class="col-sm-1"></div>
	<div class="col-sm">
	    <small id="security-help" class="text-muted">
	    	Specify a condition selector related to the organization name.
	    </small>
	</div>
	<div class="col-sm-1" data-toggle="tooltip" data-placement="top" title="Remove this attribute">
		<i class="fas fa-minus-circle minus-button"></i>
	</div>
</div>				