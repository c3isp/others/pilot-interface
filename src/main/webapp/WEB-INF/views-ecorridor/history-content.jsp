<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="${pageContext.request.contextPath}/uis/ecorridor/js/history.js" type="text/javascript"></script>

<div id="history-page" class="main-content mt-3 mb-6">
	<a id="content-header" href="#" class="btn btn-sm animated-button thar-four">HISTORY</a>
	
	<div class="row justify-content-center m-0 mb-2">
		<div class="card mt-5 w-75 shadow p-3 mb-5 rounded card-style">
			<div class="card-header text-center ">
	    		History
			</div>
		    <div class="card-body table-responsive">
		    	<p><b class="mr-2">Info :</b> this page shows the tickets and results of the analytics that the user has issued.</p>
		    	<hr>
		    	
		    	<div id="history-error" class="row mt-5 justify-content-center alert alert-danger" style="display: none" role="alert"></div>
				<div id="history-info"  class="row mt-5 justify-content-center alert alert-info" style="display: none" role="alert"></div>
				<div id="history-success"  class="row mt-5 justify-content-center alert alert-success" style="display: none" role="alert"></div>
				<div id="history-warning"  class="row mt-5 justify-content-center alert alert-warning" style="display: none" role="alert"></div>
		    	
		    	<table class="table striped">
		    		<thead>
		    			<tr>
		    				<th scope="col">#</th>
		    				<th scope="col">Analytic Name</th>
		    				<th scope="col">Type</th>
		    				<th scope="col">Status</th>
		    				<th scope="col">Actions</th>
		    			</tr>
		    		</thead>
		    		<tbody>
		    			<c:forEach items="${historyMap.entrySet()}" var="elem" varStatus="status">
						    <tr>      
						        <td>${elem.getKey().getId()}</td>
						        <td>${elem.getValue()}</td>
						        <td id="type-${status.index}" class="tableType font-weight-bold pointer" onclick='copyToClipboard("${elem.getKey().value}")' data-toggle="popover" data-placement="top" title="Click to copy: ${elem.getKey().value}">${elem.getKey().type}</td>
						        <td>${elem.getKey().status}</td>
						        <c:if test="${elem.getKey().type=='ticket'}">
						        	<td>
						        	    <c:if test="${(elem.getKey().status=='running') or (elem.getKey().status=='valid')}">
						        			<button class="btn btn-warning" onclick='sendTicket("${elem.getKey().value}")'>Get result</button>
						        		</c:if>
						        		<button id="delete-row-${status.index}" class="btn btn-danger" onclick='deleteRow("${elem.getKey().id}","${status.index}")'>Delete</button>
						        	</td>
						        </c:if>
						        <c:if test="${elem.getKey().type=='dpoId'}">
						        	<td>
						        		<a id="show-result-${status.index}" href="${pageContext.request.contextPath}/dashboard/${elem.getKey().id}/" class="btn btn-success" onclick="showLoader()">Show results</a>
						        		<button id="delete-row-${status.index}" class="btn btn-danger" onclick='deleteRow("${elem.getKey().id}","${status.index}")'>Delete</button>
						        	</td>
						        </c:if>
						    </tr>
						</c:forEach>
		    		</tbody>
		    	</table>
		    </div>
		    <small class="ml-2 mt-2 mb-2 text-right">History has ${historyTable.size()} items.</small>
		</div>
	</div>
</div>