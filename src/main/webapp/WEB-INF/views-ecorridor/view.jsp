<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="it.cnr.iit.certuploader.proxy.DSAProxy"%>
<%@page import="java.util.*"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head lang="en">

<%@include file="header.jsp" %>

</head>
<body id="page-top">

	<div class="container-fluid page-container">
		
    	<div class="main_sidebar">    
    		<%@include file="sidebar.jsp" %>
    	</div>
    	
		<div class="navbar">
		    <%@include file="navbar.jsp" %>		
		    <%@include file="login.jsp" %>
		    <%@include file="register.jsp" %>
		</div>
		
<!-- 		<div class="row"> -->
<!-- 			<div class="col-2 fixed p-0"> -->
				    	
<!-- 			</div> -->
			
<!-- 		</div> -->
	    
	    <div class="row mt-4"></div>
   		<div class="row mt-5">
   			<div class="col" style="margin-left: 12%;">
   				<div class="row">
			    	<div class="col border" style="height: 1500px">
			    		<jsp:include page="${page}.jsp"  flush="true"></jsp:include>
			    	</div>
   				</div>
   			</div>
   		</div>
	    
	    <div class="footer">
		    <%@include file="footer.jsp" %>    
	    </div>
	    
	</div>
    <%@include file="loader.jsp" %>

  </body>

</html>
