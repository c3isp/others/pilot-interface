<div id="search-page" class="main-content mt-3">
	<a id="content-header" href="#" class="btn btn-sm animated-button thar-four">HOMEPAGE</a>
	
	<div id="search-card" class="row justify-content-center m-0" >
		<div class="card mt-5 w-75 shadow p-3 mb-5 rounded card-style">
		<div class="card-header row no-gutters">
				<div class="col"></div>
				<div class="col text-center">
		    		E-CORRIDOR PORTAL
				</div>
				<div class="col"></div>
			</div>
		    <div class="card-body">
		    <div class="content col text-center">
	<h3> Choose one action from the left side menu to start using the Portal</h3>
	</div>
		    </div>
		    </div>
		    </div>
	
	<div id="search-card" class="row justify-content-center m-0" >
		<div class="card mt-5 w-75 shadow p-3 mb-5 rounded card-style">
			
			
		    <div class="card-body">
	<section>

        <div id="content-wrap">
                    <h1 class="page-title">About E-CORRIDOR</h1>                                                  <div class="region region-content">
  <div id="block-system-main" class="block block-system">
  <div class="content">

<div class="content">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" >
    <p><strong><a href="https://e-corridor.eu/" style="color:#207c91">E-CORRIDOR</a></strong> aims at providing a flexible, secure and privacy-aware
    framework allowing confidential, distributed and edge-enabled
    security services, as threat analysis and prevention as well as
    privacy-aware seamless access, in multimodal transport systems.</p><p>&nbsp;</p>
</div>
<div class="content">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" >
    <p><strong><a href="https://e-corridor.eu/" style="color:#207c91">E-CORRIDOR</a></strong>'s mission is to define a framework for multi-modal transport systems,
    which provides secure advanced services for passengers and transport operators.
    The framework includes collaborative privacy-aware edge-enabled information sharing, analysis and protection as a service.</p><p>&nbsp;</p>
</div>
<div class="content">
    <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" >
    <p>The framework allows data prosumers (producers/consumers) to easily express their preferences on how to share their data,
    which analytics operations can be performed on such data and by whom, with whom the resulting data can be shared etc.
    This entails a framework that combines several technologies for expressing and enforcing <strong style="color:#207c91">data sharing agreements</strong>
    as well technologies to perform data analytics operations in a way which is compliant to these agreements.
    Among these technologies we can mention data-centric policy enforcement mechanisms and
    data analysis operations directly performed on encrypted data provided by multiple prosumers.</p><p>&nbsp;</p>

</div>
</div>
</div>
  
  
  
<!-- <div class="row m-3" id="canvas-container"> -->
<!--   <div class="col-8" id="canvas-wrapper" style="padding: 0; border: 1px solid #3b4a5b; border-radius: 5px 0 0 5px;"> -->
<!--     <svg id="canvas"></svg> -->
<!--   </div> -->
<!--   	  <div id="legend" class="col-4 d-flex flex-column sidebar" style="border: 1px solid #3b4a5b; border-left: 0px; border-radius: 0 5px 5px 0;"> -->
<!-- 	    <h2>Legend</h2> -->
<!-- 	    <ul id="legend-content"></ul> -->
<!-- 	  </div> -->
<!-- </div> -->
<!-- <div class="row m-3"> -->
<!-- <div id="selected" class="col-12 sidebar" style="border: 1px solid #3b4a5b; border-radius: 5px;"> -->
<!-- 	    <h2>Selected Node</h2> -->
<!-- 	    <div id="selection"> -->
<!-- 	    </div> -->
<!-- 	  </div> -->
<!-- </div> -->

<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/d3/3.5.17/d3.min.js" charset="utf-8"></script> -->

<!-- <script src="js/cti-stix-visualization-master/stix2viz/stix2viz/stix2viz.js"></script> -->
<!-- <script src="js/dashboard.js"></script> -->


