<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="select-div" class="form-group row mt-3 mb-5 error-subject">
    <c:set var="dsaList" value="${requestScope[param.dsaList]}" scope="page"/>
		<label class="col-sm-3 col-form-label">DSA ID<sup
			class="required-field">*</sup></label>
		<div class="col-sm-5">
			<select class="custom-select mr-sm-2" id="dsa" name="dsa_id"
			aria-describedby="dsa-help" required>
				<option value="">Choose...</option>
				<c:forEach items="${dsaList}" var="dsa">
					<option value="${dsa}">${dsa}</option>
				</c:forEach>
			</select>
		</div>
		<div class="col-sm">
			<small id="dsa-help" class="text-muted"> Select the DSA ID. </small>
		</div>
</div>

