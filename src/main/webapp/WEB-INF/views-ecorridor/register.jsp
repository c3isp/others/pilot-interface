<div id="register-page" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">

	<div class="modal-dialog" role="document">
		<div class="modal-content">
  		
   			<div class="modal-header">
     			<h5 class="modal-title">REGISTRATION</h5>
      			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
        			<span aria-hidden="true">&times;</span>
      			</button>
   			</div>
   			
   			<div class="modal-body">
   			
   				<div class="row justify-content-center mb-3">
   					<img id="regLogo" src="${pageContext.request.contextPath}/uis/ecorridor/images/logo_medium.png" class="img-fluid img-thumbnail" alt="C3ISP logo" height="256"></img>
   				</div>
   				
   				<hr>
   				
      			<form id="register-form">
      				<div class="row mt-5">
						<label class="col-sm-3 col-form-label">Name:<sup class="required-field">*</sup></label>							
						<input type="text" id="register-name" name="name" class="form-control col-sm-5" required>
					</div>
					<div class="row mt-3">
						<label class="col-sm-3 col-form-label">Surname:<sup class="required-field">*</sup></label>							
						<input type="text" id="register-surname" name="surname" class="form-control col-sm-5" required>
					</div>
					<div class="row mt-3">
						<label class="col-sm-3 col-form-label">Username:<sup class="required-field">*</sup></label>							
						<input type="text" id="register-username" name="username" class="form-control col-sm-5" required>
					</div>
					<div class="row mt-3">
						<label class="col-sm-3 col-form-label">E-Mail:<sup class="required-field">*</sup></label>
						<input type="email" id="register-email" name="email" class="form-control col-sm-5" required>
					</div>
					<div class="row mt-3">
						<label class="col-sm-3 col-form-label">Password:<sup class="required-field">*</sup></label>
				    	<input type="password" id="register-password" name="password" class="form-control col-sm-5" required>
					</div>
					<div class="row mt-3">
						<label class="col-sm-3 col-form-label">Organization:<sup class="required-field">*</sup></label>
						<input type="text" id="register-organization" name="organization" class="form-control col-sm-5" required>
					</div>
					<div class="row mt-3">
						<label class="col-sm-3 col-form-label">Role:<sup class="required-field">*</sup></label>
						<input type="text" id="register-role" name="role" class="form-control col-sm-5" required>
					</div>
					<div class="row mt-3">
						<div class="col-sm mt-3"></div>
						<div class="col-sm mt-3">
				    		<button type="submit" id="register-submit" name="register-submit" class="login-submit">Submit</button>
						</div>
						<div class="col-sm mt-3"></div>
					</div>
				</form>
   			</div>
   			
   			<div class="modal-footer">
   				<div id="register-error" class="row mt-5 justify-content-center alert alert-danger" style="display: none" role="alert"></div>
  				<div id="register-info"  class="row mt-5 justify-content-center alert alert-info" style="display: none" role="alert"></div>
  				<div id="register-success"  class="row mt-5 justify-content-center alert alert-success" style="display: none" role="alert"></div>
   			
   			</div>
    			
		</div>
	</div>
	
</div>
