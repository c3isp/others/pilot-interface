<!-- WARNING! This file is never used! This page is returned as a string from the WebApiController.
It'is just to have a nice view if we need to make any modification -->

<div id="bruteforce-parameter" class="form-group">
	<div class="row mt-2" id="param-row-0">
		<div class="col-sm-3">
			<label class="col-form-label m-auto">MinFractionFailures:</label>
		</div>
		<div class="col-sm-1"></div>
		<div class="col-sm-1">
			<input type="number" name="minFractionFailures" min="0" max="1" step="0.01">
		</div>
		<div class="col-sm">
			<small class="text-muted"> If the percentage of failures is
				equal or greater than this value, then an attack has been detected.
			</small>
		</div>
	</div>
	<div class="row mt-2" id="param-row-0">
		<div class="col-sm-3">
			<label class="col-form-label m-auto">MinFailures:</label>
		</div>
		<div class="col-sm-1"></div>
		<div class="col-sm-1">
			<input type="number" name="minFailures">
		</div>
		<div class="col-sm">
			<small class="text-muted"> Minimum number of failures to
				signal a possible malicious brute force attacker. </small>
		</div>
	</div>
</div>

<hr>