<div id="login-page">
	<div id="search-card" class="row justify-content-center m-0">
		<div class="card mt-5 w-75 shadow p-3 mb-5 rounded card-style">
			<div class="card-header row no-gutters">
				<div class="col-sm"></div>
				<div class="col-sm text-center">Login</div>
				<div class="col-sm"></div>
			</div>
			<form name='f' action="login" method='POST'>
				<table class="table table-striped">
					<tr class="table-info">
						<td class="font-weight-bold">User:</td>
						<td><input type='text' name='username'></td>
					</tr>
					<tr class="table-warning">
						<td class="font-weight-bold">Password:</td>
						<td><input type='password' name='password'/></td>
					</tr>
					<tr class="table-info">
						<td></td>
						<td><input name="submit" type="submit" value="Submit"
							class="btn btn-success" /></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>
