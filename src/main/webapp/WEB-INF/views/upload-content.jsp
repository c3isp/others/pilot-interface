<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="${pageContext.request.contextPath}/uis/c3isp/js/upload.js" type="text/javascript"></script>

<div id="upload-page" class="main-content mt-3">
	<a id="content-header" href="#" class="btn btn-sm animated-button thar-four">DATA MANAGER</a>
	
	<div class="row justify-content-center m-0">
		<div class="card mt-5 w-75 shadow p-3 mb-5 rounded card-style">
			<div class="card-header text-center ">
	    		Upload
			</div>
		    <div class="card-body">
		    
		    	<hr>
		    	
		    	<form id="data-manager-upload-form" modelAttribute="uploadedFile">
				    <div class="form-group row mb-0 mt-5">
				    	
				    	<div class="col-sm-1"></div>
					    <div class="col-sm-7">
						    <div class="image-upload-wrap">
						    	<input id="uploader-single-file" name="uploader-single-file" class="file-upload-input" type='file' onchange="readURL(this);" multiple readonly required />
					    		<div class="drag-text">
					      			<h4 id="file-name">Drag and drop<sup class="required-field">*</sup></h4>
					    		</div>
						    </div>						    
					    </div>
					    <div class="col-sm-3">
					    	<button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Select file</button>
					    </div>
					</div>
					<hr>
					
					
					<div class="form-group row mt-5">
  				    	<label class="col-sm-3 col-form-label">Event Type<sup class="required-field">*</sup></label>
    					<div class="col-sm-5">
    						<input type="text" id="event-type" name="event_type" aria-describedby="event-type-help" class="form-control" maxlength=256 placeholder="MAIL, dns, ..." autocomplete="on" required/>
    					</div>
    					<div class="col-sm">
						    <small id="event-type-help" class="text-muted">
						        Select the event type of the file you want to upload.
						    </small>
    					</div>
  					</div>
  					
  					<div class="form-group row mt-3">
  						<label class="col-sm-3 col-form-label">Start time<sup class="required-field">*</sup></label>
  						<div class="col-sm-5">
  							<input type="text" id="start-time" name="start_time" class="form-control" aria-describedby="start-time-help" placeholder="yyyy-mm-ddThh:mm:00.0Z" required>
  						</div>
  						<div class="col-sm">
						    <small id="start-time-help" class="text-muted">
						    	Specify the event start time you are uploading.
						    </small>
  						</div>
  					</div>
  					
  					<div class="form-group row mt-3">
  						<label class="col-sm-3 col-form-label">End time<sup class="required-field">*</sup></label>
  						<div class="col-sm-5">
  							<input type="text" id="end-time" name="end_time" class="form-control" aria-describedby="end-time-help" placeholder="yyyy-mm-ddThh:mm:00.0Z" required>
  						</div>
  						<div class="col-sm">
						    <small id="end-time-help" class="text-muted">
						    	Specify the event end time you are uploading.
						    </small>
  						</div>
  					</div>
  					
  					<div class="form-group row mt-3">
						<label class="col-sm-3 col-form-label">Organization<sup
							class="required-field">*</sup></label>
						<div class="col-sm-5">
							<input type="text" class="form-control" id="upload-organization"
								name="organization" aria-describedby="organization-help" value="${organisation}" required disabled>
						</div>
						<div class="col-sm">
							<small id="organization-help" class="text-muted"> Select the
								organization name to whom you belong to. </small>
						</div>
  					</div>
  					
  					<c:if test="${not empty dsaList}">
  					  <jsp:include page="dsa-selection.jsp">
  					    <jsp:param name="dsaList" value="${dsaList}" />
  					  </jsp:include>
  					</c:if>
  					
  					<div class="form-group row mt-3">
						<label class="col-sm-3 col-form-label">Stixed data</label>
						<div class="col-sm-1">
							<input type="checkbox" class="form-control" id="upload-stixed"
								name="stixed" aria-describedby="stixed-help" value="true">
						</div>
						<div class="col-sm-4"></div>
						<div class="col-sm">
							<small id="stixed-help" class="text-muted"> Tick this box
							if you desire to upload the data in Stix format. </small>
						</div>
  					</div>
  					
  					<hr>
  					
  					<div id="upload-submit-div" class="row mt-5 justify-content-center">
  						<div class="col-3">
		  					<button type="submit" id="upload-form-submit" class="file-upload-btn">Submit</button>
  						</div>
  					</div>
  					
  					<c:choose>
  					  <c:when test="${not empty error}">
  						<div id="upload-error" class="row mt-5 justify-content-center alert alert-danger" role="alert">${error}</div>
  						<script>$(".form-group").hide();
  								$("hr").hide();
  								$("#upload-submit-div").hide();
  						</script>	
  					  </c:when>
  					  <c:otherwise>
  					  	<div id="upload-error" class="row mt-5 justify-content-center alert alert-danger" style="display: none" role="alert"></div>
  					  </c:otherwise>
  					</c:choose>	
  					
  					<div id="upload-info"  class="row mt-5 justify-content-center alert alert-info" style="display: none" role="alert"></div>
  					<div id="upload-success"  class="row mt-5 justify-content-center alert alert-success" style="display: none" role="alert"></div>
  					<div id="upload-warning"  class="row mt-5 justify-content-center alert alert-warning" style="display: none" role="alert"></div>

		    	</form>
		    </div>
		</div>
	</div>
</div>

  


