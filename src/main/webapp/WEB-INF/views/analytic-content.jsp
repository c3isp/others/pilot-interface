<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="${pageContext.request.contextPath}/uis/c3isp/js/search.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/uis/c3isp/js/analytic.js" type="text/javascript"></script>

<div id="analytic-page" class="main-content mt-3">
	<a id="content-header" href="#" class="btn btn-sm animated-button thar-four">ANALYTICS</a>
	
	<div id="analytic-card" class="row justify-content-center m-0" >
		<div class="card mt-5 w-75 shadow p-3 mb-5 rounded card-style">
			<div class="card-header row no-gutters">
				<div class="col text-center">
		    		Analytic
				</div>
			</div>
			
		    <div class="card-body">
		    
		    	<hr>
		    	
		    	<form id="data-manager-analytic-form">
										
					<div class="form-group row mt-4">
  						<label for="analytic-access-purpose" class="col-sm-3 col-form-label">Access Purpose<sup class="required-field">*</sup></label>
  						<div class="col-sm-3">
  							<select class="custom-select" id="analytic-purpose" name="accessPurpose" aria-describedby="event-type-help" required>
						        <option value="Cyber Threat Monitoring">Cyber Threat Monitoring</option>
						        <option value="Data Breaches Notification">Data Breaches Notification</option>
						        <option value="Threat Notification Precautions">Threat Notification &amp; Precautions</option>
						        <option value="Law Obligations">Law Obligations</option>
						        <option value="Contractual Obligations">Contractual Obligations</option>
						    </select>
  						</div>  					
  						<div class="col-sm-1"></div>	
  						<div class="col-sm">
						    <small id="access-purpose-help" class="text-muted">
						    	Specify your access purpose.
						    </small>
  						</div>
  					</div>					
					
					<div class="form-group row mt-4 mb-4">
  				    	<label class="col-sm-3 col-form-label">Analytic<sup class="required-field">*</sup></label>
    					<div class="col-sm-3">
      						<select class="custom-select" id="analytic-type" name="serviceName" aria-describedby="event-type-help" required>
						        <option value="">Choose...</option>
						        <c:forEach items="${analyticName}" var="name" varStatus="loop">    
								    <option value="${analyticValue[loop.index]}">${name}</option>
								</c:forEach>
						    </select>
    					</div>
    					<div class="col-sm-1"></div>
    					<div class="col-sm">
						    <small id="event-type-help" class="text-muted">
						        Select the analytic you want to run.
						    </small>
    					</div>
  					</div>
							
					<hr>		
							
					<div class="text-center row mt-4 mb-4">
						<h4 id="search-panel-text" class="col-sm">Search Panel</h4>
					</div>
							
					<%@include file="search-form.jsp" %>
					
					<hr>
						
					<div class="row mt-4 mb-4 pointer" id="advanced-arrow" style="display:none">
						<div class="col-sm"></div>
						<a id="advance-collapse" class="col-sm-2 text-right text-primary"
							data-toggle="collapse" aria-expanded="false"> Advanced <i
							id="analytic-resize-arrow" class="fas fa-arrow-down"></i>
						</a>
					</div>
					
					<div id="advanced" class="collapse">	
						<div id="include">
						</div>
					</div>					
						
  					
  					<div id="analytic-submit-div" class="row mt-4 justify-content-center">
  						<div class="col-sm"></div>
  						<div class="col-sm-3 text-center">
		  					<button id="submit" type="submit" class="file-upload-btn">Run</button>
  						</div>
  						<div class="col-sm"></div>  						
  					</div>
  					
  					<c:choose>
  					  <c:when test="${not empty error}">
  						<div id="analytic-error" class="row mt-5 justify-content-center alert alert-danger" role="alert">${error}</div>
  						<script>$(".form-group").hide();
  								$("hr").hide();
  								$("#analytic-submit-div").hide();
  								$("#search-panel-text").hide();
  						</script>	
  					  </c:when>
  					  <c:otherwise>
  					  	<div id="analytic-error" class="row mt-5 justify-content-center alert alert-danger" style="display: none" role="alert"></div>
  					  </c:otherwise>
  					</c:choose>	
  					
  					<div id="analytic-error" class="row mt-3 justify-content-center alert alert-danger" style="display: none" role="alert"></div>
  					<div id="analytic-info"  class="row mt-3 justify-content-center alert alert-info" style="display: none" role="alert"></div>
  					<div id="analytic-success"  class="row mt-3 justify-content-center alert alert-success" style="display: none" role="alert"></div>
  					<div id="analytic-warning"  class="row mt-3 justify-content-center alert alert-warning" style="display: none" role="alert"></div>
  					
		    	</form>
		    </div>
		</div>
	</div>
	
</div>
