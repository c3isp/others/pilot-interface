<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="dashboard-page" class="main-content mt-3">
	<div id="dashboard-info" class="row">
		<div class="col-sm-1"></div>

		<div class="card mt-5 shadow p-2 rounded col-sm">
			<div class="card-header row no-gutters">
				<div class="col text-center">Informations</div>
			</div>

			<div class="card-body">
				<c:choose>
					<c:when test="${not empty error}">
						<div class="row">
							<div class="col-sm">
								<p>Error: ${error}</p>
								<a class="btn btn-sm btn-warning mr-3" href="javascript:window.history.back()">Go Back</a>
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<div class="row">
							<div class="col-sm">
								<p><b>Analytic name :</b> ${operationName} (id:${data.getOperationId()})</p>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<p><b>History id :</b> ${data.getId()}</p>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<p><b>User id :</b> ${data.getUserId()}</p>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<p><b>Execution time :</b> ${data.getTimestamp()}</p>
							</div>
						</div>
						<div class="row">
							<div class="col-sm">
								<p><b>${data.getType()} :</b> ${data.getValue()}</p>
							</div>
						</div>					
						<a class="btn btn-sm btn-warning mr-3" href="javascript:window.history.back()">Go Back</a>
						<a class="btn btn-success btn-sm" data-toggle="collapse" href="#collapseRaw" role="button" aria-expanded="false" aria-controls="collapseRaw">Show raw data</a> <small class="ml-2"> raw result contains ${resultLines} lines.</small>						
						<div class="collapse mt-2" id="collapseRaw" >
							<div class="card card-body">
								<pre>${result}</pre>
							</div>
						</div>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
		<div class="col-sm-1"></div>
	</div>

	<c:if test="${data.getOperationId() == '1'}">
	<div id="dashboard-graphs" class="row">
		<div class="col-sm-1"></div>
		
		<div class="card-deck col-sm">
		<div class="card mt-5 shadow p-2 rounded text-center w-50">
			<div class="card-header row no-gutters">
				<div class="col text-center">bar chart for ${operationName}</div>
			</div>
			<div class="card-body">
			  <svg class="svggraph" id="graph1"></svg>
			</div>
		</div>

		<div class="card mt-5 shadow p-2 rounded text-center w-50">
			<div class="card-header row no-gutters">
				<div class="col text-center">pie chart for ${operationName}</div>
			</div>
			<div class="card-body text-center">
			   <svg class="svggraph" id="graph2"></svg>
			</div>
		</div>
		</div>
		
		<div class="col-sm-1"></div>
	</div>
	</c:if>
	
	<c:if test="${data.getOperationId() == '13' }">
		<div class="row m-3" id="canvas-container">
		<div class="col-sm-1"></div>
		  <div class="col-7" id="canvas-wrapper" style="padding: 0; border: 1px solid #3b4a5b; border-radius: 5px 0 0 5px;">
		    <svg id="canvas"></svg>
		  </div>
	  	  <div id="legend" class="col-3 d-flex flex-column sidebar" style="border: 1px solid #3b4a5b; border-left: 0px; border-radius: 0 5px 5px 0;">
		    <h2>Legend</h2>
		    <ul id="legend-content"></ul>
		  </div>
		  <div class="col-sm-1"></div>
		</div>
		<div class="row m-3">
		<div class="col-sm-1"></div>
		<div id="selected" class="col-10 sidebar" style="border: 1px solid #3b4a5b; border-radius: 5px;">
		    <h2>Selected Node</h2>
		    <div id="selection">
		    </div>
		  </div>
		<div class="col-sm-1"></div>
		</div>
		<script type="text/javascript">
			const correlateVresult = ${result};
		</script>
		
		<script src="//cdnjs.cloudflare.com/ajax/libs/d3/3.5.17/d3.min.js" charset="utf-8"></script>
		
		<script src="${pageContext.request.contextPath}/vendor/cti-stix-visualization-master/stix2viz/stix2viz/stix2viz.js"></script>
		<script src="${pageContext.request.contextPath}/uis/c3isp/js/dashboard.js"></script>
	</c:if>

	<div id="dashboard-table" class="row mb-3">
		<div class="col-sm-1"></div>

		<div class="card mt-5 mb-6 shadow p-2 rounded text-center col-sm">
			<div class="card-header row no-gutters">
				<div class="col text-center">${operationName} Table</div>
			</div>
			<div class="card-body text-center">
				<div id="dashboard-table" class="row mt-3 text-center">
					<div class="col text-center table-responsive">
					<table class="table striped table-success">
						<c:if test="${not empty resultListSpamClassify}">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Dpo-id</th>
									<th scope="col">Filename</th>
									<th scope="col">Classification</th>
								</tr>
							</thead>
							<tbody>	
							<c:forEach items="${resultListSpamClassify}" var="item" varStatus="itemStatus">
							<tr>
								<td scope="col" class="font-weight-bold">${itemStatus.index}</td>
								<c:set value="${item.getMailId()}" var="mailId"></c:set>
								<td class="pointer text-primary" onclick="ajaxReadDpo('${mailId}','dashboard');return false;"><u>${mailId}</u></td>
								<td><b>${item.getFilename()}</b></td>
								<td><b>${item.getClassifiedAs()}</b></td>
							</tr>
							</c:forEach>
							</tbody>
						</c:if>
						
						<c:if test="${not empty resultListSpamClusterer}">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Dpo-id</th>
									<th scope="col">Filename</th>
									<th scope="col">Purity</th>
									<th scope="col">ClusterId</th>
									
								</tr>
							</thead>
							<tbody>	
							<c:forEach items="${resultListSpamClusterer}" var="item" varStatus="itemStatus">
							<tr>
								<td scope="col" class="font-weight-bold">${itemStatus.index}</td>
								<c:set value="${item.getMailId()}" var="mailId"></c:set>
								<td class="pointer text-primary" onclick="ajaxReadDpo('${mailId}','dashboard');return false;"><u>${mailId}</u></td>
								<td><b>${item.getFilename()}</b></td>
								<td><b>${item.getPurity()}</b></td>
								<td><b>${item.getLeafId()}</b></td>
								
							</tr>
							</c:forEach>
							</tbody>
						</c:if>
						
						

						<c:if test="${not empty resultListSpamDetect}">
							<thead>
								<tr>
									<th scope="col">#</th>
<!-- 									<th scope="col">source</th> -->
<!-- 									<th scope="col">destination</th> -->
<!-- 									<th scope="col">spam value</th> -->
									<th scope="col">Dpo-id</th>
									<th scope="col">File-name</th>
									<th scope="col">Percentage</th>
									<th scope="col">Result</th>
								</tr>
							</thead>
							<tbody>	
							<c:forEach items="${resultListSpamDetect}" var="item" varStatus="itemStatus">
							<tr>
								<td scope="col" class="font-weight-bold">${itemStatus.index}</td>
								<c:set value="${item.get('mailId')}" var="mailId"></c:set>
								<td class="pointer text-primary" onclick="ajaxReadDpo('${mailId}','dashboard');return false;"><u>${mailId}</u></td>
<%-- 								<td>${item.get('subject')}</td> --%>
<%-- 								<td>${item.get('source')}</td> --%>
<%-- 								<td>${item.get('destination')}</td> --%>
								<td><b>${item.get('filename')}</b></td>
								<td><b>${item.get('spamVal')}</b></td>
								<td style='color:${item.get('spamColor')}'><b>${item.get('spam')}</b></td>
							</tr>								    
							</c:forEach>
							</tbody>								
						</c:if>
						
						<c:if test="${not empty resultListDGA}">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Msg</th>
									<th scope="col">Time</th>
									<th scope="col">Timezone</th>
								</tr>
							</thead>
							<tbody>	
							<c:forEach items="${resultListDGA}" var="item" varStatus="itemStatus">
							<tr>
								<td scope="col" class="font-weight-bold">${itemStatus.index}</td>
								<td><b>${item.get('msg')}</b></td>
								<td>${item.get('end')}</td>
								<td>${item.get('dtz')}</td>
							</tr>								    
							</c:forEach>
							</tbody>
						</c:if>

						<c:if test="${not empty resultListBruteForce}">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Src</th>
									<th scope="col">App</th>
								</tr>
							</thead>
							<tbody>
							<c:forEach items="${resultListBruteForce}" var="item" varStatus="itemStatus">
							<tr>
								<td scope="col" class="font-weight-bold">${itemStatus.index}</td>
								<td><b>${item.get('src')}</b></td>
									<td>${item.get('app')}</td>
							</tr>
							</c:forEach>
							</tbody>
						</c:if>
						
						<c:if test="${not empty resultConnToMaliciousHosts}">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Ip</th>
									<th scope="col">Blacklisted</th>
								</tr>
							</thead>
							<tbody>
							<c:forEach items="${fheIp}" var="item" varStatus="itemStatus">
							<tr>
								<td scope="col" class="font-weight-bold">${itemStatus.index}</td>
								<td><b>${item}</b></td>
								<td>${fheResult[itemStatus.index]}</td>
							</tr>
							</c:forEach>
							</tbody>
						</c:if>
					</table>
					
					<div class="card-header row no-gutters mt-5 mb-3">
						<div class="col text-center">DPOS found</div>
					</div>
					
					<table class="table striped table-success">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">DPO-ID</th>
								<th scope="col">Status</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${searchedDpos}" var="item" varStatus="itemStatus">
								<tr>
									<td scope="col" class="font-weight-bold">${itemStatus.index}</td>
									<c:choose>
									  <c:when test="${status[itemStatus.index] == 'Permitted'}">
										<td class="pointer text-primary" onclick="ajaxReadDpo('${item}','dashboard');return false;"><u>${item}</u></td>
									    <td class="text-success" ><b>Permitted</b></td>
									  </c:when>
									  <c:otherwise>
									  	<td><p>${item}</p></td>
									    <td class="text-danger"><b>Denied</b></td>
									  </c:otherwise>
									</c:choose>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					
						<div id="dashboard-error" class="row mt-5 justify-content-center alert alert-danger" style="display: none" role="alert"></div>
						<div id="dashboard-info" class="row mt-5 justify-content-center alert alert-info" style="display: none" role="alert"></div>
						<div id="dashboard-success" class="row mt-5 justify-content-center alert alert-success" style="display: none" role="alert"></div>
						<div id="dashboard-warning" class="row mt-5 justify-content-center alert alert-warning" style="display: none" role="alert"></div>
					
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-sm-1"></div>
	</div>
	<br><br><br><br><br><br><br><br><br>
</div>
<c:if test="${data.getOperationId() == '1'}">
<script src="${pageContext.request.contextPath}/uis/c3isp/js/graph.js"></script>
<script>
loadGraphGet('${pageContext.request.contextPath}/v1/convertToGraph/convertSpamInBarChart/${dpoId}/','#graph1', setGraphBarChart2);
loadGraphGet('${pageContext.request.contextPath}/v1/convertToGraph/convertSpamInPieChart/${dpoId}/','#graph2', setGraphPieChart);
</script>
</c:if>
<c:if test="${data.getOperationId() == '1'}">
	<script src="${pageContext.request.contextPath}/vendor/cti-stix-visualization-master/require.js"></script>
</c:if>