<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <div class="sidebar-user-info">  
	    <c:choose>
		    <c:when test="${empty username}">
		    	<h3 class="pt-3">Welcome user</h3>
		    </c:when>
		    <c:otherwise>
		    	<h3 class="pt-3">Welcome ${username}</h3>		        
		    </c:otherwise>
		</c:choose>		
		<img src="${pageContext.request.contextPath}/uis/c3isp/images/avatar_man.png" class="sidebar-avatar-unlogged"></img>
    </div>
    
    <div id="accordion">
	    <a href="${pageContext.request.contextPath}/home" id="sb-home" class="btn btn-sm animated-button thar-four">HOME</a>
	    <a href="${pageContext.request.contextPath}/analytic" id="sb-analytic" class="btn btn-sm animated-button thar-four">ANALYTICS</a>
	    <a href="${pageContext.request.contextPath}/history" id="sb-history" class="btn btn-sm animated-button thar-four">HISTORY</a>
	    
	    <div id="headingTwo">
	        <h5 class="mb-0">
	      		<a href="#" class="btn btn-sm animated-button thar-four collapsed" data-toggle="collapse" data-target="#dataManagerCollapse" aria-expanded="false" aria-controls="dataManagerCollapse">DATA MANAGER</a>
	        </h5>
	    </div>		    
	    <div id="dataManagerCollapse" class="collapse dark-bg" aria-labelledby="headingTwo" data-parent="#accordion">
        	<div id="sb-upload" class="text-right sidebar-row">
        		<div class="sidebar-icon">
			    	<i class="fas fa-file-upload color-white"></i>
        		</div>
        		<div class="sidebar-element">
        			<a href="${pageContext.request.contextPath}/upload" id="sb-upload" class="color-white" role="button">Upload</a>
        		</div>	
        	</div>
<!--         	<div id="sb-streaming" class="text-right sidebar-row"> -->
<!--         		<div class="sidebar-icon"> -->
<!-- 			    	<i class="fas fa-cloud-upload-alt color-white"></i> -->
<!--         		</div> -->
<!--         		<div class="sidebar-element"> -->
<!--         			<span class="color-white">Streaming</span> -->
<!--         		</div>	 -->
<!--         	</div> -->
        	<div id="sb-search" class="text-right sidebar-row">
        		<div class="sidebar-icon">
			    	<i class="fas fa-search color-white"></i>
        		</div>
        		<div class="sidebar-element">
        			<a href="${pageContext.request.contextPath}/search" id="sb-search" class="color-white" role="button">Search</a>
        		</div>	
        	</div>
        	<div id="sb-move" class="text-right sidebar-row">
        		<div class="sidebar-icon">
        			<i class="fas fa-angle-right color-white"></i>
        		</div>
        		<div class="sidebar-element">
        			<a href="${pageContext.request.contextPath}/move" id="sb-move" class="color-white" role="button">Move</a>
        		</div>	
        	</div>
        	<div id="sb-streaming" class="text-right sidebar-row">
        		<div class="sidebar-icon">
        			<i class="fas fa-stream color-white"></i>
        		</div>
        		<div class="sidebar-element">
        			<a href="${pageContext.request.contextPath}/streaming" id="sb-streaming" class="color-white" role="button">Streaming</a>
        		</div>	
        	</div>
        	<div id="sb-dsa" class="text-right sidebar-row">
        		<div class="sidebar-icon">
			    	<i class="fas fa-file-contract color-white"></i>
        		</div>
        		<div class="sidebar-element">
        			<a href="//dsamgr2.iit.cnr.it/DSAEditor/" target="_blank" id="sb-dsa" class="color-white" role="button">DSA</a>
        		</div>	
        	</div>
        </div>
</div>


