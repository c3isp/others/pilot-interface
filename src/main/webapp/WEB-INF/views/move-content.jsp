<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="item" class="eu.c3isp.isi.api.restapi.types.metadata.Metadata"/>
<div id="move-page" class="main-content mt-3">
	<a id="content-header" href="#" class="btn btn-sm animated-button thar-four">MOVE</a>
	
	<div id="move-card" class="row justify-content-center m-0" >
		<div class="card mt-5 w-75 shadow p-3 mb-5 rounded card-style">
			<div class="card-header row no-gutters">
				<div class="col text-center">
		    		Move
				</div>
			</div>
			
		    <div class="card-body">
		    
		    	<hr>
		    	
		    	<form id="data-manager-move-form">
										
					<div class="form-group row mt-4">
  						<label for="move-access-purpose" class="col-sm-3 col-form-label">Access Purpose<sup class="required-field">*</sup></label>
  						<div class="col-sm-3">
  							<select class="custom-select" id="move-purpose" name="accessPurpose" aria-describedby="event-type-help" required>
						        <option value="">Choose...</option>
						        <option value="Cyber Threat Monitoring">Cyber Threat Monitoring</option>
						        <option value="Data Breaches Notification">Data Breaches Notification</option>
						        <option value="Threat Notification Precautions">Threat Notification &amp; Precautions</option>
						        <option value="Law Obligations">Law Obligations</option>
						        <option value="Contractual Obligations">Contractual Obligations</option>
							<option value="Contractual Obligations">Multi-Modal Transportation Monitoring</option>
						    </select>
  						</div>  					
  						<div class="col-sm-1"></div>	
  						<div class="col-sm">
						    <small id="access-purpose-help" class="text-muted">
						    	Specify your access purpose.
						    </small>
  						</div>
  					</div>					
					
  					
		    	</form>
		    </div>
		</div>
	</div>
	<div id="card-body table-responsive" class="row justify-content-center m-0" >
		<div class="card mt-5 w-75 shadow p-3 mb-5 rounded card-style">
	<div id="dpotable-table" class="content-center m-0">
		<div class=col-2></div>
		<div class="col text-center m-0">
			<table class="table striped" >
			    <thead>
				    <tr class="d-flex">
				    	<th class="col-1" >Number</th>
					    <th class="col-4" >dpo</th>
					    <th class="col-5" >Metadata</th>
					    <th class="col-2" >Action</th>
				    </tr>
			    </thead>
			    <tbody>
				  <c:forEach items="${localDpo}" var="item" varStatus="items">
							<tr class="d-flex">
								<td class="col-1" id="move-type-${items.index}" >${items.index} </td> 
								<td class="col-4"scope="col" class="font-weight-bold">${item.getId()}</td>
								<td class="col-5"scope="col">${item.toString()}</td>
								<td class="col-2">
								<button id="move-${item.getId()}" class="btn btn-danger" 
								onclick='moveContentScript("${item.getId()}", "${items.index}")'>Move</button></td>
								
							</tr>								    
							</c:forEach>
			    </tbody>
			</table>
		</div>
		<div class=col-2></div>
	</div>
	</div>
	</div>
	
</div>
