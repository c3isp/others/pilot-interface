<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- glyph icons -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/vendor/fontawesome/web-fonts-with-css/css/fontawesome-all.css">

<!-- sidebar -->

<link href="${pageContext.request.contextPath}/vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

<!-- Custom styles for this template-->
<link href="${pageContext.request.contextPath}/uis/c3isp/css/index.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/uis/c3isp/css/button.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/uis/c3isp/css/sidebar.css" rel="stylesheet">

<link href="${pageContext.request.contextPath}/uis/c3isp/css/tables.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/uis/c3isp/css/popover.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/uis/c3isp/css/animations.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/uis/c3isp/css/dashboard.css" rel="stylesheet">

<!-- <script src="/js/bundle.js"></script> -->
<script src="${pageContext.request.contextPath}/vendor/jquery/dist/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/vendor/jquery-ui/jquery-ui.min.js"></script>

<c:if test = "${page == 'dashboard'}">
  <script src="${pageContext.request.contextPath}/vendor/d3/d3.min.js" charset="utf-8"></script>
  <link href="${pageContext.request.contextPath}/vendor/nvd3/build/nv.d3.css" rel="stylesheet" type="text/css">
  <script src="${pageContext.request.contextPath}/vendor/nvd3/build/nv.d3.js"></script>
  <%-- <script src="${pageContext.request.contextPath}/uis/c3isp/js/graph.js"></script> --%>
</c:if>

<!-- favicon -->
<link rel="icon" href="${pageContext.request.contextPath}/uis/c3isp/images/favicon.png" type="images/png" sizes="16x16">

<title>C3ISP - Collaborative and Confidential Information Sharing and Analysis for Cyber Protection</title>
   
