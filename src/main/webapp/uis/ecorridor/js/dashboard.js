
var visualizer;
selectedContainer = document.getElementById('selection');
canvasContainer = document.getElementById('canvas-container');
canvas = document.getElementById('canvas');

/*******************************************************************
 * Resizes the canvas based on the size of the window
 ******************************************************************/
function resizeCanvas() {
	var cWidth = document.getElementById('canvas-wrapper').clientWidth;
	var cHeight = document.getElementById('legend').clientHeight;
	document.getElementById('canvas-wrapper').style.width = cWidth;
	canvas.style.width = cWidth;
	canvas.style.height = cHeight;
}

/*******************************************************************
 * Will be called right before the graph is built.
 ******************************************************************/
function vizCallback() {
	resizeCanvas();
}

/*******************************************************************
 * Will be called if there's a problem parsing input.
 ******************************************************************/
function errorCallback() {

}

/*******************************************************************
 * Initializes the graph, then renders it.
 ******************************************************************/
function vizStixWrapper(content, customConfig) {
	cfg = {
		iconDir : "/pilot-interface/js/cti-stix-visualization-master/stix2viz/stix2viz/icons"
	}
	visualizer = new Viz(canvas, cfg, populateLegend,
			populateSelected);
	visualizer.vizStix(content, customConfig, vizCallback,
			errorCallback);
}



/*******************************************************************
 * Fetches STIX 2.0 data from an external URL (supplied user) via
 * AJAX. Server-side Access-Control-Allow-Origin must allow
 * cross-domain requests for this to work.
 ******************************************************************/





/*******************************************************************
 * Adds icons and information to the legend.
 * 
 * Takes an array of type names as input
 ******************************************************************/
function populateLegend(typeGroups) {
	var ul = document.getElementById('legend-content');
	var color = d3.scale.category20();
	typeGroups
		.forEach(function(typeName, index) {
			var li = document.createElement('li');
			var val = document.createElement('span');
			var key = document.createElement('div');
			var keyImg = document.createElement('img');
			keyImg.onerror = function() {
				// set the node's icon to the default if this
				// image could not load
				this.src = visualizer.d3Config.iconDir
						+ "/stix2_custom_object_icon_tiny_round_v1.svg";
			}
			keyImg.src = visualizer.iconFor(typeName);
			keyImg.width = "37";
			keyImg.height = "37";
			keyImg.style.background = "radial-gradient("
					+ color(index) + " 16px,transparent 16px)";
			keyImg.style.margin = "0 10px 15px 0";
			key.appendChild(keyImg);
			val.innerText = typeName.charAt(0).toUpperCase()
					+ typeName.substr(1).toLowerCase(); // Capitalize
														// it
			li.appendChild(key);
			key.appendChild(val);
			ul.appendChild(li);
		});
}

/*******************************************************************
 * Adds information to the selected node table.
 * 
 * Takes datum as input
 ******************************************************************/
function populateSelected(d) {
	// Remove old values from HTML
	selectedContainer.innerHTML = "";

	var counter = 0;

	Object.keys(d).forEach(function(key) { // Make new HTML
											// elements and display
											// them
		// Create new, empty HTML elements to be filled and injected
		var div = document.createElement('div');
		var type = document.createElement('div');
		var val = document.createElement('div');

		// Assign classes for proper styling
		if ((counter % 2) != 0) {
			div.classList.add("odd"); // every other row will have
										// a grey background
		}
		type.classList.add("type");
		val.classList.add("value");

		// Add the text to the new inner html elements
		var value = d[key];
		type.innerText = key;
		val.innerText = value;

		// Add new divs to "Selected Node"
		div.appendChild(type);
		div.appendChild(val);
		selectedContainer.appendChild(div);

		// increment the class counter
		counter += 1;
	});
}

/*******************************************************************
 * Hides the data entry container and displays the graph container
 ******************************************************************/
function hideMessages() {
	canvasContainer.classList.toggle("hidden");
}



/*******************************************************************
 * Returns the page to its original load state
 ******************************************************************/
function resetPage() {
	var header = document.getElementById('header');
	if (header.classList.contains('linkish')) {
		hideMessages();
		visualizer.vizReset();
		document.getElementById('files').value = ""; // reset the
														// files
														// input
		document.getElementById('chosen-files').innerHTML = ""; // reset
																// the
																// subheader
																// text
		document.getElementById('legend-content').innerHTML = ""; // reset
																	// the
																	// legend
																	// in
																	// the
																	// sidebar
		document.getElementById('selection').innerHTML = ""; // reset
																// the
																// selected
																// node
																// in
																// the
																// sidebar

		header.classList.remove('linkish');
	}
}

		

/*******************************************************************
 * When the page is ready, setup the visualization and bind events
 ******************************************************************/


window.onresize = resizeCanvas;


/******
 * actually start canvas
 * 
 * 
 */


vizStixWrapper(correlateVresult, "");








