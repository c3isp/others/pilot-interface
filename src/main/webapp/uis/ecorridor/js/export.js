//Submit handlers
$(function(){
	
	//SEARCH FORM - SUBMIT
	$("#data-manager-search-form").on("submit",function(form){
		form.preventDefault();
		
		var jsonObject = Object.create(searchJson);
		jsonObject = populateSearchJsonObject(jsonObject);
		
		var json = jsonObject.createSearchJson(jsonObject);
		console.log("json="+json);
		jsonObject.deleteSearchJson(jsonObject);
		
		console.log("searching for results");
		searchSubmit(json);
			
		
	});
	
});

//Click handlers
$(function(){
	
	//PLUS BUTTON HANDLERS
	$("#add-search-param").click(function(){
		
		var searchParam = $("#search-param").val();
		console.log("searchParam="+searchParam);
		
		switch(searchParam){
			case "event_type" : addRequiredField("event-type"); $("#event-type-div").fadeIn(); break;
			case "start_time" : addRequiredField("start-time"); $("#start-time-div").fadeIn(); break;
			case "end_time" : addRequiredField("end-time"); $("#end-time-div").fadeIn(); break;
			case "organization" : addRequiredField("organization"); $("#organization-div").fadeIn(); break;
			case "security_level" : addRequiredField("security"); addRequiredField("level"); $("#security-div").fadeIn(); break;
			case "id" : addRequiredField("dpo"); $("#dpo-div").fadeIn(); break;
			default: decodeError("no searchParam value chosen","","analytic"); break;
		}
	});
	
	$("#event-type-div .minus-button").click(function(){
		$(this).parent().parent().fadeOut();
		removeRequiredField("event-type");
	});
	
	$("#start-time-div .minus-button").click(function(){
		$(this).parent().parent().fadeOut();
		removeRequiredField("start-time");
	});
	
	$("#end-time-div .minus-button").click(function(){
		$(this).parent().parent().fadeOut();
		removeRequiredField("end-time");
	});
	
	$("#organization-div .minus-button").click(function(){
		$(this).parent().parent().fadeOut();
		removeRequiredField("organization");
	});
	
	$("#dpo-div .minus-button").click(function(){
		$(this).parent().parent().fadeOut();
		removeRequiredField("dpo");
	});
	
	$("#security-div .minus-button").click(function(){
		$(this).parent().parent().fadeOut();
		removeRequiredField("security");
	});
	
	$("#new-search-button").click(function(){
		
		$("#data-manager-search-form")[0].reset();
		$("#table-div").fadeOut();
		
		$("#new-search").fadeOut(function(){
			$("#search-card").fadeIn();
		});
		
	});
});

//others

function populateSearchJsonObject(jsonObject){
	
	var location = $("input[name=searchLocation]:checked", "form").val();
	var operator = $("input[name=combining_rule]:checked", "form").val();
	
	jsonObject.location = location;
	jsonObject.operator = operator;
	
	$('form').find('div[setted=true]').each(function(){
		
		var attributeSelect = $(this).find("select").attr("attribute");
		var attributeInput = $(this).find("input").attr("attribute");
		
		if(attributeSelect != null)
			jsonObject.attributes.push(attributeSelect);
		else
			jsonObject.attributes.push(attributeInput);
		
		var operator = $(this).find("select[name=operator] option:selected").val();
		
		if(operator != null)
			jsonObject.operators.push(operator);
		else
			jsonObject.operators.push("eq");
		
		var selectValue = $(this).find("select[name=value] option:selected").val(); 
		var inputValue = $(this).find("input[name=value]").val();
		
		if(selectValue != null)
			jsonObject.values.push(selectValue);
		else
			jsonObject.values.push(inputValue);		    		
	});
	
	return jsonObject;
}
