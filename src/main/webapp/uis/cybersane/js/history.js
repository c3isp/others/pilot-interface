//onLoad handlers

$(function(){
	$("#history-page td[data-toggle=popover]").popover({
		trigger: "hover"
	});
	
	footerRepositioning();
});

//Others
function sendTicket(value){
	showLoader();
	ajaxHistoryGetDpo(value);
}

function copyToClipboard(value){
	var $temp = $("<input>");
	$temp.val(value);
	$("body").append($temp);
	$temp.select();
	document.execCommand("copy");
	$temp.remove();
}

function deleteRow(id,rowNumber){
    console.log("row id="+id);
    ajaxHistoryDeleteRow(id,rowNumber);
}

function footerRepositioning(){
	var height = $( document ).height();
	var footer_height = $(".footer").height();
	
	$(".page-container").css({
		"min-height": height+"px"
	});
	
	$(".footer-recomputed").css({
		"bottom": "-"+footer_height+"px"
	});
}