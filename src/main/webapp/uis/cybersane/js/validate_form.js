/******************* SEARCH FORM ************************/
function validateSearchForm(form_data,attribute){
	
	//We validate organization fields
	switch(attribute){
		case "organization": return validateOrganization();
		case "start_time": return validateDate("start");
		case "end_time": return validateDate("end");
		case "security": return validateSecurity();
		case "dpo": return validateDpo();
		case "event_type": return validateEventType(); 
		default: console.log("validating with attribute=plus!"); return 0;
	}
}

function validateOrganization(){
	
	if($("#search-organization-select").val() == "" || $("#search-organization").val().length == 0){
		console.log("2-required error in organization");
		//decodeError("2-required","","search");
		decodeError("required","","search");
		return -1;
	}
	
	return 0;
}

function validateDate(date){
	
	if(validateDateRegex("search",date) == -1){
		console.log("wrong "+date+" date format");
		return -1;
	}

	if(validateDateInput(date) == -1){
		console.log("2-required error in "+date+" date");
		return -1;
	}
	
	return 0;
}

function validateDateInput(start_or_end){
	
	var time_select = $("#search-"+start_or_end+"-time-select").val();
	var time_input = $("#search-"+start_or_end+"-time").val();
	
	if(time_select === "" || time_input.length == 0){
		decodeError("required","","search");
//		decodeError("2-required","","search");
		return -1;
	}
	
	return 0;
}

function validateSecurity(){
	
	if($("#search-security-select").val() == "" || $("#search-level-select").val() == ""){
		console.log("2-required error security");
//		decodeError("2-required","","search");
		decodeError("required","","search");
		return -1;
	}
	
	return 0;
	
}

function validateDpo(){
	
	if($("#search-dpo-id").val() == ""){
		console.log("required error dpo");
		decodeError("required","","search");
		return -1
	}
	
	return 0;
}

function validateEventType(){
	
	if($("#event-type").val() == ""){
		console.log("required error event type");
		decodeError("required","","search");
		return -1;
	}
	
	return 0;
}

//function validateConditionSelectors(form_data){
//	
//	var array = form_data.split("&");
//	var count = 0;
//	var max_count = 2;
//	
//	for(var i=0; i<array.length; i++){
//		
//		var field_value = array[i].split("=");
//		var field = field_value[0];
//		var value = field_value[1];
//		
//		//console.log(field+"="+value);
//		//required field. We can skip it
//		if(field === "searchLocation" || field === "searchDataType")
//			continue;
//		
//		if(field === "searchDpoId" && value.length > 0)
//			max_count = 1;
//			
//		if(value.length > 0)
//			count++;
//	}
//	
//	//console.log("count="+count);
//	//console.log("max_count="+max_count);
//	
//	if(count == max_count && count > 0)
//		return 0;
//	else{
////		decodeError("no-condition-selector","","search");
//		console.log("Error in validateConditionSelectors, with form_data=");
//		console.log(form_data);
//		decodeError("required","","search");
//		return -1;
//	}
//}

/************************************************************/


/******************** COMMON FORM ***************************/

function validateDateRegex(card_type,start_or_end){
	
	var date_regex = new RegExp(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{1}Z/gm);
	var time = $("#"+card_type+"-"+start_or_end+"-time").val(); 
	
	if(time.length > 0){
		//console.log("time.length="+time.length)
		if(time.match(date_regex) == null){
			console.log("time.match(date_regex)=null");
			decodeError("invalid",start_or_end+" time",card_type);
			return -1;
		}
	}
	
	return 0;
}

function restoreFormValues(values){
	console.log("restoring values="+values);
	var form_array = values.split("&");
	
	for(var i=0; i<form_array.length; i++){
		var field_value = form_array[i].split("=");
		var field = field_value[0];
		var value = field_value[1];
		
		if(field === "searchLocation"){
			if(value === "remote"){
				$("#data-manager-search-form input[name="+field+"]")[0].checked = true;
				$("#data-manager-search-form input[name="+field+"]")[1].checked = false;
			}
			else{
				$("#data-manager-search-form input[name="+field+"]")[0].checked = false;
				$("#data-manager-search-form input[name="+field+"]")[1].checked = true;
			}
			continue;
		}
		
		if(field === "searchEventType" || field === "searchOrganizationCS" || field === "searchStartTimeCS" ||
		   field === "searchEndTimeCS" || field === "searchSecurityCS" || field === "searchSecurityLevel"){
			$("#data-manager-search-form select[name="+field+"] option").each(function(){
				if($(this).val() === value){
					$(this).attr("selected","selected");
				} 
			});
			continue;
		}
		
		if(field === "searchOrganization" || field === "searchDpoId" || field === "searchStartTime" || 
		   field === "searchEndTime"){
			$("#data-manager-search-form input[name="+field+"]").val(value);
		}
		
	}
}

function addRequiredField(id){
	var element = $("#"+id);
	
	var select_element = $("#"+id+"-select")[0]
	var input_element = element ? element[0] : null
	
	select_element ? select_element.setAttribute("required", true) : null;
	input_element ? input_element.setAttribute("required", true) : null;
//	
//	
//	if(element.has("input")){
//		element.children('input').each(function () {
//			$(this).attr("required","true");		
//		});
//	}
//	
//	if(element.has("select")){
//		$("#"+id).find("select").each(function(){
//			$(this).attr("required","true");		
//		});
//	}
//	
	$("#"+id+"-div").attr("setted","true");
}

function removeRequiredField(id){
	
	var element = $("#"+id);
	
	var select_element = $("#"+id+"-select")[0]
	var input_element = element ? element[0] : null
	
	select_element ? select_element.removeAttribute("required") : null;
	input_element ? input_element.removeAttribute("required") : null;
	
	
//	$("#"+id+" > :input").removeAttr("required");
//	$("#"+id+" > :select").removeAttr("required");
	$("#"+id+"-div").removeAttr("setted");
}

function clearForm(form) {

    $(':input', form).each(function() {
      var type = this.type;
      var tag = this.tagName.toLowerCase(); // normalize case

      if (type == 'text' || type == 'password' || tag == 'textarea')
        this.value = "";

      else if (type == 'checkbox' || type == 'radio')
        this.checked = false;

      else if (tag == 'select')
        this.selectedIndex = -1;
    });
}
/**************************************************************/