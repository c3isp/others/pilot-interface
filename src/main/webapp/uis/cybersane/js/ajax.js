/*
 * Page: upload.jsp
 * Inputs involved: #organization-select 
 * Output: list of DSA-ID inside the #dsa-id-select <select> input
 * Use: this ajax sends the selected organization value to the getDsaId(Organization org) RESTapi
 * that returns a list of DSA-ID placed inside the #dsa-id-select <select> input
 */


function getContextPath() {
	   return window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));
}

$("#upload-organization").change(function() {
  // Check input( $( this ).val() ) for validity here
	var value = this.value;
	var data = "{\"organization\":\""+ value +"\"}";
	console.log("data="+data);
	showLoader();
	
	$.ajax({
		type: "POST",
		url: getContextPath() + "/v1/getDsaId",
		data: data,
		contentType: "application/json",  
		dataType: "json",
		success: function(result){
			
			hideLoader();
			//Error or Info
			if(decodeSuccessInfoError(result,"upload")==false){
				return;
			}
			else{
				$(".alert").fadeOut();
				$('#form-submit').show();
			}
			
			var select = $("#dsa");
			select.empty();
			
			for(var i=0; i<result.content.length; i++){
				
				var option = $("<option></option>").appendTo(select);
					option.text(result.content[i].title);
					option.val(result.content[i].id);
			}
			$("#select-div").fadeIn();
		},
		error:function(xhr, httpStatusMessage, message){
			hideLoader();
			var error = JSON.parse(xhr.responseText);
			decodeError("Error "+error.content," ","upload")
		}
	})
});

function searchSubmit(json){
	showLoader();
	
	$.ajax({
		url: getContextPath() + "/v1/searchDpoId",
		type: "POST",
		contentType: "application/json",
		dataType: "json",
		data: json,
		success: function(result){
			//Error or Info
			if(decodeSuccessInfoError(result,"search")==false){
				hideLoader();
				return;
			}
			
			console.log(result);
			console.log(result.content);
			createSearchTableResults(result.content,function(){
				hideLoader(function(){
					$("#search-card").fadeOut(function(){
						$("#table-div").fadeIn();
						$(".alert").hide();
						$("#new-search").fadeIn();
					});
				});
			});
		},
		error:function(xhr, httpStatusMessage, message){
			hideLoader();
			var error = JSON.parse(xhr.responseText);
			decodeError("Error "+error.content," ","search")
		}
	})
}

function uploadSingleFile(file){
	
	showLoader();
	
	$.ajax({
		url: getContextPath() + "/v1/uploadFile",
		type: "POST",
//		enctype: 'multipart/form-data',
		contentType: false,
		processData: false,
		data: file,
		dataType: "json",
		cache: false,
		success: function(result){
			
			hideLoader();
			if(decodeSuccessInfoError(result,"upload")==false){
				return;
			}
			
			$(".alert").fadeOut(function(){
				if (result.content.additionalProperties == null){
					$("#upload-success").text(result.content).fadeIn();
				}
				else{
					$("#upload-success").text("Your file has been uploaded successfully, with the following DpoID:\n"+result.content.additionalProperties.dposId).fadeIn();										
				}
			});
		},
		error:function(xhr, httpStatusMessage, message){
			hideLoader();
			var error = JSON.parse(xhr.responseText);
			decodeError("Error "+error.content," ","upload")
		},
		timeout: 60 * 60 * 1000

	})
}

function uploadFileMetadata(metadata){
	
	showLoader();
	
	$.ajax({
		url: getContextPath() + "/v1/uploadFileMetadata",
		type: "POST",
		contentType: "application/json",
		data: metadata,
		dataType: "json",
		success: function(result){
			
			hideLoader();
			if(decodeSuccessInfoError(result)==false){
				return;
			}
			
			$(".alert").fadeOut(function(){
				$("#upload-success").text("Your file has been uploaded successfully, with the following DpoID:\n"+result.content.additionalProperties.dposId).fadeIn();
			});
		},
		error: function(){
			hideLoader();
			decodeError("exception");
		}
	});
	
}

function registrationSubmit(data){
	
	showLoader();
	
	$.ajax({
		url: getContextPath() + "/v1/registerNewUser",
		type: "POST",
		contentType: "application/json",
		data: data,
		dataType: "json",
		success: function(result){
			
			hideLoader();
			if(decodeSuccessInfoError(result)==false){
				return;
			}
			
			$(".alert").fadeOut(function(){
				$("#register-success").text("Thank you! Your request has been accepted. Please check your e-mail and follow the instruction to complete the registration").fadeIn();
			});
		},
		error: function(){
			hideLoader();
			decodeError("exception");
		}
	})
}

function ajaxLoginSubmit(data){
	
	showLoader();
	
	$.ajax({
		url: getContextPath() + "/v1/login",
		type: "POST",
		contentType: "application/json",
		data: data,
		dataType: "json",
		success: function(result){
			
			hideLoader();
			if(decodeSuccessInfoError(result)==false){
				return;
			}
			
			$(".alert").fadeOut(function(){
				$("#register-success").text("Thank you! Your request has been accepted. Please check your e-mail and follow the instruction to complete the registration").fadeIn();
			});
		},
		error: function(){
			hideLoader();
			decodeError("exception");
		}
	})
}


function analyticSubmit(data){
	showLoader();
	console.log(data);
	$.ajax({
		url: getContextPath() + "/v1/runAnalytic",
		type: "POST",
		data: data,
		dataType: "json",
		contentType: "text/html",
		success: function(result){
			
			hideLoader();
			if(decodeSuccessInfoError(result)==false){
				return;
			}
			
			$(".alert").fadeOut(function(){
				$("#analytic-success").text("Your analytic is running with the following ticket: "+result.content.value).fadeIn();
			});
		},
		error: function(){
			hideLoader();
			decodeError("exception");
		}
	})
}

function ajaxHistoryGetDpo(data){
	
	showLoader();
	console.log("data="+data);
	$.ajax({
		url: getContextPath() + "/v1/history/getResult/"+data+"/",
		type: "POST",
		data: data,
		dataType: "json",
		success: function(result){
			
			hideLoader();
			if(decodeSuccessInfoError(result,"history")==false){
				return;
			}
			
			window.location.href=getContextPath() + "/history";
			
		},
		error: function(){
			hideLoader();
			decodeError("exception");
		}
	})
}


function ajaxHistoryDeleteRow(data,rowNumber){
	
	showLoader();
	console.log(data);
	
	$.ajax({
		url: getContextPath() + "/v1/history/deleteRow",
		type: "POST",
		data: data,
		contentType: "text/plain",
		success: function(result){
			
			if(decodeSuccessInfoError(result)==false){
				hideLoader();
				return;
			}
			
			hideLoader(function(){
				$("#type-"+rowNumber).parent().fadeOut();
			});
		},
		error: function(){
			hideLoader();
			decodeError("exception");
		}
	})
}

function moveContentScript(data, rowNumber) {
	showLoader();
	console.log(data);
	console.log(rowNumber);
	
	var accessPurpose = $( "#move-purpose option:selected" ).text()
	
	//var jsondata = {'dpoid' : data, 'accessPurpose' : accessPurpose};
	var jsondata = {};
	jsondata.dpoid = data;
	jsondata.accessPurpose = accessPurpose;
	console.log(JSON.stringify(jsondata));
	url_string = getContextPath() + "/v1/move_v1";
	console.log(url_string);
	
	var json = JSON.stringify(jsondata);

	$.ajax({
		url: getContextPath() + "/v1/move_v1",
		type: "POST",
		data: json,
		contentType: 'application/json',
		success: function(result){
			
			if(decodeSuccessInfoError(result)==false){
				hideLoader();
				return;
			}
			
			console.log("#move-type-"+rowNumber);
			hideLoader(function(){
				$("#move-type-"+rowNumber).parent().remove();
			});
		},
		error: function(){
			hideLoader();
			decodeError("exception");
		}
	})

}

function ajaxStreaming(data){
	
	showLoader();
	
	$.ajax({
		url: getContextPath() + "/v1/setStreamParams",
		data: data,
		type: "POST",
		contentType: 'application/json',
		success: function(result){
			
			hideLoader();
			if(decodeSuccessInfoError(result)==false){
				return;
			}
			
//			hideLoader(function(){				
//				var array = result.split("%%%");
//				
//				$("#streaming-success").fadeOut();
//				$("#streaming-error").fadeOut();
//				$("#streaming-info").fadeOut();
//				
//				console.log(array[1]);
//				if(array[1] != null || array[1] != undefined){
//					$("#streaming-error").text(array[1]).delay(500).fadeIn();
//				}
//				else{
//					$("#streaming-success").text(result).delay(500).fadeIn();
//				}
//			});
		},
		error: function(){
			hideLoader();
			decodeError("exception");
		}
	})
}

function ajaxReadDpo(dpoId,page) {
	showLoader();
	console.log(dpoId);
	document.location.href=getContextPath()+"/download/"+dpoId+"/";
	hideLoader();
/*	$.ajax({
		url: getContextPath() + "/download/"+dpoId+"/",
		type: "GET",
		success: function(result){
			hideLoader();
			document.location.href=getContextPath()+"/download/"+dpoId+"/";
		},
		error: function(){
			hideLoader();
			appendError("You don't have the permissions to download this dpo!",page);
		}
	})
*/
}


function ajaxExportDpo(dpoId,page) {
	console.log(dpoId);
        var mispKey = prompt("Please provide misp key:");
        if (!mispKey)
            return;
        
        console.log('mispKey', mispKey);
        
        var exportParams = JSON.stringify({mispKey: mispKey});
        
	showLoader();
	$.ajax({
		url: getContextPath() + "/export/"+dpoId+"/misp",
                data: {exportParams: exportParams},
		type: "GET",
		success: function(result){
			hideLoader();
			appendSuccess("DPO exported successfully", page);
		},
		error: function(e){
                        console.error('ajax error', e);
			hideLoader();
                        // display error message if found
                        var errStr = "You don't have the permissions to download this dpo!";
                        if (e.responseJSON && e.responseJSON.message)
                            errStr = "Error: " + e.responseJSON.message;
			appendError(errStr,page);
		}
	});
}

//THIS IS A BASE FUNCTION USEFULL FOR IMPLEMENTING THE NOTIFY MECHANISM ON THE PORTAL

//$(function(){
//	getNotify();
//});
//
//function getNotify(){
//	
//	console.log("called getNotify()");
//	$.ajax({
//		url: getContextPath() + "/v1/tryToFetchAnalyticResult",
//		data: "",
//		type: "GET",
//		success: function(result){
//			console.log(result);
//			setTimeout(getNotify,3000);
//		},
//		error: function(error){
//			console.log(error);
//		}
//	});
//}
