//Submit handler
$(function(){
	
	//When a file is selected for upload, it will be uploaded on the back-end
	//in this way we can take the metadata directly from the file uploaded and
	//use them to fill up all the form fields
	
//	$(".file-upload-input").on("change",function(){
//		
//		console.log("called");
//		$(".progress-bar").css("width","0%").attr('aria-valuenow', 0);
//		var file = $("#uploader-single-file").get(0).files[0];
//		var formData = new FormData(file);
//		formData.append("file", file);
//		
//		  $.ajax({
//			url: getContextPath() + "/v1/uploadSingleFile",
//		    type: 'POST',
//		    data: formData,
//		    cache: false,
//		    contentType: false,
//		    processData: false,
//		    xhr: function () {
//		      var myXhr = $.ajaxSettings.xhr();
//		      if (myXhr.upload) {
//		        // For handling the progress of the upload
//		        myXhr.upload.addEventListener('progress', function (e) {
//		          if (e.lengthComputable) {
//		        	  var percentage = (e.loaded / e.total) * 100;
//		        	  $(".progress-bar").css("width",percentage+"%");
//		          }
//		        }, false);
//		      }
//		      return myXhr;
//		    },
//		    success: function(result){
//		    	console.log("after choosing the file result is: "+result);
//		    	var tmp_array=result.split("ERROR:");
//		    	if(tmp_array[1] != null || tmp_array[1] !== undefined){
//		    		console.log("errore");
//		    		decodeError(result,"","upload");
//		    		$("#progress-bar").remove("class");
//		    		$("#progress-bar").attr("class","progress-bar progress-bar-striped progress-bar-error active");
//		    		$("#upload-organization").find('option:selected').removeAttr("selected");
//		    		$("#select-div").fadeOut();
//		    		$("#start-time").val("");
//		    		$("#end-time").val("");
//		    		$("#event-type").val("");
//		    		return;
//		    	}
//		    	tmp_array=result.split("INFO:");
//		    	if(tmp_array[1] != null || tmp_array[1] !== undefined){
//		    		console.log("info");
//		    		$("#progress-bar").remove("class");
//		    		$("#progress-bar").attr("class","progress-bar progress-bar-striped progress-bar-success active");
//		    		$("#upload-organization option").attr("selected","");
//		    		$("#upload-organization").find('option:selected').removeAttr("selected");
//		    		$("#select-div").fadeOut();
//		    		$("#start-time").val("");
//		    		$("#end-time").val("");
//		    		$("#event-type").val("");
//		    		return;
//		    	}
//		    	else{
//		    		var json = JSON.parse(result);
//		    		$("#start-time").val(json.startTime).toggle("highlight",100).toggle("highlight",{color:"#d3f1dc"},800);
//		    		$("#end-time").val(json.endTime).toggle("highlight",100).toggle("highlight",{color:"#d3f1dc"},800)
//		    		var organization = (json.organization).toUpperCase();
//		    		console.log("Calling selectDSA... organization is: "+organization);
//		    		$("#upload-organization option[value='"+organization+"']").attr("selected","selected");
//		    		$("#upload-organization").toggle("highlight",100).toggle("highlight",{color:"#d3f1dc"},800)
//		    		$("#event-type").val(json.dataType).toggle("highlight",100).toggle("highlight",{color:"#d3f1dc"},800)
//		    		selectDsa("{\"organization\":\""+ organization +"\"}");
//		    	}
//		    },
//		    error: function(error){
//		    	console.log("error: "+error);
//		    }
//		  });
//		
//	});
	
	//UPLOAD FORM
	$("#data-manager-upload-form").on("submit",function(form){
		form.preventDefault();
		
		if($("#uploader-single-file").get(0).files[0] == null){
			$(".alert").fadeOut(function(){
				$("#upload-error").text("Please choose at least one file to upload").fadeIn();
			});
			return;
		}
		
		var jsonObject = Object.create(uploadJson);
		jsonObject.event_type = $("input[name=event_type]").val();
		jsonObject.start_time = $("input[name=start_time]").val();
		jsonObject.end_time = $("input[name=end_time]").val();
		jsonObject.organization = $("select[name=organization]").val();
		jsonObject.dsa_id = $("#dsa").val();
		
		if ($('input[name=stixed]').is(":checked")) {
			jsonObject.stixed = "true";
		} else {
			jsonObject.stixed = "false";
		}
		
		console.log("jsonObject.stixed ="+jsonObject.stixed );
		console.log("dsa_id is "+jsonObject.dsa_id);
		
//        var value = $("#isi_type").prop('checked');
//		jsonObject.localisi = value.toString();
//		console.log(value.toString());
		//jsonObject.dsa_id = $("select[name=dsa_id] option:selected").val();

		
		

//		fileInput.each(function(index, field){
//			console.log(index);
//			const file = field.files[index];
//			console.log(file.name);
//		});	
//		
//		return;
		
//		var file = $("#uploader-single-file").get(0).files[0];
		var filename = $("#uploader-single-file").get(0).files[0].name; 
		jsonObject.id = crypto.getRandomValues(new Uint32Array(4)).join('-')+"-"+filename;
		
		var formdata = new FormData();
		$.each($("input[type='file']")[0].files, function(i, file) {
			formdata .append('files', file);
	    });
		
		var json = jsonObject.createUploadJson(jsonObject);
		
		console.log("json="+json);
		jsonObject.deleteUploadJson(jsonObject);
		
		formdata.append("metadata",json);
		uploadSingleFile(formdata);
		
		
//		var card = Object.create(protoUploadCard);
//		
//		if(card.validateForm() == -1)
//			return;
//		
//		card.formData = $("#data-manager-upload-form").serialize();
//		card.file = $("#uploader-single-file").get(0).files[0];
//		card.filename = $("#uploader-single-file").get(0).files[0].name; 
//		card.uuid = crypto.getRandomValues(new Uint32Array(4)).join('-')+"-";
//		
//		card.formData = decodeURIComponent(card.formData);
//		
//		var form_data = new FormData();
//		form_data.append("file",card.file);
//		
//		var param_array = card.formData.split("&");
//		
//		var json = '{\n' +
//			'"id": "' + card.uuid + card.filename + '",\n';
//		
//		for(var i=0; i<param_array.length; i++){
//			var query_item = param_array[i];
//			var item = query_item.split("=");
//			var field = item[0];
//			var value = item[1];
//			
//			if(field === "dsa_id")
//				value = value.substring(0,value.length-4);
//			
//			if(i == param_array.length-1)
//				json += '"'+field+'": '+'"'+value+'"\n';
//			else
//				json += '"'+field+'": '+'"'+value+'",\n';
//		}
//		json += "}";
//		/*
//		json='{' +
//					    '"id": "3605064090-2890033390-1290830049-3959168099-test.cef",'+
//					    '"event_type": "ssh",'+
//					    '"start_time": "2018-11-13T11:32:48.0Z",'+
//					    '"end_time": "2018-11-13T11:42:48.0Z",'+
//					    '"organization": "CNR",'+
//					    '"dsa_id": "DSA-78e40d8a-a7cc-4ec8-8b46-819b9870e4bf"'+
//					'}';
//		*/
//		console.log("json="+json);
//		//uploadSingleFileMetadata(json);
//		form_data.append("metadata",json);
//		uploadSingleFile(form_data);
//		
//		console.log("upload submitted");
	});
});

//OnLoad handlers
$(function(){
	
	//Autocomplete the startTime and endTime params
	var currentdate = new Date(); 
	var year = currentdate.getFullYear();
	
	var month = currentdate.getMonth()+1;
	month = (month<10 ? '0' : '') + month;
	
	var day = currentdate.getDate();
	day = (day<10 ? '0' : '') + day;
	
	var hours = currentdate.getHours();
	hours = (hours<10 ? '0' : '') + hours;
	
	var min = currentdate.getMinutes();
	min = (min<10 ? '0' : '') + min;
	
	var sec = currentdate.getSeconds();
	sec = (sec<10 ? '0' : '') + sec;
	
	var startTime = year + "-" + month + "-" + day + "T" 
					+ hours + ":" + min + ":" + sec + ".0Z";
	
	var endHour = parseInt(hours);
	endHour = endHour + 1;
	var endDay = parseInt(day);
	
	if(endHour > 24){
		endHour -= 24;
		endDay++;
	}
	
	endHour = (endHour<10 ? '0' : '') + endHour;
	endDay = (endDay<10 ? '0' : '') + endDay;
	
	var endTime = year + "-" + month + "-" + endDay + "T" 
					+ endHour + ":" + min + ":" + sec + ".0Z";

	$("#start-time").val(startTime);
	$("#end-time").val(endTime);
});

/************ DRAG AND DROP **************/

function readURL(input) {
    if (input.files && input.files[0]) {

    	var reader = new FileReader();

    	reader.onload = function(e) {
    		//$('.image-upload-wrap').hide();

    		$("#file-name").text(input.files[0].name);
		    //$('.file-upload-image').attr('src', e.target.result);
		    //$('.file-upload-content').show();
		
		    //$('.image-title').html(input.files[0].name);
    	};

    	reader.readAsDataURL(input.files[0]);

    } else {
    	removeUpload();
    }
}

function removeUpload() {
    $('.file-upload-input').replaceWith($('.file-upload-input').clone());
    $('.file-upload-content').hide();
    $('.image-upload-wrap').show();
}

$('.image-upload-wrap').bind('dragover', function () {
    $('.image-upload-wrap').addClass('image-dropping');
});

$('.image-upload-wrap').bind('dragleave', function () {
    $('.image-upload-wrap').removeClass('image-dropping');
});

