function decodeError(result,type,error_page){
	if(result.length == 0){
		appendError("No result found",error_page);
		return -1;
	}
	
	if(result == null || result === undefined){
		appendError("Null or undefined result. Fix the bug!",error_page);
		return -1;
	}
	
	if(result === "exception"){
		appendError("Something went wrong. Please retry again.",error_page);
		return -1;
	}
	
	if(result === "invalid"){
		appendError("Invalid "+type+" format",error_page);
		return -1;
	}
	
	if(result === "required"){
		appendError("You must fill all the required fields",error_page);
		return -1;
	}
	
	if(result === "no searchParam value chosen"){
		appendError("Please choose a search criterion before",error_page);
		return -1;
	}
	
	if(type === " "){
		appendError(result,error_page);
		return -1;
	}
	
//	if(result === "2-required"){
//		appendError("You must provide both condition selector and its input",error_page);
//		return -1;
//	}
	
//	if(result === "no-condition-selector"){
//		appendError("You must provide at most one condition selector and its input",error_page);
//		return -1;
//	}
	
	return 0;
}

function decodeSuccessInfoError(response,page){
	
	$(".alert").fadeOut();
	if(response.status==="ERROR"){
		if(response.content.indexOf('"message"') >= 0 && response.content.indexOf('"value"') >= 0){
			var array = response.content.split('"message":"');
			array = array[1].split("\"");
			$("#"+page+"-error").fadeOut().text(array[0]).fadeIn();
		}
		else{
			$("#"+page+"-error").fadeOut().text(response.content).fadeIn();
		}
		return false;
	}
	if(response.status==="INFO"){
		$("#"+page+"-info").fadeOut().text(response.content).fadeIn();
		return false;
	}
	if(response.status==="WARNING"){
		$("#"+page+"-warning").fadeOut().text(response.content).fadeIn();
		return false;
	}
	
//	array=toDecode.split("[SUCCESS]");
//	if(array[1] != null){
//		$("#upload-success").fadeOut(function(){
//			this.text(array[1]).fadeIn();
//		});
//		$("#upload-info").fadeOut();
//		$("#upload-error").fadeOut();
		return true;
//	}
}

function appendSuccess(msg, page) {
	console.log("error: "+msg);
	//$(".error-subject").fadeOut();
	$(".alert").fadeOut();
	$("#"+page+"-success").fadeOut().text(msg).fadeIn();
}

function appendError(error,error_page){
	console.log("error: "+error);
	//$(".error-subject").fadeOut();
	$(".alert").fadeOut();
	$("#"+error_page+"-error").fadeOut().text(error).fadeIn();
}

