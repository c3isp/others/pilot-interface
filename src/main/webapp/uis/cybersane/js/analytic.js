var advancedParams = 0;

//On Load handlers
$(function(){
	
	//Disable searchLocation Radio buttons
	var radioLocation = $("input[name=searchLocation]");
	radioLocation.parent().parent().parent().remove();
	
	$("hr[class=removable]").remove();
	
});

//Click handlers
$(function(){
	
	$("#analytic-type").on("change",function(){
		var data = $("#analytic-type > option:selected").val();
		$.ajax({
			url: getContextPath() + "/v1/analyticAdvancedParams/"+data+"/",
			type: "GET",
			data: data,
			success: function(data){
				if(data==""){
					$("#advanced-arrow").fadeOut();
					$("#include").fadeOut().empty();
				}
				else{
					$("#advanced-arrow").fadeOut().fadeIn();
					$("#include").fadeOut().empty().append(data).fadeIn();
				}
			},
			error: function(error){
				console.log(error);
			}
		})
	});
	
	$("#advance-collapse").click(function(){
		$("#advanced").collapse("toggle");
	});
});

//onSubmit handler
$(function(){
	
	$("#submit").click(function(form){
		form.preventDefault();
		
//		if($("#analytic-type option:selected").val()==""){
//			$(".alert").fadeOut(function(){
//				$("#analytic-error").text("Please choose the analytic to run").fadeIn();
//			});
//			return;
//		}
		if(validateAnalyticInput("search-param") == -1)
			return;
		if(validateAnalyticInput("analytic-type") == -1)
			return;
		if(validateAnalyticInput("event-type") == -1)
			return;
		if(validateAnalyticInput("organization-select") == -1)
			return;
		if(validateAnalyticInput("organization") == -1)
			return;
		if(validateAnalyticInput("dpo") == -1)
			return;
		if(validateAnalyticInput("start-time-select") == -1)
			return;
		if(validateAnalyticInput("start-time") == -1)
			return;
		if(validateAnalyticInput("end-time-select") == -1)
			return;
		if(validateAnalyticInput("end-time") == -1)
			return;
		if(validateAnalyticInput("level-select") == -1)
			return;
		if(validateAnalyticInput("security-select") == -1)
			return;
		
		var jsonObject = Object.create(analyticJson);
		jsonObject = populateAnalyticJson(jsonObject);
		
		var json = jsonObject.createAnalyticJson(jsonObject);
		
		console.log("json="+json);
		jsonObject.deleteAnalyticJson(jsonObject);
		json = btoa(json);
		
		analyticSubmit(json);
		
	});
});

//Others
function populateAnalyticJson(jsonObject){
	
	var formData = $("#data-manager-analytic-form").serialize();
	var formDataArray = formData.split("&");
	
	for(var i=0; i<formDataArray.length; i++){
		var keyValue = formDataArray[i].split("=");
		var key = keyValue[0];
		var value = keyValue[1];
		
		console.log("key: "+ key + "; value: "+value);
		
		if(key === "accessPurpose"){
			jsonObject.accessPurpose = value;
			continue;
		}
		if(key === "serviceName"){
			jsonObject.serviceName = value;
			continue;
		}
		if(key === "serviceParams" && value != ""){
			console.log("serviceParams value:"+value);
			jsonObject.serviceParams.push(value);
		}
		if(key === "minFractionFailures" && value != ""){
			jsonObject.minFractionFailures = value;
		}
		if(key === "minFailures" && value != ""){
			jsonObject.minFailures = value;
		}
		if(key === "purity" && value != ""){
			jsonObject.purity = value;
		}
		if(key === "minElementsThreshold" && value != ""){
			jsonObject.minElementsThreshold = value;
		}
		if(key === "featuresOnly" && value != ""){
			jsonObject.featuresOnly = value;
		}
		if(key === "analysisMethod" && value != ""){
			jsonObject.analysisMethod = value;
		}
		if(key === "application" && value != ""){
			jsonObject.application = value;
		}
		
	}
	
	var searchJsonObject = Object.create(searchJson);
	jsonObject.searchCriteria = populateSearchJsonObject(searchJsonObject);
	
	return jsonObject;
}

function validateAnalyticInput(id){
	
	if( $("#"+id).val()=="" && $("#"+id).attr("required")=="required"){
		$(".alert").fadeOut(function(){
			$("#analytic-error").text("Please fill all the required field before").fadeIn();
		});
		return -1;
	}
	return 0;
}
