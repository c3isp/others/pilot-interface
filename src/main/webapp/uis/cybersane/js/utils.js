function uuid() {
	return crypto.getRandomValues(new Uint32Array(4)).join('-');
}

function createSearchTableResults(result,callback){

	var table = $("#search-table");
	table.empty();
	
	var thead = $("<thead>").appendTo(table);
	var tr = $("<tr>").appendTo(thead);
	var th = $("<th>").text("#").appendTo(tr);
		th = $("<th>").text("DPO-ID").appendTo(tr);
		th = $("<th>").text("Download").appendTo(tr);
		
	var tbody = $("<tbody>").appendTo(table);
		
	for(let i=0; i<result.length; i++){
		let dpoId=result[i];
		tr = $("<tr>").appendTo(tbody);
		var td = $("<td>").text(i).appendTo(tr);
			td = $("<td>").text(dpoId).appendTo(tr);
			td = $("<td>").appendTo(tr);
		$("<button>").val("DOWNLOAD").text("DOWNLOAD").attr("class","btn btn-success").attr("dpoId", dpoId)
		.click(function(event){
			ajaxReadDpo(event.target.getAttribute("dpoId"),"searchResult");
		}).appendTo(td);
                
                
		$("<button>").val("EXPORT").text("EXPORT").attr("class","btn btn-info ml-2").attr("dpoId", dpoId)
		.click(function(event){
                        ajaxExportDpo(event.target.getAttribute("dpoId"), "searchResult");
		}).appendTo(td);
	}
	
	if(callback){
		console.log("calling callback");
		callback();
	}
}

function buildSearchJson(jsonObject){
	
	var json = '{\n' +
	'"combining_rule": "' + jsonObject.operator + '",\n' +
		'"criteria": [\n';
	
	for(var i=0; i<jsonObject.attributes.length; i++){
	
		if(i == jsonObject.attributes.length-1){
			json += '{\n' +
				'"attribute": "' + jsonObject.attributes[i] + '",\n' +
				'"operator": "' + jsonObject.operators[i] + '",\n' +
				'"value": "' + jsonObject.values[i] + '"\n' +
			'}';
		}
		else{
			json += '{\n' +
				'"attribute": "' + jsonObject.attributes[i] + '",\n' +
				'"operator": "' + jsonObject.operators[i] + '",\n' +
				'"value": "' + jsonObject.values[i] + '"\n' +
			'},'
		}
	}

	json += ']\n' +
	'}';
	
	return json;
}

function emptySearchJson(jsonObject){
	
	jsonObject.attributes.length = 0;
	jsonObject.operators.length = 0;
	jsonObject.values.length = 0;
	jsonObject.operator = "";
	jsonObject.location = "";
}

function buildAnalyticJson(jsonObject){
	
	var analyticJson = '{\n' +
	    '"subjectId": "' + jsonObject.subjectId + '",\n' + 
		'"metadata": {\n' +
//			'"accessPurpose": "' + jsonObject.accessPurpose + '"\n' +
		'},\n' + 
		'"searchCriteria": ';
	
	var returnedSearchJson = jsonObject.searchCriteria.createSearchJson(jsonObject.searchCriteria);
	
	analyticJson += returnedSearchJson + ',\n' +
		'"serviceName": "' + jsonObject.serviceName + '",\n' +
		'"serviceParams": {\n';
	
	var index = 0;
	while (index < jsonObject.serviceParams.length-1) {
		
		if(index+1 >= jsonObject.serviceParams.length-1){
			analyticJson += '"' + jsonObject.serviceParams[index] +'": "' + jsonObject.serviceParams[index+1] + '"\n';
		}
		if(index+1 < jsonObject.serviceParams.length-1){
			analyticJson += '"' + jsonObject.serviceParams[index] +'": "' + jsonObject.serviceParams[index+1] + '",\n'; 
		}
		
		index = index + 2;
	}
	
	if(jsonObject.serviceName == "bruteForceAttacksDetection"){
		analyticJson += 
			'"minFractionFailures": "' + jsonObject.minFractionFailures +'",\n' +
			'"minFailures": "' + jsonObject.minFailures +'"\n';			
	}
	
	if(jsonObject.serviceName == "spamEmailClusterer"){
		analyticJson += 
			'"minElementsThreshold": "' + jsonObject.minElementsThreshold +'",\n' +
			'"purity": "' + jsonObject.purity +'"\n';			
	}
	
	if(jsonObject.serviceName == "spamEmailClassify"){
		analyticJson += 
			'"features_only": "' + jsonObject.featuresOnly +'"\n';
	}
	
	if(jsonObject.serviceName == "connToMaliciousHosts"){
		analyticJson += 
			'"AnalysisMethod": "' + jsonObject.analysisMethod +'"\n';
	}
	
	if(jsonObject.serviceName == "correlateVulnerabilityAttack"){
		analyticJson += 
			'"application": "' + jsonObject.application +'"\n';
	}
	
	analyticJson += '},\n' +
	    '"additionalAttribute": {\n' +
	        '"accessPurpose": "' + jsonObject.accessPurpose + '"\n' +
        '}\n' +
	 '}';
	
	return analyticJson;
}

function emptyAnalyticJson(jsonObject){
	
	jsonObject.accessPurpose = "";
	jsonObject.searchCriteria.deleteSearchJson(jsonObject.searchCriteria);
	jsonObject.serviceName = "";
	jsonObject.serviceParams.length = 0;
	jsonObject.subjectId = "user";
}

function buildUploadJson(jsonObject){
	
	var stixed = jsonObject.stixed == "true" ? "true" : "false";
	console.log("stixed="+stixed);
	stixed = jsonObject.stixed === "true" ? "true" : "false";
	console.log("stixed="+stixed);
	
	var uploadJson = '{\n' +
		'"id": "' + jsonObject.id + '",\n' +
		'"event_type": "' + jsonObject.event_type + '",\n' +
		'"start_time": "' + jsonObject.start_time + '",\n' +
		'"end_time": "' + jsonObject.end_time + '",\n' +
		'"dsa_id": "' + jsonObject.dsa_id + '",\n' +
		'"subject_id": "' + jsonObject.subject_id + '",\n' +
		'"stixed": "' + stixed + '"\n' +
//		'"localisi": "' + jsonObject.localisi + '"\n' +
	'}';
	
	return uploadJson;
}

function emptyUploadJson(jsonObject){
	
	jsonObject.id = "";
	jsonObject.event_type = "";
	jsonObject.start_time = "";
	jsonObject.end_time = "";
	jsonObject.dsa_id = "";
	jsonObject.subject_id = "user";
	jsonObject.stixed= "false";
//	jsonObject.localisi = "";
	
}

function buildStreamingJson(jsonObject){
	
	var json = '{\n' +
		'"aggregators": [\n' +
		'{\n'+
		'"rootPath": "' + jsonObject.rootPath + '",\n' +
		'"sleepTime": "' + (jsonObject.sleepTime != "" ? jsonObject.sleepTime : "60") + '",\n' +
		'"organization": "' + jsonObject.organization+ '",\n' +
		'"dsaId": "' + jsonObject.dsaId + '"\n' +
		'}\n' +
		']\n' +
		'}';
	
	return json;
}

function redirectTo(path) {
	var getUrl = window.location;
	var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
	window.location.href=baseUrl + path;
}
