$(document).ready(function(){
    
	var popoverTemplate = '<div class="popover">' +
						      '<div class="arrow"></div>' +
						      '<h3 class="popover-header"></h3>' +
						      '<div class="popover-body"></div>' +
						  '</div>';
	
	//Login popover
    $('#profile').popover({
    		placement: "bottom",
    		template: popoverTemplate,
    		title: "LOGIN",
    		html: true,
    		content: function(){
    	        return $('#login-page').html();
    		}
    });

});

//Closing popover
$(document).on('click', function (e) {
    $('[data-toggle="popover"],[data-original-title]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {                
            (($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
        }
    });
});