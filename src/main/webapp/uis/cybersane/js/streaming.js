$(function(){
	
	$("#streaming-form").on("submit",function(form){
		form.preventDefault();
		
		var jsonObject = Object.create(streamingJson);
		jsonObject.rootPath = $("input[name=folder-path]").val();
		jsonObject.sleepTime = $("input[name=time-interval]").val();
		jsonObject.organization = $("select[name=\"organization\"] option:selected").val();
		jsonObject.dsaId = $("select[name=\"dsa_id\"] option:selected").val();
		
		var json = jsonObject.createStreamingJson(jsonObject);
		
		console.log(json);
//		json = JSON.stringify(json);
		
		ajaxStreaming(json);
	});
});
