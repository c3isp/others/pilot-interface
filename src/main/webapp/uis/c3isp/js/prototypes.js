//var protoSearchCard = {
//		
//	cardType : "search",
//	id: "",
//	formData: "",
//	attribute: "plus", //[plus, dpo, organization, start-time, end-time, security]
//	invalid: false,
//	hasSearchPanel: false,
//	hasOperatorPanel: false,
//	visibility: "visible",
//	tmp: true,
//	operator: "none",
//	sessionStorageId: "",
//	
//	goAway: function(callback){
//		goAwaySearchCard(callback);
//	},
//	comeBack: function(callback){
//		comeBackSearchCard(callback);
//	},
//	validateForm: function(form_data,attribute){
//		return validateSearchForm(form_data,attribute);
//	},
//	restoreForm: function(){
//		restoreFormValues(sessionStorage.getItem(this.sessionStorageId,this.formData));
//	}
//}
//
//var protoUploadCard = {
//		
//	cardType : "upload",
//	formData: "",
//	filename: "",
//	file: "",
//	uuid: "",
//	
//	validateForm: function(){
//		if(validateDateRegex("upload","start") == -1 || validateDateRegex("upload","end") == -1)
//			return -1;
//		else 
//			return 0;
//	}
//}
//
//var protopanel = {
//	id: "",
//	searchId: "",
//	operator: "none", //[and,or,none]
//	selected: false, //[true,false]
//	visible: false //[true,false]
//}

var searchJson = {
	
	location: "",
	combinigRule: "",
	attributes: new Array(),
	operators: new Array(),
	values: new Array(),
	
	createSearchJson(jsonObject){
		return buildSearchJson(jsonObject);
	},
	
	deleteSearchJson(jsonObject){
		emptySearchJson(jsonObject);
	}
}

var uploadJson = {
		
	id: "",
	event_type: "",
	start_time: "",
	end_time: "",
	dsa_id: "",
	subject_id: "user",
	stixed: "false",
//	localisi: "",
	
	createUploadJson(jsonObject){
		return buildUploadJson(jsonObject);
	},
	
	deleteUploadJson(jsonObject){
		emptyUploadJson(jsonObject);
	}
		
}

var analyticJson = {
		
	accessPurpose: "",
	searchCriteria: Object.create(searchJson),
	serviceName: "",
	serviceParams: new Array(),
	subjectId: "user",
	additionalAttributes: new Array(),
	
	minFractionFailures: 0.5,
	minFailures: 1,
	minElementsThreshold: 1,
	purity: 0.5,
	featuresOnly: false,
	analysisMethod: "BLACK_LIST_LOW",
	application: "IBM PowerKVM 2.1.0.2",

	createAnalyticJson(jsonObject){
		return buildAnalyticJson(jsonObject);
	},
	
	deleteAnalyticJson(jsonObject){
		emptyAnalyticJson(jsonObject);
	}
}

var streamingJson = {
	
	rootPath : "",
	sleepTime : "60",
	organization: "",
	dsaId: "",
		
	createStreamingJson(jsonObject){
		return buildStreamingJson(jsonObject);
	}
}

//var cardArray = [];
//var panelArray = [];
