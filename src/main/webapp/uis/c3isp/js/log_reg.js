/************* LOGIN AND REGISTRATION FORM HANDLERS ********************/

$(function(){
	$("#register-form").on("submit",function(form){
		form.preventDefault();
		
		form = $(this).serialize();
		
		formArray = form.split("&");
		
		var json ="{\n";
		
		for(var i=0; i<formArray.length; i++){
			var pair = formArray[i].split("=");
			var key = pair[0];
			var value = decodeURIComponent(pair[1]);
			
			if(i == formArray.length - 1)
				json += '"' + key + '": "' + value + '"\n';
			else{
				json += '"' + key + '": "' + value + '",\n';
			}
		}
		
		json += "}"; 
		console.log(json);
		registrationSubmit(json);
	});
});

function loginSubmit(){
	
		//form.preventDefault();
		console.log("clicked");
		var form = $("form").serialize();
		
		formArray = form.split("&");
		
		var json ="{\n";
		
		for(var i=0; i<formArray.length; i++){
			var pair = formArray[i].split("=");
			var key = pair[0];
			var value = decodeURIComponent(pair[1]);
			
			if(i == formArray.length - 1)
				json += '"' + key + '": "' + value + '"\n';
			else{
				json += '"' + key + '": "' + value + '",\n';
			}
		}
		
		json += "}"; 
		console.log(json);
		ajaxLoginSubmit(json);
	
}