function goAwaySearchCard(callback){
		
		var card = $("#search-card");
		var width = card.width();
		
		card.css({
			"position":"absolute",
			"width": width
		}).animate({
				left: "100%",
			},500
		).fadeOut(function(){
			
			if(callback)
				callback();
		});
		
}

function comeBackSearchCard(callback){
	
	$("#search-card").css({
		"position": "relative",
		"left": "100%"
	})
	.show().animate({
			left: "0%",
	},500,function(){
		
		if(callback)
			callback();
	});
	
}

function searchAndOrAnimation(search_card_count){
	
	var card = $("#search-card");
	var width = card.width();
	
	card.css({
		"position":"absolute",
		"width": width
	}).animate({
			left: "100%",
		},500
	).fadeOut(function(){
		$("#panel"+search_card_count).fadeIn();
		$("form").trigger("reset").fadeIn();
	});
	
}	

function plusButtonsAnimation(callback){
	
	$("#add-dpo").toggle("drop");
	setTimeout(function() {$("#add-start-time").toggle("drop")},100);
	setTimeout(function() {$("#add-end-time").toggle("drop")},200);
	setTimeout(function() {$("#add-organization").toggle("drop")},300);
	setTimeout(function() {$("#add-security").toggle("drop")},400);
	
	setTimeout(function(){
		if(callback)
			callback();
	},500);
}

function showLoader(callback){
    $('#loader').fadeIn(function(){    	
    	if(callback)
    		callback();
    });
}

function hideLoader(callback){
	$('#loader').fadeOut(function(){    	
    	if(callback)
    		callback();
    });
}
