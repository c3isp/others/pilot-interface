function loadGraphGet(url, svg_id, func) {
	console.log(url)

	$.ajax({
	    type: "GET",
	    async: true,
	    url: url,
	    success: function(data) { func(svg_id, data) },
	    failure: function(errMsg) { console.log("load Graph failed: " + svg_id) }
	});
}

function loadGraph(url, dpo_id, svg_id, func) {
	console.log(url)
	console.log(dpo_id)

	$.ajax({
	    type: "POST",
	    async: true,
	    url: url,
	    data: dpo_id,
	    contentType: "application/json; charset=utf-8",
	    dataType: "json",
	    success: function(data) { func(svg_id, data) },
	    failure: function(errMsg) { console.log("load Graph failed: " + svg_id) }
	});
}

function setGraphPieChart2(svg_id, data) {
    var width = 300;
    var height = 300;

    nv.addGraph(function() {
        var chart = nv.models.pie()
                .x(function(d) { return d.key; })
                .y(function(d) { return d.y; })
                .width(width)
                .height(height)
                .labelType(function(d, i, values) {
                    return values.key + ':' + values.value;
                })
                ;

        d3.select(svg_id)
                .datum([data])
                .transition().duration(1200)
                .attr('width', width)
                .attr('height', height)
                .call(chart);
        return chart;
    });
}

function setGraphPieChart(svg_id, data) {
    var height = 300;
    var width = 400;

    console.log(svg_id + " : " + data);

    var chart1;
    nv.addGraph(function() {
        var chart1 = nv.models.pieChart()
            .x(function(d) { return d.key })
            .y(function(d) { return d.y })
            .donut(true)
            .width(width)
            .height(height)
            .padAngle(.08)
            .cornerRadius(5)
            .id('donut');

        chart1.title("");
        chart1.pie.labelsOutside(false).donut(true);

    	d3.select(svg_id)
    		.attr("align","center")
    		.datum(JSON.parse(data))
    		.transition().duration(1200)
    		.call(chart1);

        return chart1;
    });
}

function setGraphBarChart2(svg_id, data) {
    var width = 300;
    var height = 300;

    console.log(svg_id + " : " + data);

    nv.addGraph(function() {
	    var chart = nv.models.discreteBarChart()
	    	.width(width)
	    	.height(height)
	        .x(function(d) { return d.label })
	        .y(function(d) { return d.value })
	        .staggerLabels(true)
	        .showValues(true)
	        .duration(250)
	        .rotateLabels(-45);

	    chart.yAxis.tickFormat(d3.format(',.0f'));

	    d3.select(svg_id)
    		.datum(JSON.parse(data))
	        .call(chart);

	    return chart;
    });
}

function setGraphBarChart3(svg_id, data) {
    var width = 350;
    var height = 350;

    console.log(svg_id + " : " + data);

    nv.addGraph({
        generate: function() {
            var width = nv.utils.windowSize().width,
                height = nv.utils.windowSize().height;

            var chart = nv.models.multiBarChart()
                .width(width)
                .height(height)
                .stacked(true)
                ;

            chart.dispatch.on('renderEnd', function(){
                console.log('Render Complete');
            });

            var svg = d3.select(svg_id).datum(JSON.parse(data));
            console.log('calling chart');
            svg.transition().duration(0).call(chart);

            return chart;
        },
        callback: function(graph) {
            nv.utils.windowResize(function() {
                var width = nv.utils.windowSize().width;
                var height = nv.utils.windowSize().height;
                graph.width(width).height(height);

                d3.select(svg_id)
                    .attr('width', width)
                    .attr('height', height)
                    .transition().duration(0)
                    .call(graph);

            });
        }
    });
}

function setGraphMultiBarChart(svg_id, data) {
    var width = 350;
    var height = 350;

    console.log(svg_id + " : " + data);

	nv.addGraph({
	    generate: function() {
	        var chart = nv.models.multiBarChart()
	            .width(width)
	            .height(height)
	            .stacked(true);

	        chart.dispatch.on('renderEnd', function(){
	            console.log('Render Complete');
	        });

	        var svg = d3.select(svg_id).datum(JSON.parse(data));
	        svg.transition().duration(0).call(chart);

	        return chart;
	    },
	    callback: function(graph) {
	        nv.utils.windowResize(function() {
	            var width = nv.utils.windowSize().width;
	            var height = nv.utils.windowSize().height;
	            graph.width(width).height(height);

	            d3.select(svg_id)
	                .attr('width', width)
	                .attr('height', height)
	                .transition().duration(0)
	                .call(graph);
	        });
	    }
	});
}

function setGraphStackedAreaChart(svg_id, data) {
    let jsonData = JSON.parse(data);
    var colors = d3.scale.category20();

    var chart;
    nv.addGraph(function() {
        chart = nv.models.stackedAreaChart()
            .legendPosition("right")
            .useInteractiveGuideline(true)
            .x(function(d) { return d[0] })
            .y(function(d) { return d[1] })
            .controlLabels({stacked: "Stacked"})
            .duration(500);

    chart._options.controlOptions = [];
    chart.legend.rightAlign(true);
    chart.legend.margin({top: 0, right: 0, left: 0, bottom: 0});
    chart.xAxis.tickFormat(function(d) { return d3.time.format('%d-%m-%Y')(new Date(d)) });
    chart.yAxis.tickFormat(d3.format(',.0f'));

    chart.legend.vers('furious');
    chart.legend.updateState(true);

    d3.select(svg_id)
        .datum(jsonData)
        .transition().duration(1000)
        .call(chart)
        .each('start', function() {
            setTimeout(function() {
                d3.selectAll(svg_id.concat(' *')).each(function() {
                    if(this.__transition__)
                        this.__transition__.duration = 1;
                })
            }, 0)
        });

        //nv.utils.windowResize(chart.update);
        return chart;
    });
}