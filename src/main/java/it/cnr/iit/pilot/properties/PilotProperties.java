package it.cnr.iit.pilot.properties;

import java.util.logging.Logger;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.util.UriComponentsBuilder;

@Configuration
public class PilotProperties {

	private Logger LOGGER = Logger.getLogger(PilotProperties.class.getName());

	@Value("${api.eventhandler.uri}")
	@NotNull
	private String eventHandlerUri;

	@Value("${api.eventhandler.subscribers}")
	@NotNull
	private String eventHandlerSubscribers;

	@Value("${api.eventhandler.subscribe}")
	@NotNull
	private String eventHandlerSubscribe;

	@Value("${api.eventhandler.notifyevent}")
	@NotNull
	private String eventHandlerNotifyEvent;

	@Value("${api.self.uri}")
	@NotNull
	private String selfUri;

	@Value("${api.self.eventarrived}")
	@NotNull
	private String selfEventArrived;

	public PilotProperties() {

	}

	public String getEventHandlerUri() {
		return eventHandlerUri;
	}

	public void setEventHandlerUri(String eventHandlerUri) {
		this.eventHandlerUri = eventHandlerUri;
	}

	public String getEventHandlerSubscribers() {
		return eventHandlerSubscribers;
	}

	public String getEventHandlerSubscribersAPI() {
		return getApiUri(getEventHandlerUri(), getEventHandlerSubscribers());
	}

	public void setEventHandlerSubscribers(String eventHandlerSubscribers) {
		this.eventHandlerSubscribers = eventHandlerSubscribers;
	}

	public String getEventHandlerSubscribe() {
		return eventHandlerSubscribe;
	}

	public String getEventHandlerSubscribeAPI() {
		return getApiUri(getEventHandlerUri(), getEventHandlerSubscribe());
	}

	public void setEventHandlerSubscribe(String eventHandlerSubscribe) {
		this.eventHandlerSubscribe = eventHandlerSubscribe;
	}

	public String getEventHandlerNotifyEvent() {
		return eventHandlerNotifyEvent;
	}

	public String getEventHandlerNotifyEventAPI() {
		return getApiUri(getEventHandlerUri(), getEventHandlerNotifyEvent());
	}

	public void setEventHandlerNotifyEvent(String eventHandlerNotifyEvent) {
		this.eventHandlerNotifyEvent = eventHandlerNotifyEvent;
	}

	public String getSelfUri() {
		return selfUri;
	}

	public void setSelfUri(String selfUri) {
		this.selfUri = selfUri;
	}

	public String getSelfEventArrived() {
		return selfEventArrived;
	}

	public void setSelfEventArrived(String selfEventArrived) {
		this.selfEventArrived = selfEventArrived;
	}

	public String getSelfEventArrivedAPI() {
		return getApiUri(getSelfUri(), getSelfEventArrived());
	}

	public String getApiUri(String url, String path) {
		return UriComponentsBuilder.fromUriString(url).path(path).build().toUri().toString();
	}

}
