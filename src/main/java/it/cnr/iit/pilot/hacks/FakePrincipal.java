package it.cnr.iit.pilot.hacks;

import java.security.Principal;
import java.util.Map;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

public class FakePrincipal implements Principal {


	private String getUsername(){
		try {
			OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
			Authentication userAuthentication = oAuth2Authentication.getUserAuthentication();
			Map<String, String> details = (Map<String, String>) userAuthentication.getDetails();
			String[] sub = details.get("sub").toString().split(":");
			System.out.println(sub);
			return sub[sub.length-1];
		} catch (Exception e) {
			e.printStackTrace();
			return "user";
		}
	} 

	private String name;

	public FakePrincipal(String name) {
		this.name = name;
	}

	public FakePrincipal() {
		name = getUsername();
	}

	@Override
	public String getName() {
		return name;
	}

}
