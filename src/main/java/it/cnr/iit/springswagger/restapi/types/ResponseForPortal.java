package it.cnr.iit.springswagger.restapi.types;

public class ResponseForPortal<T> {

	private String status;
	private T content;

	public ResponseForPortal() {
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public T getContent() {
		return content;
	}

	public void setContent(T content) {
		this.content = content;
	}

}
