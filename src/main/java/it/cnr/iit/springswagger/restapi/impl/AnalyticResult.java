package it.cnr.iit.springswagger.restapi.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

public class AnalyticResult {

	private String status;
	private List<byte[]> resultArray = new ArrayList<byte[]>();
	private Map<String, Object> additionalInformations = new HashMap<String, Object>();

	public AnalyticResult() {
	}

	public AnalyticResult(String errorStatus, String errorMessage) {
		status = errorStatus;
		setErrorMessage(errorMessage);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<byte[]> getResultArray() {
		return resultArray;
	}

	public void addToResultArray(byte[] content) {
		resultArray.add(content);
	}

	public void setResultArray(List<byte[]> resultArray) {
		this.resultArray = resultArray;
	}

	public Map<String, Object> getAdditionalInformations() {
		return additionalInformations;
	}

	public void setAdditionalInformations(Map<String, Object> additionalInformations) {
		this.additionalInformations = additionalInformations;
	}

	public void setErrorMessage(Object error) {
		this.additionalInformations.put("error", error);
	}

	public Object getErrorMessage() {
		return this.additionalInformations.get("error");
	}

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}

}