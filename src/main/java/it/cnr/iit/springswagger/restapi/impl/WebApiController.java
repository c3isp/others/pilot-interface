package it.cnr.iit.springswagger.restapi.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.core.util.FileUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import eu.c3isp.iai.subapi.types.AnalyticMetadata;
import eu.c3isp.iai.subapi.types.Result;
import eu.c3isp.isi.api.restapi.types.AnalyticResponse;
import eu.c3isp.isi.api.restapi.types.Dsa;
import eu.c3isp.isi.api.restapi.types.IAIParam;
import eu.c3isp.isi.api.restapi.types.SearchParams;
import eu.c3isp.isi.api.restapi.types.xacml.RequestContainer;
import io.swagger.annotations.ApiModel;
import it.cnr.iit.cert.Uploader;
import it.cnr.iit.certuploader.utility.MoveParameter;
import it.cnr.iit.common.attributes.AttributeIds;
import it.cnr.iit.common.types.Metadata;
import it.cnr.iit.common.utility.FileUtility;
import it.cnr.iit.database.common.CommonDatabase;
import it.cnr.iit.database.create.UploadTable;
import it.cnr.iit.database.history.HistoryTable;
import it.cnr.iit.database.history.OperationTable;
import it.cnr.iit.database.notification.NotificationTable;
import it.cnr.iit.database.notification.UserTable;
import it.cnr.iit.exceptions.UploadExceptions;
import it.cnr.iit.pilot.hacks.FakePrincipal;
import it.cnr.iit.springswagger.restapi.types.Constants;
import it.cnr.iit.utility.IAIUtility;
import it.cnr.iit.utility.ISIUtility;
import it.cnr.iit.utility.UtilsForControllers;

/****************** UPLOAD SINGLE FILE *****************************/
@ApiModel(value = "WebApiRest", description = "WebApi interface REST API")
@RestController
@RequestMapping("/v1")
@EnableAutoConfiguration
@SuppressWarnings("unchecked")
public class WebApiController<E> {
	private Logger log = Logger.getLogger(WebApiController.class.getName());

	@Value("${api.getDsaId.searchDsaId}")
	private String searchDsaId;
	@Value("${api.getDsaId.isi-api}")
	private String isiApi;
	@Value("${security.user.name}")
	private String username;
	@Value("${security.user.password}")
	private String password;
	@Value("${log-aggregator.config-file}")
	private String aggregatorConfigFile;
	@Value("${log-aggregator.config-path}")
	private String aggregatorConfigPath;

	@Autowired
	private Environment environment;
	@Autowired
	private CommonDatabase commonDatabase;
	@Autowired
	private Uploader uploader;
	@Qualifier("no_oauth") // change in "oauth" for using oidc
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private ISIUtility isiUtility;
	@Autowired
	private IAIUtility iaiUtility;
	@Autowired
	private UtilsForControllers utilsForControllers;

	public static String extension = "";
	public static Path uploadedFile = null;
	public static Path downloadedFile = null;

	@RequestMapping("/")
	public String test() {
		String str = "hello world";
		str = "<b>1st RESOURCE</b>: " + restTemplate.getForObject("http://localhost:9000/v1/test", String.class);
		str += "<br/><br/><b>2nd RESOURCE</b>: "
				+ restTemplate.getForObject("http://localhost:9000/v1/test", String.class);
		return str;
	}

	@RequestMapping(value = "/dbTest")
	public Boolean dbTest() {
		UserTable user = new UserTable();
		user.setName("Calogero");
		user.setSurname("Lo Bue");
		user.setEmail("calogero.lobue@iit.cnr.it");
		user.setOrganization("CNR");
		commonDatabase.createOrUpdateEntry(user);

		NotificationTable notification = new NotificationTable();
		notification.setType("Type");
		notification.setMessage("This is a message");

		Map<String, Object> params = new HashMap<>();
		params.put("name", "Calogero");
		List<UserTable> userTableList = (List<UserTable>) commonDatabase.getRowsByOneOrMoreParams(params,
				UserTable.class);
		System.out.println(userTableList.get(0).getEmail());

		return commonDatabase.createOrUpdateEntry(notification);
	}

	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public void uploadFile(@RequestParam("metadata") String uploadedMetadata,
			@RequestParam(value = "files", required = false) MultipartFile[] mfArray, Principal principal,
			HttpServletResponse httpResponse) throws IOException {

		principal = principal == null || principal.getName() == "unknown" ? new FakePrincipal() : principal;
		utilsForControllers.checkIfLogged(principal, httpResponse);

		Map<String, String> attributeMap = new HashMap<String, String>();

		ObjectMapper mapper = new ObjectMapper();
		try {
			Metadata metadata = mapper.readValue(uploadedMetadata, Metadata.class);

			attributeMap.put(AttributeIds.SUBJECT_ID, principal.getName());
			attributeMap.put(AttributeIds.ACTION_ID, "create");

			String organization = utilsForControllers.getOrganisationFromLDAP(principal, "ou=SME,ou=Pilots")
					+ utilsForControllers.getOrganisationFromLDAP(principal, "ou=Enterprise,ou=Pilots");
			if (organization.isEmpty()) {
				organization = utilsForControllers.getOrganisationFromMySQL(principal);
			}

			Dsa dsa = utilsForControllers.getDsaId(organization, principal, httpResponse).stream()
					.filter(s -> s.getTitle().equals(metadata.getDsaId())).findFirst().get();

			metadata.setDsaId(dsa.getId().replace(".xml", ""));
			metadata.setOrganization(organization);
			metadata.setExtension(FileUtils.getFileExtension(FileUtility.convert(mfArray[0])));
                        // Removed organization attribute because some PIP
                        // will insert during fattening request
			// attributeMap.put(AttributeIds.SUBJECT_ORGANIZATION, organization);
			attributeMap.put(AttributeIds.ACCESS_PURPOSE, dsa.getPurpose());

			log.severe("in uploadFile metadata = " + metadata.toString());

			List<String> responses = new ArrayList<String>();
			try {
				responses = uploader.uploadMultipleFiles(mfArray, metadata, attributeMap);
			} catch (UploadExceptions ue) {
				log.severe(ue.getMessage());
				httpResponse = utilsForControllers.prepareReturn(httpResponse, "ERROR", ue.getMessage());
				return;
			}

			if (mfArray.length > 1) {
				httpResponse = utilsForControllers.prepareReturn(httpResponse, "SUCCESS",
						"Your " + mfArray.length + " files have been upload correctly");
			} else {

				httpResponse = utilsForControllers.prepareReturn(httpResponse, "SUCCESS",
						new ObjectMapper().readValue(responses.get(0), Result.class));
			}
//			return ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson(responseForPortal));
		} catch (Exception e) {
			httpResponse = utilsForControllers.prepareReturn(httpResponse, "ERROR", e.getMessage());
			e.printStackTrace();
		}
	}

	@PostMapping(value = "/move_v1")
	public String move(@RequestBody MoveParameter param, Principal principal, HttpServletResponse httpResponse)
			throws JsonProcessingException, URISyntaxException {
		principal = principal == null || principal.getName() == "unknown" ? new FakePrincipal() : principal;
		utilsForControllers.checkIfLogged(principal, httpResponse);
		// LOGGER.info(searchParams);
		log.info(new ObjectMapper().writeValueAsString(param));
		String userLogged = principal.getName();
		String accessPurpose = param.getAccessPurpose();
		RequestContainer requestContainer = AnalyticMetadata.createAnalyticForMove(userLogged, accessPurpose)
				.convertToRequestContainer();

		String url = environment.getProperty(Constants.LOCAL_ISI)
				+ environment.getProperty(Constants.LOCAL_ISI + Constants.MOVE);

		// TODO
		// THIS MUST BE CHANGED IN A REQUEST WITH OAUTH2 WHEN OIDC IS INTEGRATED WITH
		// THE ISI
		String response = isiUtility.moveDpo(param.getDpoid(), requestContainer, restTemplate, url);
		return response;
	}

	/**************************
	 * SEARCH DPO ID
	 *
	 * @throws IOException
	 *******************************/

	@PostMapping(value = "/searchDpoId")
	public void searchDpoId(@RequestBody SearchParams searchParams, Principal principal,
			HttpServletResponse httpResponse) throws IOException {

		principal = principal == null || principal.getName() == "unknown" ? new FakePrincipal() : principal;
		utilsForControllers.checkIfLogged(principal, httpResponse);
		// TODO
		// THIS MUST BE CHANGED IN A REQUEST WITH OAUTH2 WHEN OIDC IS INTEGRATED WITH
		// THE ISI
		List<String> dpoList = isiUtility.searchDpo(searchParams, restTemplate);

		if (dpoList.isEmpty()) {
			log.log(Level.WARNING, "No DPOs have been found for the following request: " + searchParams);
			httpResponse = utilsForControllers.prepareReturn(httpResponse, "WARNING", "No DPOs have been found");

		} else {
			for (String s : dpoList) {
				log.info("DPO-ID: " + s);
			}
			httpResponse = utilsForControllers.prepareReturn(httpResponse, "SUCCESS", dpoList);
		}

	}

	/**************************
	 * STREAMING
	 *
	 * @throws IOException
	 **********************************/

	@RequestMapping(value = "/setStreamParams", method = RequestMethod.POST)
	public void setStreamParams(@RequestBody String json, Principal principal, HttpServletResponse httpResponse)
			throws IOException {

		try {

			principal = principal == null || principal.getName() == "unknown" ? new FakePrincipal() : principal;
			utilsForControllers.checkIfLogged(principal, httpResponse);

			File dir = new File(aggregatorConfigPath);

			if (!dir.exists()) {
				log.info("Creating directory " + dir);
				dir.mkdir();
			}

			File file = new File(aggregatorConfigPath + "/" + aggregatorConfigFile);

			if (file.exists()) {
				file.delete();
			}

			log.info("Creating file " + file);
			BufferedWriter br = new BufferedWriter(new FileWriter(file));
			br.write(json);
			br.close();

		} catch (Exception e) {
			e.printStackTrace();
			httpResponse = utilsForControllers.prepareReturn(httpResponse, "ERROR", e.getMessage());
		}

		httpResponse = utilsForControllers.prepareReturn(httpResponse, "SUCCESS",
				"Configuration file correctly created");
	}

	/**************************
	 * ANALYTIC
	 *
	 * @throws IOException
	 *******************************/

	@RequestMapping(value = "/runAnalytic", method = RequestMethod.POST, consumes = "text/html")
	public void runAnalytic(@RequestBody String json, Principal principal, HttpServletResponse httpResponse)
			throws IOException {
		principal = principal == null || principal.getName() == "unknown" ? new FakePrincipal() : principal;
		utilsForControllers.checkIfLogged(principal, httpResponse);

		log.severe("json from portal: " + json);

		if (json.isEmpty()) {
			httpResponse = utilsForControllers.prepareReturn(httpResponse, "ERROR", "Please fill the form before");
			return;
		}

		json = decodeFormData(json);
		IAIParam analyticParams = null;

		log.severe("decoded json from portal: " + json);

		try {

			analyticParams = new ObjectMapper().readValue(json, IAIParam.class);

			// FIXME: the name MUST be taken from the principal! Fix this when the
			// authentication works
//			if (principal.getName().equals("user") && !analyticParams.getSubjectId().contains("user")) {
//				analyticParams.getMetadata().put("subjectId", analyticParams.getSubjectId());
//			} else {
//			System.out.println("principal.getName()=" + principal.getName());
//			Map<String, String> attributeMap = new HashMap<String, String>();
//			attributeMap.put(AttributeIds.SUBJECT_ID, principal.getName());
//			attributeMap.put(AttributeIds.ACTION_ID, analyticParams.getServiceName());
//			attributeMap.put(AttributeIds.ACCESS_PURPOSE, analyticParams.getAdditionalAttribute().get("accessPurpose"));

			analyticParams.setSubjectId(principal.getName());
			String organization = utilsForControllers.getOrganisationFromLDAP(principal, "ou=SME,ou=Pilots")
					+ utilsForControllers.getOrganisationFromLDAP(principal, "ou=Enterprise,ou=Pilots");
			if (organization.isEmpty()) {
				organization = utilsForControllers.getOrganisationFromMySQL(principal);
			}
			analyticParams.getAdditionalAttribute().put("organisation", organization);
//			RequestContainer requestContainer = new RequestBuilder().build(attributeMap);
//			}

			json = new Gson().toJson(analyticParams);

			log.info("Calling analytic : " + analyticParams.getServiceName());
			AnalyticResponse analyticResponse = iaiUtility.runAnalytic(analyticParams.getServiceName(), json,
					restTemplate, principal.getName());

			if (!analyticParams.getServiceName().contains("legacy")) {
				dbCreateNewHistoryElement(analyticParams.getServiceName(), analyticResponse.getValue());
			}

			httpResponse = utilsForControllers.prepareReturn(httpResponse, "SUCCESS", analyticResponse);
			return;

		} catch (Exception e) {
			httpResponse = utilsForControllers.prepareReturn(httpResponse, "ERROR", e.getMessage());
			e.printStackTrace();
		}
	}

	private String decodeFormData(String formData) {

		formData = new String(Base64.getDecoder().decode(formData));
		try {
			formData = java.net.URLDecoder.decode(formData, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}

		return formData;
	}

	/******************** GRAPHS ************************/

	@GetMapping(value = "/convertToGraph/{api}/{dpoId}/")
	public String convertToGraph(@PathVariable("api") String api, @PathVariable("dpoId") String dpoId,
			Principal principal) {
		log.info("!!!! convert: " + api + " " + dpoId);

		return iaiUtility.convertToGraph(api, dpoId, restTemplate, principal.getName());
	}

	/********************
	 * HISTORY
	 *
	 * @throws IOException
	 ************************/

	@RequestMapping(value = "/history/getResult/{ticket}/", method = RequestMethod.POST)
	public void getResult(@PathVariable("ticket") String ticket, Principal principal, HttpServletResponse httpResponse)
			throws IOException {

		principal = principal == null || principal.getName() == "unknown" ? new FakePrincipal() : principal;
		utilsForControllers.checkIfLogged(principal, httpResponse);

		log.info("called /history/getDpo/ with ticket " + ticket);

		String jsonResult = iaiUtility.getResponseForTicket(ticket, restTemplate);

		log.severe("jsonResult=" + jsonResult);
		JSONObject json = new JSONObject(jsonResult);
		if (json.has("message")) {
			log.severe("json has not result property");

			if (json.get("message").toString().contains("running")) {
				httpResponse = utilsForControllers.prepareReturn(httpResponse, "WARNING",
						json.get("message").toString());
				setStatusToHistoryElement(ticket, "Running");
			}
			if (json.get("message").toString().contains("stopped")) {
				httpResponse = utilsForControllers.prepareReturn(httpResponse, "ERROR", json.get("message").toString());
				setStatusToHistoryElement(ticket, "Revoked");
			}
			return;
		}

		Result result = new ObjectMapper().readValue(jsonResult, Result.class);

		if (result == null) {
			setStatusToHistoryElement(ticket, "ERROR");
			httpResponse = utilsForControllers.prepareReturn(httpResponse, "ERROR",
					"An error occurred while getting the response with the ticket " + ticket);
			return;
		}

		log.severe("getResult gave the following result: ");
		result.getAdditionalProperties().entrySet().stream()
				.forEach(el -> log.severe("key=" + el.getKey() + ", value=" + el.getValue()));

		String dpoId = "";
		String[] searchedDpos = {};
		String[] permittedDpos = {};

		for (String key : result.getAdditionalProperties().keySet()) {
			String value = Optional.ofNullable(result.getAdditionalProperties().get(key)).orElse("");
			log.severe("inside the switch: key=" + key + ", value=" + value);

			switch (key.toLowerCase()) {
			case "warning":
			case "error":
				log.severe("in error warning case");
				setHttpResponse(httpResponse, key, value, ticket);
				return;
			case "dposid":
				dpoId = value;
				log.severe("in dposid case");
				break;
			case "searcheddpos":
				log.severe("in searcheddpos case");
				searchedDpos = result.getAdditionalProperties().get("searchedDpos").replace("[", "").replace("]", "")
						.replace(" ", "").split(",");
				break;
			case "permitteddpos":
				log.severe("in permitteddpos case");
				permittedDpos = result.getAdditionalProperties().get("permittedDpos").replace("[", "").replace("]", "")
						.replace(" ", "").split(",");
				break;
			default:
				log.severe("in default case");
				setHttpResponse(httpResponse, "ERROR", "Something went wrong...", ticket);
				return;
			}
		}

		HistoryTable historyTable = commonDatabase.getField("value", ticket, HistoryTable.class);
		System.out.println("historyTable row: " + new ObjectMapper().writeValueAsString(historyTable));

		historyTable.setSearchedDpos(searchedDpos);
		historyTable.setPermittedDpos(permittedDpos);

		System.out
				.println("historyTable element to be inserted: " + new ObjectMapper().writeValueAsString(historyTable));
		commonDatabase.createOrUpdateEntry(historyTable);

		if (dpoId.isEmpty()) {
			setStatusToHistoryElement(ticket, "Not found");
			httpResponse = utilsForControllers.prepareReturn(httpResponse, "NOT FOUND",
					"It was impossible to find the dpo associated with the ticket " + ticket);
			return;
		}

		log.info("DpoId associated with the result of the analytic: " + dpoId);

		dbUpdateTicketToDpo(ticket, dpoId);

		// TODO
		// THIS MUST BE CHANGED IN A REQUEST WITH OAUTH2 WHEN OIDC IS INTEGRATED WITH
		// THE ISI
		UploadTable uploadTable = new UploadTable();
		uploadTable.setAction("analytic");
		uploadTable.setDpo(dpoId);
		uploadTable.setIsStixed("true");
		uploadTable.setFilename(UUID.randomUUID().toString());
		uploadTable.setExstension("rst");
		commonDatabase.createOrUpdateEntry(uploadTable);

		String response = new String(isiUtility.readDpo(dpoId, principal, restTemplate));
		if (response == null || response.isEmpty()) {
			httpResponse = utilsForControllers.prepareReturn(httpResponse, "ERROR", "Something went wrong...");
			return;
		}
		log.info("response : " + response);

		httpResponse = utilsForControllers.prepareReturn(httpResponse, "SUCCESS", response);
	}

	private void setHttpResponse(HttpServletResponse httpResponse, String status, String value, String ticket)
			throws IOException {
		httpResponse = utilsForControllers.prepareReturn(httpResponse, status, value);

		if (value.toLowerCase().contains("stopped")) {
			log.severe("in stopped if");
			setStatusToHistoryElement(ticket, "Stopped");
			return;
		} else if (value.toLowerCase().contains("running")) {
			log.severe("in running if");
			setStatusToHistoryElement(ticket, "Running");
			return;
		} else if (value.toLowerCase().contains("revoke")) {
			log.severe("in revoked if");
			setStatusToHistoryElement(ticket, "Revoked");
			return;
		} else if (value.toLowerCase().contains("no results")) {
			log.severe("in no results 1 if");
			setStatusToHistoryElement(ticket, "No results");
			return;
		} else if (value.toLowerCase().contains("wrong")) {
			log.severe("in no results 2 if");
			setStatusToHistoryElement(ticket, "No results");
			return;
		} else if (value.toLowerCase().contains("no dpos")) {
			log.severe("in no dpos if");
			setStatusToHistoryElement(ticket, "No dpos found");
//		DpoResponses dpoResponses = new ObjectMapper().readValue(response, DpoResponses.class);
			httpResponse = utilsForControllers.prepareReturn(httpResponse, "WARNING",
					"Could not run the analytic because there were no available DPOs");
			return;
		}

	}

	@PostMapping(value = "/history/deleteRow")
	public void deleteRow(@RequestBody String id, Principal principal, HttpServletResponse httpResponse)
			throws IOException {

		System.out.println("received id: " + id);
		if (!commonDatabase.deleteEntry(Integer.parseInt(id), HistoryTable.class)) {
			httpResponse = utilsForControllers.prepareReturn(httpResponse, "ERROR", "Something went wrong...");
			return;
		}

		httpResponse = utilsForControllers.prepareReturn(httpResponse, "SUCCESS", "Entry correctly deleted");
	}

	private void dbCreateNewHistoryElement(String operationName, String value) {

		Map<String, Object> map = new HashMap<>();
		map.put("name", operationName);

		List<OperationTable> operationTableList = (List<OperationTable>) commonDatabase.getRowsByOneOrMoreParams(map,
				OperationTable.class);

		if (operationTableList.size() == 0) {
			log.info("No results found");
		}

		OperationTable operationTable = operationTableList.get(0);
		int operation_id = operationTable.getId();

		HistoryTable historyTable = new HistoryTable();
		historyTable.setOperationId("" + operation_id);
		historyTable.setStatus("running");
		historyTable.setType("ticket");
		historyTable.setUserId("0");
		historyTable.setTimestamp(new Timestamp(new Date().getTime()));
		historyTable.setValue(value);

		commonDatabase.createOrUpdateEntry(historyTable);
	}

	private void setStatusToHistoryElement(String value, String status) {

		Map<String, Object> map = new HashMap<>();
		map.put("value", value);

		List<HistoryTable> historyTableList = (List<HistoryTable>) commonDatabase.getRowsByOneOrMoreParams(map,
				HistoryTable.class);

		if (historyTableList.size() > 1) {
			log.info("Ambiguous result: more then one entries have been found");
			return;
		}

		HistoryTable historyTable = historyTableList.get(0);

		historyTable.setStatus(status);
		commonDatabase.createOrUpdateEntry(historyTable);
	}

	private void dbUpdateTicketToDpo(String ticket, String dpoId) {

		Map<String, Object> map = new HashMap<>();
		map.put("value", ticket);

		List<HistoryTable> historyTableList = (List<HistoryTable>) commonDatabase.getRowsByOneOrMoreParams(map,
				HistoryTable.class);

		if (historyTableList.size() == 0) {
			log.info("No results found");
			return;
		}
		if (historyTableList.size() > 1) {
			log.info("Ambiguous result: more then one entries have been found");
			return;
		}

		HistoryTable historyData = historyTableList.get(0);

		log.info("history row old : " + (new Gson().toJson(historyData).toString()));
		historyData.setType("dpoId");
		historyData.setValue(dpoId);
		historyData.setStatus("valid");

		log.info("history row new : " + (new Gson().toJson(historyData).toString()));
		commonDatabase.createOrUpdateEntry(historyData);
	}

	/************************* OTHERS **************************/

	@GetMapping(value = "/analyticAdvancedParams/{analytic}")
	public String analyticAdvancedParams(@PathVariable("analytic") String analytic) {

		switch (analytic) {
		case "bruteForceAttacksDetection":
			return "<div id=\"bruteforce-parameter\" class=\"form-group\">\r\n\t\t\t\t\t \t<div class=\"row mt-2\" id=\"param-row-0\">\r\n\t\t\t\t\t \t\t<div class=\"col-sm-2\">\r\n\t\t\t\t\t \t\t\t<label class=\"col-form-label\">MinFractionFailures:</label>\r\n\t\t\t\t\t \t\t</div>\r\n\t\t\t\t\t \t\t<div class=\"col-sm-3\">\r\n\t\t\t\t\t \t\t\t<input type=\"number\" name=\"minFractionFailures\" class=\"form-control\" min=\"0\" max=\"1\" step=\"0.01\">\r\n\t\t\t\t\t \t\t</div>\r\n\t\t\t\t\t \t\t<div class=\"col-sm-2\"></div>\r\n\t\t\t\t\t \t\t<div class=\"col-sm\">\r\n\t\t\t\t\t \t\t\t<small class=\"text-muted\"> If the percentage of failures is\r\n\t\t\t\t\t \t\t\t\tequal or greater than this value, then an attack has been detected.\r\n\t\t\t\t\t \t\t\t</small>\r\n\t\t\t\t\t \t\t</div>\r\n\t\t\t\t\t \t</div>\r\n\t\t\t\t\t\t<div class=\"row mt-2\" id=\"param-row-1\">\r\n\t\t\t\t\t\t\t<div class=\"col-sm-2\">\r\n\t\t\t\t\t\t\t\t<label class=\"col-form-label\">MinFailures:</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"col-sm-3\">\r\n\t\t\t\t\t\t\t\t<input type=\"number\" name=\"minFailures\" class=\"form-control\">\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"col-sm-2\"></div>\r\n\t\t\t\t\t\t\t<div class=\"col-sm\">\r\n\t\t\t\t\t\t\t\t<small class=\"text-muted\"> Minimum number of failures to\r\n\t\t\t\t\t\t\t\t\tsignal a possible malicious brute force attacker. \r\n\t\t\t\t\t\t\t\t</small>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t    </div>\r\n\t\t\t\t\t</div>";
		case "spamEmailClassify":
			return "<div id=\"spamEmail-parameter\" class=\"form-group\">\r\n\t\t\t\t\t\t\t\t<div class=\"row mt-2\" id=\"param-row-0\">\r\n\t\t\t\t\t\t\t\t\t<div class=\"col-sm-2\">\r\n\t\t\t\t\t\t\t\t\t\t<label class=\"col-form-label\">Features Only:</label>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t<div class=\"col-sm-4\">\r\n\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"features_only\">\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t<div class=\"col-sm-1\"></div>\r\n\t\t\t\t\t\t\t\t\t<div class=\"col-sm\">\r\n\t\t\t\t\t\t\t\t\t\t<small class=\"text-muted\">Put this to <i>true</i> if you want to run the analytic only on a set of a features already extracted.</small>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>";
		case "spamEmailClusterer":
			return "<div id=\"spamClusterer-parameter\" class=\"form-group\"> \r\n"
					+ "  <div class=\"row mt-2\" id=\"param-row-0\">\r\n" + "    <div class=\"col-sm-2\"> \r\n"
					+ "      <label class=\"col-form-label\">Purity:</label> \r\n" + "    </div> \r\n"
					+ "    <div class=\"col-sm-3\"> \r\n"
					+ "      <input type=\"number\" name=\"purity\" class=\"form-control\" min=\"0\" max=\"1\" step=\"0.01\"> \r\n"
					+ "    </div> \r\n" + "    <div class=\"col-sm-2\"></div> \r\n" + "    <div class=\"col-sm\"> \r\n"
					+ "      <small class=\"text-muted\"> INSERT A DESCRIPTION HERE </small> \r\n" + "    </div> \r\n"
					+ "  </div>\r\n" + "  <div class=\"row mt-2\" id=\"param-row-1\">\r\n"
					+ "    <div class=\"col-sm-2\">\r\n"
					+ "      <label class=\"col-form-label\">MinElementsThreshold:</label>\r\n" + "    </div>\r\n"
					+ "    <div class=\"col-sm-3\">\r\n"
					+ "      <input type=\"number\" name=\"minElementsThreshold\" class=\"form-control\" min=\"1\" max=\"10\" step=\"1\">\r\n"
					+ "    </div>\r\n" + "    <div class=\"col-sm-2\"></div>\r\n" + "    <div class=\"col-sm\">\r\n"
					+ "      <small class=\"text-muted\"> INSERT A DESCRIPTION HERE </small>\r\n" + "    </div>\r\n"
					+ "  </div>\r\n" + "</div>";
		case "connToMaliciousHosts":
			return "<div class=\"form-group row mt-4 mb-4\">\r\n"
					+ "  <label class=\"col-sm-2 col-form-label\">Analysis Method</label>\r\n"
					+ "  <div class=\"col-sm-3\">\r\n"
					+ "    <select class=\"custom-select\" id=\"analysisMethod\" name=\"analysisMethod\" aria-describedby=\"analysisMethod-help\" required>\r\n"
					+ "      <option value=\"BLACK_LIST_LOW\">Low</option>\r\n"
					+ "      <option value=\"BLACK_LIST_MEDIUM\">Medium</option>\r\n"
					+ "      <option value=\"BLACK_LIST_HIGH\">High</option>\r\n"
					+ "      <option value=\"BLACK_LIST_FULL\">Full</option>\r\n" + "    </select>\r\n" + "  </div>\r\n"
					+ "  <div class=\"col-sm-2\"></div>\r\n" + "  <div class=\"col-sm\">\r\n"
					+ "    <small id=\"event-type-help\" class=\"text-muted\">\r\n"
					+ "      Select the depth level of the comparison between your IPs list and the black list.\r\n"
					+ "    </small>\r\n" + "  </div>	\r\n" + "</div>";
		case "correlateVulnerabilityAttack":
			return "<div id=\"correlateVulnerabilityAttack-parameter\" class=\"form-group\"> \r\n"
					+ "  <div class=\"row mt-2\" id=\"param-row-0\">\r\n" + "    <div class=\"col-sm-2\"> \r\n"
					+ "      <label class=\"col-form-label\">Application:</label> \r\n" + "    </div> \r\n"
					+ "    <div class=\"col-sm-3\"> \r\n"
					+ "      <input type=\"text\" name=\"application\" class=\"form-control\" placeholder=\"IBM PowerKVM 2.1.0.2\"> \r\n"
					+ "    </div> \r\n" + "    <div class=\"col-sm-2\"></div> \r\n" + "    <div class=\"col-sm\"> \r\n"
					+ "      <small class=\"text-muted\">Insert the name of the application of which you want to discover the vulnerabilities, exploits and mitigations</small> \r\n"
					+ "    </div> \r\n" + "</div>";
		default:
			return "";
		}
	}

}
