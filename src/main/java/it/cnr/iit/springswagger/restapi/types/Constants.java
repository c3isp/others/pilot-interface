package it.cnr.iit.springswagger.restapi.types;

/**
 * Collection of properties that will be present in the application.properties
 * 
 * @author antonio
 *
 */
public class Constants {
	public static final String LOCAL_ISI = "isi.local";
	public static final String CENTRALIZED_ISI = "isi.global";
	public static final String CREATE = ".create";
	public static final String MOVE = ".move";
	public static final String DPO_STORAGE = "DPO_STORAGE";
	public static final String SEARCH_DSA = "SEARCH_DSA";
	public static final String SEARCH_DPO = ".search";

}
