package it.cnr.iit.springswagger.restapi.types;

public class SearchObject {

	private String searchLocation;
	private String searchDataType;
	private String searchOrganizationCS;
	private String searchOrganization;
	private String searchDpoId;
	private String searchDpoIdCs;
	private String searchStartTimeCS;
	private String searchStartTime;
	private String searchEndTimeCS;
	private String searchEndTime;
	private String searchSecurityLevelCS;
	private String searchSecurityLevel;
	private String searchCombiningRule;

	public SearchObject() {
	}

	public String getSearchLocation() {
		return searchLocation;
	}

	public void setSearchLocation(String searchLocation) {
		this.searchLocation = searchLocation;
	}

	public String getSearchDataType() {
		return searchDataType;
	}

	public void setSearchDataType(String searchDataType) {
		this.searchDataType = searchDataType;
	}

	public String getSearchOrganizationCS() {
		return searchOrganizationCS;
	}

	public void setSearchOrganizationCS(String searchOrganizationCS) {
		this.searchOrganizationCS = searchOrganizationCS;
	}

	public String getSearchOrganization() {
		return searchOrganization;
	}

	public void setSearchOrganization(String searchOrganization) {
		this.searchOrganization = searchOrganization;
	}

	public String getSearchDpoIdCs() {
		return searchDpoIdCs;
	}

	public String getSearchDpoId() {
		return searchDpoId;
	}

	public void setSearchDpoId(String searchDpoId) {
		this.searchDpoId = searchDpoId;
	}

	public String getSearchStartTimeCS() {
		return searchStartTimeCS;
	}

	public void setSearchStartTimeCS(String searchStartTimeCS) {
		this.searchStartTimeCS = searchStartTimeCS;
	}

	public String getSearchStartTime() {
		return searchStartTime;
	}

	public void setSearchStartTime(String searchStartTime) {
		this.searchStartTime = searchStartTime;
	}

	public String getSearchEndTimeCS() {
		return searchEndTimeCS;
	}

	public void setSearchEndTimeCS(String searchEndTimeCS) {
		this.searchEndTimeCS = searchEndTimeCS;
	}

	public String getSearchEndTime() {
		return searchEndTime;
	}

	public void setSearchEndTime(String searchEndTime) {
		this.searchEndTime = searchEndTime;
	}

	public String getSearchSecurityLevelCS() {
		return searchSecurityLevelCS;
	}

	public void setSearchSecurityLevelCS(String searchSecurityLevelCS) {
		this.searchSecurityLevelCS = searchSecurityLevelCS;
	}

	public String getSearchSecurityLevel() {
		return searchSecurityLevel;
	}

	public void setSearchSecurityLevel(String searchSecurityLevel) {
		this.searchSecurityLevel = searchSecurityLevel;
	}

	public String getCombiningRule() {
		return searchCombiningRule;
	}

	public void setSearchCombiningRule(String searchCombiningRule) {
		this.searchCombiningRule = searchCombiningRule;
	}
}
