package it.cnr.iit.springswagger.restapi.impl;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.c3isp.iai.subapi.fhe.FHEResponse;
import eu.c3isp.iai.subapi.fhe.FHEResults;
import eu.c3isp.iai.subapi.types.DpoResponses;
import eu.c3isp.isi.api.restapi.types.metadata.Metadata;
import it.cnr.iit.database.common.CommonDatabase;
import it.cnr.iit.database.create.UploadTable;
import it.cnr.iit.database.history.HistoryTable;
import it.cnr.iit.database.history.OperationTable;
import it.cnr.iit.pilot.hacks.FakePrincipal;
import it.cnr.iit.utility.IAIUtility;
import it.cnr.iit.utility.ISIUtility;
import it.cnr.iit.utility.UtilsForControllers;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * This class is in charge of showing the jsp page related to uploader. jsp.
 * <p>
 * In order to implement such functionality we've followed the <a href=
 * "http://www.springboottutorial.com/creating-web-application-with-spring-boot">tutorial</a>:
 * </p>
 *
 * @author antonio, calogero, alessandro
 *
 */

@Controller
@EnableOAuth2Client
public class WebController {

	@Qualifier("no_oauth")
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private CommonDatabase commonDatabase;

	@Autowired
	private UtilsForControllers utilsForControllers;

	@Autowired
	private ISIUtility isiUtility;

	@Autowired
	private IAIUtility iaiUtility;

	private Logger log = Logger.getLogger(WebController.class.getName());

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String indexRoot(ModelMap model, Principal user) {
//		restTemplate.getForObject("http://localhost:9000/v1/getResourceIaiAuthentication", String.class);
		user = user == null || user.getName().equals("unknown") ? new FakePrincipal() : user;
		model.put("username", user.getName());
		model.put("page", "home-content");
		return "view";
	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(ModelMap model, Principal user) {
		user = user == null || user.getName().equals("unknown") ? new FakePrincipal() : user;
//		restTemplate.getForObject("http://localhost:9000/v1/getResourceIaiAuthentication", String.class);
		model.put("username", user.getName());
		model.put("page", "home-content");
		return "view";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(ModelMap model) {
		System.out.println("LOGIN CALLED");
		model.put("page", "login1");
		return "view";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(ModelMap model) {
		System.out.println("LOGOUT CALLED");
		model.put("page", "home-content");
		return "view";
	}

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String test(ModelMap model, Principal user) {
		user = user == null || user.getName().equals("unknown") ? new FakePrincipal() : user;
		model.put("username", user.getName());
		model.put("page", "home-content");
		return "view2";
	}

	@RequestMapping(value = "/upload", method = RequestMethod.GET)
	public String upload(ModelMap model, Principal user, HttpServletResponse httpResponse) throws IOException {
		user = user == null || user.getName().equals("unknown") ? new FakePrincipal() : user;
		String organisation = utilsForControllers.getOrganisationFromLDAP(user, "ou=SME,ou=Pilots")
				+ utilsForControllers.getOrganisationFromLDAP(user, "ou=Enterprise,ou=Pilots");
		if (organisation.isEmpty()) {
			organisation = utilsForControllers.getOrganisationFromMySQL(user);
		}

		System.out.println("organisation: " + organisation);
		List<String> dsaList = utilsForControllers.getDsaId(organisation, user, httpResponse).stream()
				.map(s -> s.getTitle()).collect(Collectors.toList());
		Collections.reverse(dsaList);

		model.put("page", "upload-content");
		model.put("dsaList", dsaList);
		model.put("organisation", organisation);
		model.put("username", user.getName());
		return "view";
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public String search(ModelMap model, Principal user) {
		user = user == null || user.getName().equals("unknown") ? new FakePrincipal() : user;
		model.put("username", user.getName());
		model.put("page", "search-content");
		return "view";
	}

	@RequestMapping(value = "/analytic", method = RequestMethod.GET)
	public String analytic(ModelMap model, Principal user, HttpServletResponse httpResponse) throws IOException {
		user = user == null || user.getName().equals("unknown") ? new FakePrincipal() : user;
		String organisation = utilsForControllers.getOrganisationFromLDAP(user, "ou=SME,ou=Pilots")
				+ utilsForControllers.getOrganisationFromLDAP(user, "ou=Enterprise,ou=Pilots");
		if (organisation.isEmpty()) {
			organisation = utilsForControllers.getOrganisationFromMySQL(user);
		}

		System.out.println("organisation: " + organisation);

		ArrayList<String> analyticName = new ArrayList<>();
		ArrayList<String> analyticValue = new ArrayList<>();

		switch (organisation) {
		case "SPARTACompany1":
		case "SPARTACompany2":
		case "SPARTACompany3":
		case "ISCOM-MISE":
			analyticName.add("Spam Email Classify");
			analyticName.add("Spam Email Clusterer");
			analyticName.add("Spam Email Detect");
			analyticName.add("Correlate Vulnerability To Attack");

			analyticValue.add("spamEmailClassify");
			analyticValue.add("spamEmailClusterer");
			analyticValue.add("spamEmailDetect");
			analyticValue.add("correlateVulnerabilityAttack");
			break;
		case "ISP@CNR":
			// analyticName.add("Spam Email Clusterer");
			analyticName.add("Detect DGA");
			analyticName.add("Match DGA");
			analyticName.add("Brute Force Attacks Detection");
			analyticName.add("Connection To Malicious Hosts");
			analyticName.add("Spam Email Classify");
			analyticName.add("Spam Email Clusterer");
			analyticName.add("Spam Email Detect");
			analyticName.add("Correlate Vulnerability To Attack");
			analyticName.add("Legacy Analytic");

			// analyticName.add("Correlate Vulnerability To Attack");

			// analyticValue.add("spamEmailClusterer");
			analyticValue.add("detectDGA");
			analyticValue.add("matchDGA");
			analyticValue.add("bruteForceAttacksDetection");
			analyticValue.add("connToMaliciousHosts");
			analyticValue.add("spamEmailClassify");
			analyticValue.add("spamEmailClusterer");
			analyticValue.add("spamEmailDetect");
			analyticValue.add("correlateVulnerabilityAttack");
			analyticValue.add("legacyAnalytics");
			// analyticValue.add("correlateVulnerabilityAttack");

			break;
		case "SME":
			analyticName.add("Find Vulnerability");
			analyticName.add("Find Malware");
			analyticName.add("Find Attacking Hosts");

			analyticValue.add("findVulnerability");
			analyticValue.add("findMalware");
			analyticValue.add("findAttackingHosts");
			break;

		case "Enterprise":
			analyticName.add("Legacy Analytic Saturn");
			analyticName.add("PredictAttackTrend");

			analyticValue.add("legacyAnalyticsSaturn");
			analyticValue.add("predictAttackTrend");
			break;
		default:
			analyticName.add("Spam Email Classify");
			analyticName.add("Spam Email Clusterer");
			analyticName.add("Spam Email Detect");
//			analyticName.add("Legacy Analytic Saturn");
//			analyticName.add("PredictAttackTrend");
//			analyticName.add("Find Vulnerability");
//			analyticName.add("Find Malware");
//			analyticName.add("Find Attacking Hosts");
//			analyticName.add("Correlate Vulnerability Attack");
			analyticName.add("Detect DGA");
			analyticName.add("Match DGA");
			analyticName.add("Brute Force Attacks Detection");
			analyticName.add("Connection To Malicious Hosts");
			analyticName.add("Correlate Vulnerability To Attack");

			analyticValue.add("spamEmailClassify");
			analyticValue.add("spamEmailClusterer");
			analyticValue.add("spamEmailDetect");
			analyticValue.add("detectDGA");
			analyticValue.add("matchDGA");
			analyticValue.add("bruteForceAttacksDetection");
			analyticValue.add("connToMaliciousHosts");
			analyticValue.add("correlateVulnerabilityAttack");
//			analyticValue.add("findVulnerability");
//			analyticValue.add("findMalware");
//			analyticValue.add("findAttackingHosts");
//			analyticValue.add("correlateVulnerabilityAttack");
//			analyticValue.add("legacyAnalyticsSaturn");
//			analyticValue.add("predictAttackTrend");

			break;
		}

		model.put("page", "analytic-content");
		model.put("analyticValue", analyticValue);
		model.put("analyticName", analyticName);
		model.put("username", user.getName());
		return "view";
	}

	@RequestMapping(value = "/dsaManager", method = RequestMethod.GET)
	public String dsaManager(ModelMap model) {
		model.put("page", "dsa-manager-content");
		return "view";
	}

	@RequestMapping(value = "/move", method = RequestMethod.GET)
	public String move(ModelMap model, Principal user) {
		user = user == null || user.getName().equals("unknown") ? new FakePrincipal() : user;
		try {
			List<Metadata> dpoIds = searchAllDpo();
			model.put("localDpo", dpoIds);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.put("username", user.getName());
		model.put("page", "move-content");
		return "view";
	}

	private List<Metadata> searchAllDpo() throws JsonParseException, JsonMappingException, IOException {
		// List<Metadata> dpos = new ArrayList<>();
		// // String isiUrl = env.getProperty(Constants.LOCAL_ISI);
		// String isiUrl = environment.getProperty(Constants.LOCAL_ISI)
		// + environment.getProperty(Constants.LOCAL_ISI + Constants.SEARCH_DPO);
		// isiUrl += "/true";
		// List<String> dpoIds = utility.searchDpo(SearchParams.createEmpty(),
		// oauthRestTemplate, isiUrl);
		// for (String string : dpoIds) {
		// dpos.add(new ObjectMapper()
		// .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
		// .readValue(string, Metadata.class));
		// }
		// return dpos;
		return null;
	}

	@RequestMapping(value = "/read", method = RequestMethod.GET)
	public String read(ModelMap model, Principal user) {
		user = user == null || user.getName().equals("unknown") ? new FakePrincipal() : user;
		model.put("username", user.getName());
		model.put("page", "read-content");
		return "view";
	}

	@RequestMapping(value = "/history", method = RequestMethod.GET)
	public String history(ModelMap model, Principal user) {
		user = user == null || user.getName().equals("unknown") ? new FakePrincipal() : user;

		model.put("page", "history-content");
		model.put("username", user.getName());

		// Map of operation_id and analytic name
		Map<Integer, String> operationMap = commonDatabase.getWholeTable(OperationTable.class).stream()
				.map(o -> ((OperationTable) o))
				.collect(Collectors.toMap(OperationTable::getId, OperationTable::getName));

		// HistoryTable with analytic name instead of operation_id
		LinkedHashMap<HistoryTable, String> historyMap = commonDatabase.getWholeTable(HistoryTable.class).stream()
				.map(h -> ((HistoryTable) h))
				.collect(Collectors.toMap(h -> h, h -> operationMap.get(Integer.parseInt(h.getOperationId())),
						(a, b) -> a, () -> new LinkedHashMap<>()));

		// Reversing previous map in decreasing order
		model.put("historyMap", reverseMap(historyMap));
		return "view";
	}

	public static <T, Q> LinkedHashMap<T, Q> reverseMap(LinkedHashMap<T, Q> toReverse) {
		LinkedHashMap<T, Q> reversedMap = new LinkedHashMap<>();
		List<T> reverseOrderedKeys = new ArrayList<>(toReverse.keySet());
		Collections.reverse(reverseOrderedKeys);
		reverseOrderedKeys.forEach((key) -> reversedMap.put(key, toReverse.get(key)));
		return reversedMap;
	}

	@RequestMapping(value = "/dashboard/{id}/", method = RequestMethod.GET)
	public String dashboard(@PathVariable("id") int id, ModelMap model, Principal user) {
		user = user == null || user.getName().equals("unknown") ? new FakePrincipal() : user;
		final Principal finalUser = user;
		model.put("page", "dashboard");
		model.put("username", user.getName());

		HistoryTable historyData = commonDatabase.getField("id", String.valueOf(id), HistoryTable.class);
		String[] searchedDpos = historyData.getSearchedDpos();
		String[] permittedDpos = historyData.getPermittedDpos();
		List<String> status = new ArrayList<>();

		for (String s1 : searchedDpos) {
			status.add("Denied");
			for (String s2 : permittedDpos) {
				if (s1.equals(s2)) {
					status.remove(status.size() - 1);
					status.add("Permitted");
					break;
				}
			}
		}

		model.put("searchedDpos", historyData.getSearchedDpos());
		model.put("status", status);

		if (historyData != null) {
			OperationTable operationData = commonDatabase.getField("operation_id", historyData.getOperationId(),
					OperationTable.class);

			model.put("data", historyData);
			model.put("operationName", operationData.getName());

			String dpoId;
			try {
				if (historyData.isTicket()) {
					System.out.println("historyData.isTicket()=true");
					Object dpoResponses = iaiUtility.getResponseForTicket(historyData.getValue(), restTemplate);
					if (dpoResponses != null && dpoResponses instanceof DpoResponses) {
						System.out.println("dpoResponses=" + new ObjectMapper().writeValueAsString(dpoResponses));
						dpoId = ((DpoResponses) dpoResponses).getDpoId();
						// update table element after getting the dpoId from the ticket
						historyData.setType("dpoId");
						historyData.setValue(dpoId);
						commonDatabase.createOrUpdateEntry(historyData);

					} else {
						model.put("error", "error trying to access ticket: " + historyData.getValue());
						return "view";
					}
				} else {
					dpoId = historyData.getValue();
					model.put("dpoId", dpoId);
				}

				log.info("result dpoId: " + dpoId);

				String result = new String(isiUtility.readDpo(dpoId, finalUser, restTemplate));
				if (result.contains("fheResult")) {
					FHEResponse fheResponse = new ObjectMapper().readValue(result, FHEResponse.class);
					List<FHEResults> fheResultList = fheResponse.getFheResults();

					List<String> fheIp = fheResultList.stream()
							.map(f -> new String(isiUtility.readDpo(f.getDpoIp(), finalUser, restTemplate)))
							.collect(Collectors.toList());
					List<String> fheResult = fheResultList.stream()
							.map(f -> new String(isiUtility.readDpo(f.getDpoResult(), finalUser, restTemplate)))
							.collect(Collectors.toList());

					model.put("fheIp", fheIp);
					model.put("fheResult", fheResult);
				}
				model.put("result", result);
				ProcessResultForModel processResultForModel = new ProcessResultForModel();
				processResultForModel.process(model);
			} catch (Exception e) {
				model.put("error", "error processing id " + id + ". message: " + e.getMessage());
				e.printStackTrace();
			}
		} else {
			model.put("error", "id " + id + " is not valid");
		}

		return "view";
	}

	private HttpHeaders getHeaders(UploadTable uploadRow) {
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=" + uploadRow.getFilename() + "." + uploadRow.getExstension());
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");
		return headers;
	}

	@RequestMapping(value = "/download/{id}/", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<?> download(@PathVariable("id") String id, Principal user) {
		user = user == null || user.getName().equals("unknown") ? new FakePrincipal() : user;

		byte[] content = isiUtility.readDpo(id, user, restTemplate);
		if (content == null) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		UploadTable uploadRow = commonDatabase.getField("dpo", id, UploadTable.class);
		HttpHeaders headers = getHeaders(uploadRow);
		return ResponseEntity.ok().headers(headers).contentLength(content.length)
				.contentType(MediaType.parseMediaType("application/octect-stream")).body(content);
	}

	@RequestMapping(value = "/export/{id}/{target}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> export(
                                    @PathVariable("id") String id, 
                                    @PathVariable("target") String target,
                                    @RequestParam(name = "exportParams", required = false) String exportParams,
                                    Principal user
                                ) {
                log.info(String.format("Exporting %s to %s [%s]", id, target, exportParams == null ? "null" : exportParams ));
                
		user = user == null || user.getName().equals("unknown") ? new FakePrincipal() : user;

		ResponseEntity<String> content = isiUtility.exportDpo(id,  target, exportParams, user, restTemplate);
		if (content == null) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		return content;
	}

	@RequestMapping(value = "/streaming", method = RequestMethod.GET)
	public String streaming(ModelMap model, Principal user, HttpServletResponse httpResponse) throws IOException {
		user = user == null || user.getName().equals("unknown") ? new FakePrincipal() : user;

		String organisation = utilsForControllers.getOrganisationFromLDAP(user, "ou=SME,ou=Pilots")
				+ utilsForControllers.getOrganisationFromLDAP(user, "ou=Enterprise,ou=Pilots");
		if (organisation.isEmpty()) {
			organisation = utilsForControllers.getOrganisationFromMySQL(user);
		}

		System.out.println("organisation: " + organisation);
		List<String> dsaList = utilsForControllers.getDsaId(organisation, user, httpResponse).stream()
				.map(s -> s.getTitle()).collect(Collectors.toList());

		model.put("dsaList", dsaList);
		model.put("organisation", organisation);
		model.put("page", "streaming-content");
		model.put("username", user.getName());
		return "view";
	}

}
