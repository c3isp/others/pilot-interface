package it.cnr.iit.springswagger.restapi.impl;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.ui.ModelMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jcustenborder.cef.CEFParser;
import com.github.jcustenborder.cef.CEFParserFactory;
import com.github.jcustenborder.cef.Message;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import eu.c3isp.iai.subapi.fhe.FHEResponse;
import it.cnr.iit.c3isp.mailiai.json.ClassForEmailID;
import it.cnr.iit.c3isp.mailiai.json.ClassificationResult;

public class ProcessResultForModel {
	private static Logger log = Logger.getLogger(ProcessResultForModel.class.getName());

	private CEFParser cefParser = CEFParserFactory.create();

	public void process(ModelMap model) {
		if (!parseResult(model)) {
			log.info("error processing result for model");
			return;
		}

		String opname = (String) model.get("operationName");
		log.info("process dpo for operation : " + opname);

		switch (opname) {
		case "spamEmailClassify":
			mailClassify(model);
			break;
		case "spamEmailClusterer":
			mailClusterer(model);
			break;
		case "spamEmailDetect":
			mailDetect(model);
			break;
		case "detectDGA":
		case "matchDGA":
			DGA(model);
			break;
		case "bruteForceAttacksDetection":
			bruteForce(model);
			break;
		case "connToMaliciousHosts":
			connToMaliciousHosts(model);
			break;
		case "correlateVulnerabilityAttack":
			correlateVulnerabilityAttack(model);
		default:
			log.info("unknown operation name");
		}
	}

	private boolean parseResult(ModelMap model) {
		if (model.containsKey("result")) {
			String result = (String) model.get("result");
			String resultParsed = null;

			if (result != null) {
				if (result.startsWith("{")) {
					JsonElement el = new JsonParser().parse(result);
					resultParsed = new GsonBuilder().setPrettyPrinting().create().toJson(el);
				} else {
					resultParsed = String.join("\n", result.split("\r\n|\r|\n"));
				}

				model.put("resultLines", resultParsed.split("\r\n|\r|\n").length);
				model.put("result", resultParsed);

				return true;
			}
		}

		return false;
	}

	private void mailClassify(ModelMap model) {
		try {
			AnalyticResult analyticResult = new ObjectMapper().readValue((String) model.get("result"),
					AnalyticResult.class);
			ClassificationResult classificationResult = new ObjectMapper().readValue(
					new String(analyticResult.getResultArray().get(0), StandardCharsets.UTF_8),
					ClassificationResult.class);

			model.put("resultListSpamClassify", classificationResult.getList());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void mailClusterer(ModelMap model) {
		try {
			AnalyticResult analyticResult = new ObjectMapper().readValue((String) model.get("result"),
					AnalyticResult.class);
			ClassificationResult classificationResult = new ObjectMapper().readValue(
					new String(analyticResult.getResultArray().get(0), StandardCharsets.UTF_8),
					ClassificationResult.class);

			List<ClassForEmailID> clusterList = classificationResult.getList();
			Collections.sort(clusterList, Collections.reverseOrder((o1, o2) -> {
				Integer i1 = Integer.parseInt(o1.getLeafId());
				Integer i2 = Integer.parseInt(o2.getLeafId());
				return i2.compareTo(i1);
			}));

			model.put("resultListSpamClusterer", clusterList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void mailDetect(ModelMap model) {
		ArrayList<Map<String, String>> list = new ArrayList<Map<String, String>>();

		try {
			ClassificationResult classificationResult = new ObjectMapper().readValue(model.get("result").toString(),
					ClassificationResult.class);
			for (ClassForEmailID email : classificationResult.getList()) {
				Map<String, String> map = new HashMap<String, String>();
				double spamVal = Double.parseDouble(email.getClassifiedAs());
				String spam = spamVal > 0.5 ? "SPAM" : "NOT SPAM";
				String spamColor = spamVal > 0.5 ? "#ee1122" : "#11ee22";

				DecimalFormat f = new DecimalFormat("##.00");

				map.put("spam", spam);
				map.put("spamVal", f.format(spamVal * 100) + "%");
				map.put("filename", email.getFilename());
				map.put("spamColor", spamColor);
				map.put("mailId", email.getMailId());
				list.add(map);
			}
			model.put("resultListSpamDetect", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void DGA(ModelMap model) {
		ArrayList<Map<String, String>> list = new ArrayList<>();
		try {
			AnalyticResult analyticResult = new ObjectMapper().readValue((String) model.get("result"),
					AnalyticResult.class);
			String cef = new String(analyticResult.getResultArray().get(0));
			for (String line : cef.split("[\\r\\n]+")) {
				Message message = cefParser.parse(line);
				Map<String, String> map = new HashMap<String, String>();
				map.put("msg", message.extensions().get("msg"));
				map.put("end", convertTimestamp(message.extensions().get("end")));
				map.put("dtz", message.extensions().get("dtz"));
				list.add(map);
			}
			model.put("resultListDGA", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void bruteForce(ModelMap model) {
		ArrayList<Map<String, String>> list = new ArrayList<>();
		try {
			AnalyticResult analyticResult = new ObjectMapper().readValue((String) model.get("result"),
					AnalyticResult.class);
			String cef = new String(analyticResult.getResultArray().get(0));
			for (String line : cef.split("[\\r\\n]+")) {
				Message message = cefParser.parse(line);
				Map<String, String> map = new HashMap<String, String>();

				String src = message.extensions().get("src");
				if (!isIPv4Address(src)) {
					continue;
				}

				map.put("src", src);
				map.put("app", message.extensions().get("app"));
				list.add(map);
			}
			model.put("resultListBruteForce", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String convertTimestamp(String strTimestamp) {
		String humanTimestamp = new Date(Long.parseLong(strTimestamp)).toString();
		log.info("timestamp : " + strTimestamp + " > " + humanTimestamp);

		return humanTimestamp;
	}

	public static Boolean isIPv4Address(String address) {
		try {
			Object res = InetAddress.getByName(address);
			return res instanceof Inet4Address || res instanceof Inet6Address;
		} catch (final UnknownHostException ex) {
			return false;
		}
	}

	private void connToMaliciousHosts(ModelMap model) {

		try {
			System.out.println("model.get(\"result\").toString()=" + model.get("result").toString());
//			DpoResponses dpoResponses = (DpoResponses) model.get("result");
//			String fheResult = dpoResponses.getResults().get(0).getAdditionalProperties().get("fheResults");
			FHEResponse fheResponse = new ObjectMapper().readValue(model.get("result").toString(), FHEResponse.class);
			model.put("resultConnToMaliciousHosts", fheResponse.getFheResults());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void correlateVulnerabilityAttack(ModelMap model) {
		System.out.println("in correlateVulnerabilityAttack function");
		System.out.println("model.get(\"result\")=" + model.get("result"));
		model.put("resultCorrelateVulnerabilityAttack", model.get("result"));
	}

}
