package it.cnr.iit.springswagger.restapi.types;

public class Organization {
	private String organization;

	public Organization() {
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

}
