package it.cnr.iit.springswagger.restapi.types;

import java.util.List;

public class SearchObjectList {

	private List<SearchObject> searchObjectList;

	public SearchObjectList() {
	}

	public List<SearchObject> getSearchObjectList() {
		return searchObjectList;
	}

	public void setSearchObjectList(List<SearchObject> searchObjectList) {
		this.searchObjectList = searchObjectList;
	}
}
