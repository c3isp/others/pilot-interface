package it.cnr.iit.springswagger.restapi.impl;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.cnr.iit.common.eventhandler.events.Event;

@ApiModel(value = "EventHandlerController", description = "pilotInterface EventHandlerController")
@RestController
public class EventHandlerController {

	private static final Logger LOGGER = Logger.getLogger(EventHandlerController.class.getName());

	@ApiResponses(value = { @ApiResponse(code = 412, message = "Client Error"),
			@ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 200, message = "Request accepted") })
	@ApiOperation(httpMethod = "POST", value = "Receives event notifications from EventHandler")
	@PostMapping(value = { "/v1/eventArrived" })
	public ResponseEntity<Object> getEventArrived(@RequestBody(required = true) Event event) {
		LOGGER.log(Level.INFO, "getEventNotification called");
		try {
			System.out.println(event.toString());
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Precondition exception : " + e.getMessage());
			return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
		}

		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
