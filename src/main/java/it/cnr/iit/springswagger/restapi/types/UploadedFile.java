package it.cnr.iit.springswagger.restapi.types;

import org.springframework.web.multipart.MultipartFile;

import eu.c3isp.isi.api.restapi.types.metadata.Metadata;

public class UploadedFile {

	private MultipartFile file;
	private Metadata metadata;

	public UploadedFile() {
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public Metadata getMetadata() {
		return metadata;
	}

	public void setMetadata(Metadata metadata) {
		this.metadata = metadata;
	}

}
