package it.cnr.iit.springswagger.restapi.types;

public class Dpo {
	private String dpoId;
	private String dpoTimestamp;

	public Dpo() {
	}

	public String getDpoId() {
		return dpoId;
	}

	public void setDpoId(String dpoId) {
		this.dpoId = dpoId;
	}

	public String getDpoTimestamp() {
		return dpoTimestamp;
	}

	public void setDpoTimestamp(String dpoTimestamp) {
		this.dpoTimestamp = dpoTimestamp;
	}

}
