package it.cnr.iit.springswagger.restapi.types;

import it.cnr.iit.database.history.HistoryTable;

public class DashboardInfo {
	private String AnalyticName;
	private String ticket;
	private String dpoid;
	private HistoryTable historyData;

	public String getAnalyticName() {
		return AnalyticName;
	}

	public void setAnalyticName(String analyticName) {
		AnalyticName = analyticName;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getDpoid() {
		return dpoid;
	}

	public void setDpoid(String dpoid) {
		this.dpoid = dpoid;
	}

	public HistoryTable getHistoryData() {
		return historyData;
	}

	public void setHistoryData(HistoryTable historyData) {
		this.historyData = historyData;
	}
}
