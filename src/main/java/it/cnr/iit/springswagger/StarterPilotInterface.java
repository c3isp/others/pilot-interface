/**
 * Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package it.cnr.iit.springswagger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableOAuth2Client
@ComponentScan("it.cnr.iit")
public class StarterPilotInterface extends SpringBootServletInitializer {

	@Value("${security.activation.status}")
	private boolean securityActivationStatus;

	@Autowired
	BuildProperties buildProperties;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(StarterPilotInterface.class);
	}

	@Bean
	public Docket documentation() {
		Docket docket = new Docket(DocumentationType.SWAGGER_2);
		docket.apiInfo(metadata());
		if (!securityActivationStatus) {
			return docket.select().paths(PathSelectors.regex("/v1/.*")).build();
		} else {
			return docket
					// .securitySchemes(new ArrayList<ApiKey>(Arrays.asList(new
					// ApiKey("mykey", "api_key", "header"))))
					.securitySchemes(new ArrayList<>(Arrays.asList(new BasicAuth("basicAuth"))))
					.securityContexts(new ArrayList<>(Arrays.asList(securityContext()))).select()
					.paths(PathSelectors.regex("/v1/.*")).build();
		}
	}

	private SecurityContext securityContext() {
		return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.regex("/v1/.*"))
				.build();
	}

	List<SecurityReference> defaultAuth() {
		AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
		AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
		authorizationScopes[0] = authorizationScope;
		// return new ArrayList<SecurityReference>(Arrays.asList(new
		// SecurityReference("mykey", authorizationScopes)));
		return new ArrayList<>(Arrays.asList(new SecurityReference("basicAuth", authorizationScopes)));
	}

	@Bean
	public UiConfiguration uiConfig() {
		return new UiConfiguration("validatorUrl");
	}

	private ApiInfo metadata() {
		return new ApiInfoBuilder().title("Pilot Interface").description("C3ISP pilot interface")
				.version(buildProperties.getVersion()).contact(new Contact("Calogero Lo Bue", "", "email@email.com"))
				.build();
	}

	public static void main(String[] args) {
		SpringApplication.run(StarterPilotInterface.class, args);
	}

}
