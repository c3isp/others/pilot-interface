package it.cnr.iit.springswagger.streaming.types;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Aggregator {

	@JsonProperty("rootPath")
	private String rootPath;
	@JsonProperty("sleepTime")
	private String sleepTime;
	@JsonProperty("organization")
	private String organization;
	@JsonProperty("sleepTime")
	private String dsaId;

	public Aggregator() {
	}

	public String getRootPath() {
		return rootPath;
	}

	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	}

	public String getSleepTime() {
		return sleepTime;
	}

	public void setSleepTime(String sleepTime) {
		this.sleepTime = sleepTime;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getDsaId() {
		return dsaId;
	}

	public void setDsaId(String dsaId) {
		this.dsaId = dsaId;
	}

}
