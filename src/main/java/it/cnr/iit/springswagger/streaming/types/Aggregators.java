package it.cnr.iit.springswagger.streaming.types;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Aggregators {

	@JsonProperty("aggregators")
	private List<Aggregator> aggregators;

	public Aggregators() {
	}

	public List<Aggregator> getAggregators() {
		return aggregators;
	}

	public void setAggregators(List<Aggregator> aggregators) {
		this.aggregators = aggregators;
	}

}
