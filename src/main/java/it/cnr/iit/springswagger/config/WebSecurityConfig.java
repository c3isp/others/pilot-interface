/**
 * Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package it.cnr.iit.springswagger.config;

import it.cnr.iit.springswagger.restapi.impl.WebApiController;
import java.time.Duration;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.DefaultTlsDirContextAuthenticationStrategy;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.client.RestTemplate;

/**
 * @author MIMANE
 *
 */
@Configuration
@EnableWebSecurity
@EnableOAuth2Client
@Order(99)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	private Logger LOG = Logger.getLogger(WebSecurityConfig.class.getName());

	@Value("${security.activation.status}")
	private boolean securityActivationStatus;

	@Value("${ldap.userSearchBase}")
	private String ldapUserSearchBase;

	@Value("${ldap.userSearchFilter}")
	private String ldapUserSearchFilter;

	@Value("${ldap.url}")
	private String ldapUrl;

	@Value("${ldap.use.starttls}")
	private boolean ldapUseStartTls;
        
        @Value("${ldap.user}")
        private String ldapUser;
        
        @Value("${ldap.password}")
        private String ldapPassword;

	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;

	@Value("${oidc.client.clientId}")
	private String clientID;
	@Value("${oidc.client.clientSecret}")
	private String clientSecret;
	@Value("${oidc.client.accessTokenUri}")
	private String accessTokenUri;
	@Value("${oidc.client.userAuthorizationUri}")
	private String userAuthorizationUri;
	@Value("${oidc.client.scope}")
	private String scope;

	@Autowired
	OAuth2ClientContext oauth2ClientContext;

	@Autowired
	LdapContextSource contextSource;
        
        @Autowired
        private DataSource dataSource;

	/**
	 * These paths are required for Swagger and so must not be protected
	 */
	String[] apiPath = { "/v2/api-docs", "/configuration/ui", "/swagger-resources", "/configuration/security",
			"/swagger-ui.html", "/webjars/**", "/uis/**", "/vendor/**", "/", "/login**", "/webjars/**",
			"/error**" };

	/**
	 * Here we configure basic authentication for REST endpoints starting with
	 * '/v1/' path
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		if (!securityActivationStatus) {
			http.authorizeRequests().anyRequest().permitAll();
		} else {
			http.httpBasic();
			http.authorizeRequests().antMatchers(apiPath).permitAll().anyRequest().authenticated().and().formLogin()
					.loginPage("/login").permitAll().defaultSuccessUrl("/home", true).and().logout()
					.logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login").and()
					.addFilterBefore(ssoFilter(), BasicAuthenticationFilter.class);
		}
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED).maximumSessions(2)
				.expiredUrl("/login").and().invalidSessionUrl("/login").sessionFixation().migrateSession();
		http.csrf().disable();
		http.headers().frameOptions().disable();
	}

	private Filter ssoFilter() {
		OAuth2ClientAuthenticationProcessingFilter oidcFilter = new OAuth2ClientAuthenticationProcessingFilter(
				"/login/oidc");
		OAuth2RestTemplate oidcTemplate = new OAuth2RestTemplate(oidc(), oauth2ClientContext);
		oidcFilter.setRestTemplate(oidcTemplate);
		UserInfoTokenServices tokenServices = new UserInfoTokenServices(oidcResource().getUserInfoUri(),
				oidc().getClientId());
		tokenServices.setRestTemplate(oidcTemplate);
		oidcFilter.setTokenServices(tokenServices);
		return oidcFilter;
	}

	/**
	 * Handling the Redirects
	 */
	@Bean
	public FilterRegistrationBean<OAuth2ClientContextFilter> oauth2ClientFilterRegistration(
			OAuth2ClientContextFilter filter) {
		FilterRegistrationBean<OAuth2ClientContextFilter> registration = new FilterRegistrationBean<OAuth2ClientContextFilter>();
		registration.setFilter(filter);
		registration.setOrder(-100);
		return registration;
	}
        
        @Autowired
        public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
            auth.jdbcAuthentication()
                //.passwordEncoder(new BCryptPasswordEncoder())
                .passwordEncoder(NoOpPasswordEncoder.getInstance())
                .dataSource(dataSource)
                .usersByUsernameQuery("select username, password, true AS enabled from users where username=?")
                .authoritiesByUsernameQuery("select username, role from users where username=?")
            ;
        }

	/**
	 * Config LDAP authentication
	 */
	/*
        @Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.ldapAuthentication().userSearchFilter(ldapUserSearchFilter).contextSource(contextSource);
	}
        */

	@Bean
	@ConfigurationProperties("oidc.client")
	public AuthorizationCodeResourceDetails oidc() {
		return new AuthorizationCodeResourceDetails();
	}

	@Bean
	@Primary
	@ConfigurationProperties("oidc.resource")
	public ResourceServerProperties oidcResource() {
		return new ResourceServerProperties();
	}

	@Bean
	public LdapContextSource ldapContextSource() {
                LOG.info("Build ldapContext with the following parameters:");
                LOG.info("- LDAP url: " + ldapUrl + " - user: " + ldapUser);
                LOG.info("- LDAP searchBase: " + ldapUserSearchBase);
                LOG.info("- LDAP useStartTls: " + (ldapUseStartTls ? "true" : "false"));
                
		LdapContextSource contextSource = new LdapContextSource();
		contextSource.setUrl(ldapUrl);
		contextSource.setBase(ldapUserSearchBase);
//		contextSource.setUrl("ldap://nexusc3isp.iit.cnr.it:389/");
//		contextSource.setBase("dc=c3isp,dc=eu");
//		contextSource.setUserDn("cn=admin,dc=c3isp,dc=eu");
		contextSource.setUserDn(ldapUser);
//		contextSource.setPassword("c3isnexus20");
		contextSource.setPassword(ldapPassword);
		contextSource.setPooled(false);
		if (ldapUseStartTls) {
			DefaultTlsDirContextAuthenticationStrategy strategy = new DefaultTlsDirContextAuthenticationStrategy();
			strategy.setShutdownTlsGracefully(true);
			contextSource.setAuthenticationStrategy(strategy);
		}

		return contextSource;
	}

	@Bean
	public LdapTemplate ldapTemplate() {
		return new LdapTemplate(ldapContextSource());
	}

	@Qualifier("no_oauth")
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
		return restTemplateBuilder.basicAuthentication(restUser, restPassword).setReadTimeout(Duration.ofMinutes(60))
				.setConnectTimeout(Duration.ofMinutes(60)).build();
	}

	@Qualifier("oauth")
	@Bean
	public RestTemplate oauthRestTemplate(OAuth2ClientContext oauth2ClientContext) {
		SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
		int timeout = 60 * 60 * 1000;
		factory.setConnectTimeout(timeout);
		factory.setReadTimeout(timeout);
		OAuth2RestTemplate oauth2RestTemplate = new OAuth2RestTemplate(oidc(), oauth2ClientContext);
		oauth2RestTemplate.setRequestFactory(factory);
		return oauth2RestTemplate;
	}

	@Bean
	public HttpSessionEventPublisher httpSessionEventPublisher() {
		return new HttpSessionEventPublisher();
	}

}
