package it.cnr.iit.database.create;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "upload")
public class UploadTable {
	public static final String DPO_FIELD = "dpo";
//	public static final String USER_FIELD = "user";
	public static final String EXSTENSION_FIELD = "exstension";
	public static final String FILENAME_FIELD = "filename";
	public static final String STIXED_FIELD = "is_stixed";
	public static final String ACTION_FIELD = "action";

	@DatabaseField(columnName = DPO_FIELD, id = true, canBeNull = false)
	private String dpo;

//	@DatabaseField(columnName = USER_FIELD, canBeNull = false)
//	private String user;

	@DatabaseField(columnName = FILENAME_FIELD, canBeNull = false)
	private String filename;

	@DatabaseField(columnName = EXSTENSION_FIELD)
	private String exstension;

	@DatabaseField(columnName = STIXED_FIELD, defaultValue = "false")
	private String isStixed;

	@DatabaseField(columnName = ACTION_FIELD, defaultValue = "none")
	private String action;

	public UploadTable() {
	}

//	public String getUser() {
//		return user;
//	}
//
//	public void setUser(String user) {
//		this.user = user;
//	}

	public String getDpo() {
		return dpo;
	}

	public void setDpo(String dpo) {
		this.dpo = dpo;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getExstension() {
		return exstension;
	}

	public void setExstension(String exstension) {
		this.exstension = exstension;
	}

	public String getIsStixed() {
		return isStixed;
	}

	public void setIsStixed(String isStixed) {
		this.isStixed = isStixed;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

}
