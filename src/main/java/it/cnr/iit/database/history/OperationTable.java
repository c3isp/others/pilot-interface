package it.cnr.iit.database.history;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "operation_name")
public class OperationTable {
	public static final String ID_FIELD = "operation_id";
	public static final String NAME_FIELD = "name";

	@DatabaseField(columnName = ID_FIELD, generatedId = true, allowGeneratedIdInsert = true)
	private int id;

	@DatabaseField(canBeNull = false, columnName = NAME_FIELD)
	private String name;

	public OperationTable() {
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
