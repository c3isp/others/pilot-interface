package it.cnr.iit.database.history;

import java.sql.Timestamp;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "operation_history")
public class HistoryTable {
	public static final String ID_FIELD = "id";

	public static final String USER_ID_FIELD = "user_id"; // 0
	public static final String OPERATION_ID_FIELD = "operation_id";

	public static final String TYPE_FIELD = "type"; // ticket,
	// dpo
	public static final String VALUE_FIELD = "value";
	public static final String STATUS_FIELD = "status"; // valid,
	public static final String SEARCHED_DPOS_FIELD = "searched_dpos";
	public static final String PERMITTED_DPOS_FIELD = "permitted_dpos";

	// expired,
	// forbidden

	@DatabaseField(columnName = ID_FIELD, generatedId = true, allowGeneratedIdInsert = true)
	private int id;

	@DatabaseField(canBeNull = false, columnName = USER_ID_FIELD)
	private String userId;

	@DatabaseField(columnName = OPERATION_ID_FIELD)
	private String operationId;

	@DatabaseField(columnName = TYPE_FIELD)
	private String type;

	@DatabaseField(columnName = VALUE_FIELD)
	private String value;

	@DatabaseField(columnName = STATUS_FIELD)
	private String status;

	@DatabaseField(columnName = SEARCHED_DPOS_FIELD, dataType = DataType.SERIALIZABLE)
	private String[] searchedDpos;

	@DatabaseField(columnName = PERMITTED_DPOS_FIELD, dataType = DataType.SERIALIZABLE)
	private String[] permittedDpos;

	@DatabaseField
	private Timestamp timestamp;

	/**
	 * Empty constructor
	 */
	public HistoryTable() {
	}

	public int getId() {
		return id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOperationId() {
		return operationId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public boolean isTicket() {
		return getType().equals("ticket");
	}

	public boolean isDpoId() {
		return getType().toLowerCase().equals("dpoid");
	}

	public String[] getSearchedDpos() {
		return searchedDpos;
	}

	public void setSearchedDpos(String[] searchedDpos) {
		this.searchedDpos = searchedDpos;
	}

	public String[] getPermittedDpos() {
		return permittedDpos;
	}

	public void setPermittedDpos(String[] permittedDpos) {
		this.permittedDpos = permittedDpos;
	}

}
