package it.cnr.iit.database.wp3userdb;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "users")
public class PortalUsersTable {

	public static final String USERNAME_FIELD = "username";
	public static final String NAME_FIELD = "name"; // 0
	public static final String SURNAME_FIELD = "surname";
	public static final String COUNTRY_FIELD = "country";
	public static final String ORGNAME_FIELD = "orgname"; // ticket,
	public static final String MEMBER_FIELD = "member"; // valid,
	public static final String ROLE_FIELD = "role";
	public static final String EMAIL_FIELD = "email"; // valid,
	public static final String PASSWORD_FIELD = "password"; // valid,
	public static final String USERSCOL_FIELD = "userscol"; // valid,
	public static final String SALT_FIELD = "salt"; // valid,

	@DatabaseField(columnName = USERNAME_FIELD, canBeNull = false, id = true)
	private String username;
	@DatabaseField(columnName = NAME_FIELD, canBeNull = false)
	private String name;
	@DatabaseField(columnName = SURNAME_FIELD, canBeNull = false)
	private String surname;
	@DatabaseField(columnName = COUNTRY_FIELD, canBeNull = false)
	private String country;
	@DatabaseField(columnName = ORGNAME_FIELD, canBeNull = false)
	private String orgname;
	@DatabaseField(columnName = MEMBER_FIELD, canBeNull = false)
	private String member;
	@DatabaseField(columnName = ROLE_FIELD, canBeNull = false)
	private String role;
	@DatabaseField(columnName = EMAIL_FIELD, canBeNull = false)
	private String email;
	@DatabaseField(columnName = PASSWORD_FIELD, canBeNull = false)
	private String password;
	@DatabaseField(columnName = USERSCOL_FIELD, canBeNull = false)
	private String userscol;
	@DatabaseField(columnName = SALT_FIELD, canBeNull = false)
	private String salt;

	public PortalUsersTable() {
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getOrgname() {
		return orgname;
	}

	public void setOrgname(String orgname) {
		this.orgname = orgname;
	}

	public String getMember() {
		return member;
	}

	public void setMember(String member) {
		this.member = member;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserscol() {
		return userscol;
	}

	public void setUserscol(String userscol) {
		this.userscol = userscol;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public static String getUsernameField() {
		return USERNAME_FIELD;
	}

	public static String getNameField() {
		return NAME_FIELD;
	}

	public static String getSurnameField() {
		return SURNAME_FIELD;
	}

	public static String getOrgnameField() {
		return ORGNAME_FIELD;
	}

	public static String getCountryField() {
		return COUNTRY_FIELD;
	}

	public static String getMemberField() {
		return MEMBER_FIELD;
	}

	public static String getRoleField() {
		return ROLE_FIELD;
	}

	public static String getEmailField() {
		return EMAIL_FIELD;
	}

	public static String getPasswordField() {
		return PASSWORD_FIELD;
	}

	public static String getUserscolField() {
		return USERSCOL_FIELD;
	}

	public static String getSaltField() {
		return SALT_FIELD;
	}

	public static String getGroupField() {
		return MEMBER_FIELD;
	}

}
