package it.cnr.iit.database.notification;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "notificationAck")
public class NotificationAckTable {

	public static final String ID_FIELD = "id";
	public static final String UID_FIELD = "uid";

	@DatabaseField(generatedId = true, columnName = ID_FIELD)
	private Integer id;
	@DatabaseField(canBeNull = false, columnName = UID_FIELD)
	private String uid;

	public NotificationAckTable() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

}
