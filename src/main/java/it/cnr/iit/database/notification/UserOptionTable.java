package it.cnr.iit.database.notification;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "userOption")
public class UserOptionTable {

	public static final String ID_FIELD = "id";
	public static final String UID_FIELD = "uid";
	public static final String OPTNAME_FIELD = "optname";
	public static final String OPTVALUE_FIELD = "optvalue";

	@DatabaseField(generatedId = true, columnName = ID_FIELD)
	private Integer id;
	@DatabaseField(canBeNull = false, columnName = UID_FIELD)
	private String uid;
	@DatabaseField(canBeNull = false, columnName = OPTNAME_FIELD)
	private String optname;
	@DatabaseField(generatedId = true, columnName = OPTVALUE_FIELD)
	private Integer optvalue;

	public UserOptionTable() {
	}

	public Integer getUoid() {
		return id;
	}

	public void setUoid(Integer id) {
		this.id = id;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getOptname() {
		return optname;
	}

	public void setOptname(String optname) {
		this.optname = optname;
	}

	public Integer getOptvalue() {
		return optvalue;
	}

	public void setOptvalue(Integer optvalue) {
		this.optvalue = optvalue;
	}

}
