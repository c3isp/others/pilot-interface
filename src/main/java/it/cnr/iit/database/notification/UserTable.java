package it.cnr.iit.database.notification;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "user")
public class UserTable {

	public static final String ID_FIELD = "id";
	public static final String NAME_FIELD = "name";
	public static final String SURNAME_FIELD = "surname";
	public static final String EMAIL_FIELD = "email";
	public static final String ORGANIZATION_FIELD = "organization";

	@DatabaseField(generatedId = true, columnName = ID_FIELD)
	private Integer id;
	@DatabaseField(canBeNull = false, columnName = NAME_FIELD)
	private String name;
	@DatabaseField(canBeNull = false, columnName = SURNAME_FIELD)
	private String surname;
	@DatabaseField(canBeNull = false, columnName = EMAIL_FIELD)
	private String email;
	@DatabaseField(canBeNull = false, columnName = ORGANIZATION_FIELD)
	private String organization;

	public UserTable() {
	}

	public Integer getUid() {
		return id;
	}

	public void setUid(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String string) {
		email = string;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

}
