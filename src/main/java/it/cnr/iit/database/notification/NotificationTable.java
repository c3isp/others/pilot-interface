package it.cnr.iit.database.notification;

import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "notification")
public class NotificationTable {

	public static final String ID_FIELD = "id";
	public static final String TYPE_FIELD = "type";
	public static final String MESSAGE_FIELD = "message";
	public static final String DATE_FIELD = "date";

	@DatabaseField(generatedId = true, columnName = ID_FIELD)
	private Integer id;
	@DatabaseField(canBeNull = false, columnName = TYPE_FIELD)
	private String type;
	@DatabaseField(canBeNull = false, columnName = MESSAGE_FIELD)
	private String message;
	@DatabaseField(canBeNull = false, columnName = DATE_FIELD)
	private Date date;

	public NotificationTable() {
	}

	public Integer getNid() {
		return id;
	}

	public void setNid(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
