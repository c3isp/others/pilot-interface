package it.cnr.iit.eventhandler;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import it.cnr.iit.common.eventhandler.AbstractEventHandlerSubscriber;
import it.cnr.iit.pilot.properties.PilotProperties;

@Configuration
public class EventHandlerSubscriber extends AbstractEventHandlerSubscriber {

	private static final Logger LOGGER = Logger.getLogger(EventHandlerSubscriber.class.getName());

	@Autowired
	private PilotProperties properties;

	public EventHandlerSubscriber() {
		super(new String[] { "abc", "test" });
		setScheduleRate(30, 300);
		start();
	}

	@Override
	public String getEventHandlerSubscribeAPI() {
		return properties.getEventHandlerSubscribeAPI();
	}

	@Override
	public String getEventHandlerSubscribersAPI() {
		return properties.getEventHandlerSubscribersAPI();
	}

	@Override
	public String getSelfEventArrivedAPI() {
		return properties.getSelfEventArrivedAPI();
	}

}
