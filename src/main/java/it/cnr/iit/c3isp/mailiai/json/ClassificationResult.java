package it.cnr.iit.c3isp.mailiai.json;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ClassificationResult {
	private List<ClassForEmailID> list = new ArrayList<>();

	public List<ClassForEmailID> getList() {
		return list;
	}

	public void setList(List<ClassForEmailID> list) {
		this.list = list;
	}

	public boolean addClassifiedEmail(ClassForEmailID classifiedMail) {
		return list.add(classifiedMail);
	}

	@Override
	public String toString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

}
