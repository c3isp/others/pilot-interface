package it.cnr.iit.utility;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import java.net.URI;
import java.security.Principal;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.gson.Gson;

import eu.c3isp.isi.api.restapi.types.SearchParams;
import eu.c3isp.isi.api.restapi.types.xacml.RequestAttributes;
import eu.c3isp.isi.api.restapi.types.xacml.RequestBuilder;
import eu.c3isp.isi.api.restapi.types.xacml.RequestContainer;
import eu.c3isp.isi.api.restapi.types.xacml.RequestElement;
import it.cnr.iit.common.attributes.AttributeIds;
import it.cnr.iit.common.types.MailJson;
import it.cnr.iit.database.common.CommonDatabase;
import it.cnr.iit.database.create.UploadTable;
import java.util.logging.Level;

@Component
public class ISIUtility {

	private final static Logger log = Logger.getLogger(ISIUtility.class.getName());

	@Value("${isiUtility.isi.uri}")
	private String isiUrl;

	@Autowired
	private UtilsForControllers utilsForControllers;

	@Autowired
	private CommonDatabase commonDatabase;

	public List<String> searchDpo(SearchParams searchParams, RestTemplate restTemplate) {
		try {
			String path = "/v1/search/dpos/false";

			ObjectMapper mapper = new ObjectMapper();

			MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();

			HttpHeaders headers = new HttpHeaders();

			headers.setContentType(MediaType.APPLICATION_JSON);

			RequestContainer container = new RequestContainer();
			RequestElement el = new RequestElement();
			RequestAttributes attr = new RequestAttributes();
			attr.setAttributeId("ns:c3isp:search-string");

			String search = new Gson().toJson(searchParams);

			attr.setValue(search);
			LinkedList<RequestAttributes> list = new LinkedList<>();
			list.add(attr);
			el.setAttributes(list);
			container.setRequest(el);

			System.err.println(mapper.writeValueAsString(container));

			System.err.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(container));

			/**
			 * PLESE NOTE: Test as this is a post, I put the value directly in
			 */

			HttpEntity<RequestContainer> request = new HttpEntity<>(container, headers);
			System.err.println(headers);

			URI searchUri = UriComponentsBuilder.fromUriString(isiUrl).path(path).build().toUri();
                        log.info("Launch request to: " + searchUri.toString());

			ResponseEntity<String[]> response = restTemplate.exchange(searchUri, HttpMethod.POST, request,
					String[].class);
			for (String string : response.getBody()) {
				System.out.println(string);
			}
			String[] string = response.getBody();
			return Arrays.asList(string);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<String> searchDpo(SearchParams searchParams, RestTemplate restTemplate, String url) {
		try {
			String path = url;

			ObjectMapper mapper = new ObjectMapper();

			MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();

			HttpHeaders headers = new HttpHeaders();

			headers.setContentType(MediaType.APPLICATION_JSON);

			RequestContainer container = new RequestContainer();
			RequestElement el = new RequestElement();
			RequestAttributes attr = new RequestAttributes();
			attr.setAttributeId("ns:c3isp:search-string");

			String search = new Gson().toJson(searchParams);

			attr.setValue(search);
			LinkedList<RequestAttributes> list = new LinkedList<>();
			list.add(attr);
			el.setAttributes(list);
			container.setRequest(el);

			System.err.println(mapper.writeValueAsString(container));

			System.err.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(container));

			/**
			 * PLESE NOTE: Test as this is a post, I put the value directly in
			 */

			HttpEntity<RequestContainer> request = new HttpEntity<>(container, headers);
			System.err.println(headers);

			URI searchUri = UriComponentsBuilder.fromUriString(path).build().toUri();

			ResponseEntity<String[]> response = restTemplate.exchange(searchUri, HttpMethod.POST, request,
					String[].class);
			for (String string : response.getBody()) {
				System.out.println(string);
			}
			String[] string = response.getBody();
			return Arrays.asList(string);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
        
        public ResponseEntity<String> exportDpo(String dpoId, String exportTarget, String exportParams, Principal user, RestTemplate restTemplate) {
		try {

			String organization = utilsForControllers.getOrganisationFromLDAP(user, "ou=SME,ou=Pilots")
					+ utilsForControllers.getOrganisationFromLDAP(user, "ou=Enterprise,ou=Pilots");
			if (organization.isEmpty()) {
				organization = utilsForControllers.getOrganisationFromMySQL(user);
			}

			String path = "/v1/dpo/";
			URI uri = UriComponentsBuilder.fromUriString(isiUrl + path)
                                    .path(dpoId + "/export/" + exportTarget)
                                    .query("exportParams=" + exportParams)
                                    .build().toUri();
//			RequestContainer container = getRequestContainerReadDpo();

			Map<String, String> attributeMap = new HashMap<>();
			attributeMap.put(AttributeIds.SUBJECT_ID, user.getName());
			attributeMap.put(AttributeIds.SUBJECT_ORGANIZATION, organization);
			attributeMap.put(AttributeIds.ACTION_ID, "export");
			RequestContainer container = new RequestBuilder().build(attributeMap);

			HttpEntity<RequestContainer> request = getHttpEntity(container);
                        
                        log.log(Level.INFO, "Export request url: {0}", uri.toString());
			log.log(Level.INFO, "request in exportDpo after adding attributes: {0}", new ObjectMapper().writeValueAsString(request));

                        ResponseEntity<String> response = restTemplate.postForEntity(uri, request, String.class);
			//String result = restTemplate.exchange(uri, request, String.class);
                        return response;
		} catch (JsonProcessingException e) {
                        log.severe("Exception caught while exporting Dpo: " + e.toString());
			return null;
		}
        }
        
        public byte[] readDpo(String dpoId, Principal user, RestTemplate restTemplate) {
            return readDpo(dpoId, user, restTemplate, "read");
        }
        
	public byte[] readDpo(String dpoId, Principal user, RestTemplate restTemplate, String actionId) {
		try {

			String organization = utilsForControllers.getOrganisationFromLDAP(user, "ou=SME,ou=Pilots")
					+ utilsForControllers.getOrganisationFromLDAP(user, "ou=Enterprise,ou=Pilots");
			if (organization.isEmpty()) {
				organization = utilsForControllers.getOrganisationFromMySQL(user);
			}

			String path = "/v1/dpo/";
			URI uri = UriComponentsBuilder.fromUriString(isiUrl + path).path(dpoId + "/").build().toUri();
//			RequestContainer container = getRequestContainerReadDpo();

			Map<String, String> attributeMap = new HashMap<>();
			attributeMap.put(AttributeIds.SUBJECT_ID, user.getName());
                        // Removed organization attribute because some PIP
                        // will insert during fattening request
			// attributeMap.put(AttributeIds.SUBJECT_ORGANIZATION, organization);
			attributeMap.put(AttributeIds.ACTION_ID, actionId);
			RequestContainer container = new RequestBuilder().build(attributeMap);

			HttpEntity<RequestContainer> request = getHttpEntity(container);

			log.log(Level.INFO, "request in readDpo after adding attributes: {0}", new ObjectMapper().writeValueAsString(request));

			byte[] result = restTemplate.exchange(uri, HttpMethod.GET, request, ByteArrayResource.class).getBody()
					.getByteArray();

			UploadTable uploadRow = commonDatabase.getField("dpo", dpoId, UploadTable.class);

			if (uploadRow.getAction().contains("mail")) {
				result = decodeEmails(result);
			} else if (uploadRow.getAction().equals("upload") && uploadRow.getIsStixed().equals("false")) {
//				 this should work for ssh, not for flows
				result = Base64.getDecoder().decode(result);
			}

			return result;
		} catch (JsonProcessingException e) {
                        log.severe(e.toString());
			e.printStackTrace();
			return null;
		}
	}

	private byte[] decodeEmails(byte[] codedEmails) {
		byte[] result = null;
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			TypeFactory typeFactory = objectMapper.getTypeFactory();
			List<MailJson> mailList = objectMapper.readValue(codedEmails,
					typeFactory.constructCollectionType(List.class, MailJson.class));
			for (MailJson mail : mailList) {
				mail.setContent(new String(Base64.getDecoder().decode(mail.getContent())));
			}
			result = objectMapper.writeValueAsBytes(mailList);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public HttpEntity<RequestContainer> getHttpEntity(RequestContainer container) {
		HttpHeaders headers = new HttpHeaders();
		// headers.add("Authorization", "Basic " + base64Creds);
		headers.add("X-c3isp-input_metadata", container.toString());
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<RequestContainer> request = new HttpEntity<>(container, headers);

		return request;
	}

	public String moveDpo(String dpoId, RequestContainer requestContainer, RestTemplate restTemplate, String url) {
		HttpHeaders headers = new HttpHeaders();
		// headers.add("Authorization", "Basic " + base64Creds);
		headers.add("X-c3isp-input_metadata", requestContainer.toString());
		System.out.println(requestContainer.toString());

		HttpEntity<RequestContainer> request = getHttpEntity(requestContainer);

		URI uri = UriComponentsBuilder.fromUriString(url).path(dpoId + "/").build().toUri();
		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, request, String.class);

		return response.getBody();
	}

	public RequestContainer getRequestContainerReadDpo() {
		RequestContainer container = new RequestContainer();
		RequestElement el = new RequestElement();
		RequestAttributes attr = new RequestAttributes();
//		attr.setAttributeId("ns:c3isp:read-dpo");

//		attr.setValue("read");
		LinkedList<RequestAttributes> list = new LinkedList<>();
		list.add(attr);
		el.setAttributes(list);
		container.setRequest(el);

		return container;
	}

}
