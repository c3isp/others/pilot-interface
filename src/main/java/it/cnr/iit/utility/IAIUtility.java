package it.cnr.iit.utility;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import eu.c3isp.isi.api.restapi.types.AnalyticResponse;

@Component
public class IAIUtility {

	private final static Logger log = Logger.getLogger(IAIUtility.class.getName());

	@Value("${iaiUtility.iai.uri}")
	private String iaiUrl;

	public AnalyticResponse runAnalytic(String analyticName, String json, RestTemplate restTemplate, String username)
			throws URISyntaxException {
		URI uri = UriComponentsBuilder.fromUriString(iaiUrl).path("/v1/" + analyticName + "/").build().toUri();

		log.info("run " + analyticName + " uri: " + uri);

		RequestEntity requestEntity = RequestEntity.post(uri).contentType(MediaType.APPLICATION_JSON).body(json);

		ResponseEntity<AnalyticResponse> result = restTemplate.exchange(requestEntity, AnalyticResponse.class);

		return result.getBody();
	}

	public String getResponseForTicket(String ticket, RestTemplate restTemplate) {
		URI uri = UriComponentsBuilder.fromUriString(iaiUrl + "/v1/getResponse/").path(ticket + "/").build().toUri();

		log.info("response uri: " + uri);

		String dpoResponses = restTemplate.getForObject(uri.toString(), String.class);

		return dpoResponses;
	}

	public String convertToGraph(String api, String dpoId, RestTemplate restTemplate, String username) {
		URI uri = UriComponentsBuilder.fromUriString(iaiUrl).path("/v1/" + api + "/").queryParam("username", username)
				.build().toUri();

		log.severe("run convertToGraph uri: " + uri);

		RequestEntity requestEntity = RequestEntity.post(uri).contentType(MediaType.APPLICATION_JSON).body(dpoId);

		ResponseEntity<String> result = restTemplate.exchange(requestEntity, String.class);

		return result.getBody();
	}

}
