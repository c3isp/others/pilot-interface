package it.cnr.iit.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.ldap.LdapContext;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import eu.c3isp.isi.api.restapi.types.Dsa;
import it.cnr.iit.certuploader.proxy.DSAProxy;
import it.cnr.iit.database.common.CommonDatabase;
import it.cnr.iit.database.wp3userdb.PortalUsersTable;
import it.cnr.iit.pilot.hacks.FakePrincipal;
import it.cnr.iit.springswagger.restapi.types.ResponseForPortal;

@Component
public class UtilsForControllers {

	@Value("${ldap.user}")
	private String ldapUser;
	@Value("${ldap.password}")
	private String ldapPassword;
	@Value("${api.getDsaId.searchString}")
	private String searchString;
	@Value("${api.getDsaId.searchDsaId}")
	private String searchDsaId;
	@Value("${api.getDsaId.isi-api}")
	private String isiApi;
	@Value("${security.user.name}")
	private String username;
	@Value("${security.user.password}")
	private String password;
	@Value("${ldap.url}")
	private String ldapUrl;

	@Autowired
	private LdapContextSource contextSource;
	@Autowired
	private LdapTemplate ldapTemplate;
	@Autowired
	private CommonDatabase commonDatabase;
        @Autowired
        private DSAProxy dsaProxy;

	private LdapContext ldapContext;
	private final Hashtable<String, String> environment = new Hashtable<>();
	private Logger LOGGER = Logger.getLogger(UtilsForControllers.class.getName());

	public String getOrganisationFromLDAP(Principal principal, String searchString) {
                /*
                TODO: Enable usage configurable
                
                
		contextSource.getContext(ldapUser, ldapPassword);
		List<String> organisation = ldapTemplate.search(searchString, "uid=" + principal.getName(),
				(AttributesMapper<String>) attrs -> (String) attrs.get("o").get());

		if (!organisation.isEmpty()) {
			return organisation.get(0);
		}
                */
		return "";
	}

	public String getOrganisationFromMySQL(Principal principal) {

		String organisation = commonDatabase
				.getField(PortalUsersTable.USERNAME_FIELD, principal.getName(), PortalUsersTable.class).getOrgname();

		if (!organisation.isEmpty()) {
			return organisation;
		}
		return "";
	}

	public String getRoleFromMySQL(Principal principal) {

		String role = commonDatabase.getField(PortalUsersTable.ROLE_FIELD, principal.getName(), PortalUsersTable.class)
				.getRole();

		if (!role.isEmpty()) {
			return role;
		}
		return "";
	}

	public String getGroupFromMySQL(Principal principal) {

		String group = commonDatabase
				.getField(PortalUsersTable.MEMBER_FIELD, principal.getName(), PortalUsersTable.class).getMember();

		if (!group.isEmpty()) {
			return group;
		}
		return "";
	}

	public List<Dsa> getDsaId(String organisation, Principal principal, HttpServletResponse httpResponse)
			throws IOException {

		principal = principal == null ? new FakePrincipal() : principal;
		checkIfLogged(principal, httpResponse);

		String tmp = searchString;
		tmp = tmp.replace("[%ORG%]", organisation);

		// TODO
		// THIS MUST BE CHANGED IN A REQUEST WITH OAUTH2 WHEN OIDC IS INTEGRATED WITH
		// THE ISI
		// DSAProxy dsaProxy = new DSAProxy(isiApi, username, password);

		List<String> dsaList = dsaProxy.searchDSA(tmp);
		List<Dsa> dsaConverted = convertListToDsa(dsaList);

		if (dsaConverted.isEmpty()) {
			LOGGER.log(Level.WARNING, "No DSA has been found. Is the DSA expired?");
			return new ArrayList<>();
		} else {
			return dsaConverted;
		}
	}

	public List<Dsa> convertListToDsa(List<String> dsaList) {
		List<Dsa> list = new ArrayList<>();
		for (String string : dsaList) {
			try {
				Dsa dsa = new ObjectMapper().readValue(string, Dsa.class);
				if (dsa != null && !dsa.getStatus().equals("Expired") && dsa.getTitle() != null) {
					list.add(dsa);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return list;
	}

	public void checkIfLogged(Principal principal, HttpServletResponse httpResponse) {
		try {
			if (principal.getName().isEmpty()) {
				httpResponse.sendRedirect("/login");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public <T> HttpServletResponse prepareReturn(HttpServletResponse httpResponse, String status, T content)
			throws IOException {
		ResponseForPortal responseForPortal = new ResponseForPortal();
		responseForPortal.setStatus(status);

		if (content instanceof String) {
			if (content.toString().contains("Forbidden")) {
				responseForPortal.setContent("You're not allowed to upload any data. Please select another DSA.");
			} else {
				responseForPortal.setContent(content);
			}
		} else {
			responseForPortal.setContent(content);
		}

		httpResponse.getWriter().write(new Gson().toJson(responseForPortal));
		httpResponse.flushBuffer();
		return httpResponse;
	}

	public String getLogType(MultipartFile file) {

		BufferedReader br;
		Pattern pattern = Pattern.compile("(?<!\\d|\\d\\.)" + "(?:[01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
				+ "(?:[01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." + "(?:[01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
				+ "(?:[01]?\\d\\d?|2[0-4]\\d|25[0-5])" + "(?!\\d|\\.\\d)");
		String type = "invalid";

		try {

			String line;
			InputStream is = file.getInputStream();
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				if (line.contains("#dataType")) {
					return "valid";
				}

				Matcher matcher = pattern.matcher(line);
				if (matcher.find()) {
					if (line.contains("sshd[")) {
						type = "ssh;sshd";
						break;
					}
					if (line.contains(": query: ") && line.split(": query: ")[1].contains("IN")) {
						type = "dns;bind_dns";
						break;
					}
					if (line.contains("unbound: [") && line.contains("] info: ")) {
						type = "dns;unbound";
						break;
					}
					if ((line.contains("TCP") || line.contains("UDP")) && line.contains("->")) {
						type = "netflow;netflow-router";
						break;
					}
				}
			}

			br.close();

		} catch (IOException e) {
			System.err.println(e.getMessage());
		}

		return type;
	}
}
