package it.cnr.iit.cert;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64OutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.c3isp.iai.subapi.types.DpoResponses;
import eu.c3isp.iai.subapi.types.Result;
import eu.c3isp.isi.api.restapi.types.xacml.RequestBuilder;
import eu.c3isp.isi.api.restapi.types.xacml.RequestContainer;
import it.cnr.iit.common.types.Metadata;
import it.cnr.iit.database.common.CommonDatabase;
import it.cnr.iit.database.create.UploadTable;
import it.cnr.iit.exceptions.UploadExceptions;
import it.cnr.iit.utility.UtilsForControllers;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;

@Component
public class Uploader {

	@Value("${uploader.metadataForm}")
	private String metadataForm;
	@Value("${uploader.fileToSubmit}")
	private String fileToSubmit;
	@Value("${uploader.uri}")
	private String isiUri;

	@Autowired
	private UtilsForControllers utilsForControllers;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private CommonDatabase commonDatabase;

	private static final Logger log = Logger.getLogger(Uploader.class.getName());

	public Uploader() {
	}

	/**
	 * Uploads all the files uploaded via the gui and returns a string containing
	 * all the responses
	 *
	 * @param files    the array of files to be uploaded
	 * @param metadata the metadata to be added to each file
	 * @return String containing all the responses
	 * @throws Exception
	 */
	public List<String> uploadMultipleFiles(MultipartFile[] files, Metadata metadata, Map<String, String> attributeMap)
			throws Exception {
		if (files == null || files.length == 0 || metadata == null || restTemplate == null) {
			throw new UploadExceptions("Invalid arguments passed: " + (files == null) + "\t" + (metadata == null) + "\t"
					+ (restTemplate == null));
		} else {
			if (metadata.getEventType().toLowerCase().contains("mail")
					|| metadata.getEventType().toLowerCase().contains("spam")) {
				return uploadMailData(files, metadata, attributeMap);
			} else if (metadata.getStixed().equals("true")) {
				return uploadNormalizedData(files, metadata, attributeMap);
			}
			return uploadNotNormalizedData(files, metadata, attributeMap);
		}
	}

	private List<String> uploadNormalizedData(MultipartFile[] files, Metadata metadata,
			Map<String, String> attributeMap) throws IOException, UploadExceptions {
		String url = isiUri + "?norm=true";
		log.info(url);
		StringBuilder responses = new StringBuilder();
		RequestContainer inputMetadata;
		List<String> resultList = new ArrayList<String>();
		String result = null;

		for (MultipartFile file : files) {
			String info = utilsForControllers.getLogType(file);
			System.out.println("log type is " + info);
			if (info.equals("invalid")) {
				throw new UploadExceptions("Not supported file type: " + file.getOriginalFilename());
			}

			inputMetadata = buildMinimumInputMetadata(file.getOriginalFilename(), metadata, attributeMap);

			if (info.equals("valid")) {
				result = uploadSingleFile(file.getOriginalFilename(), file.getBytes(), inputMetadata, url);
				resultList.add(result);
			} else {
				byte[] fileToUpload = addFirstLine(metadata, info, file);
				String fileToString = new String(fileToUpload);
				if (!fileToString.equals("error")) {
					result = uploadSingleFile(file.getOriginalFilename(), fileToUpload, inputMetadata, url);
					resultList.add(result);
				} else {
					throw new UploadExceptions(
							"There was an error during the insertion of the first line (metadata) into the file "
									+ file.getOriginalFilename());
				}
			}
			createRowForUploadTable(file, result, metadata.getStixed(), "upload");
		}
		log.severe("ResultList: ");
		resultList.stream().forEach(el -> log.severe(el));
		return resultList;

	}

	// normalized = stixed
	private List<String> uploadNotNormalizedData(MultipartFile[] files, Metadata metadata,
			Map<String, String> attributeMap) throws UploadExceptions, IOException {
                log.severe("Inside not normalized");
                List<String> resultList = new ArrayList<String>();
                String url = isiUri + "?norm=false";
                log.info(url);
//		StringBuilder responses = new StringBuilder();
                RequestContainer inputMetadata;
                for (MultipartFile file : files) {
                        inputMetadata = buildMinimumInputMetadata(file.getOriginalFilename(), metadata, attributeMap);
                        File convertedFile = convertToBase64(file);
//				String result = uploadSingleFile(file.getOriginalFilename(),
//						FileUtils.readFileToByteArray(convertedFile), inputMetadata, url);
                        String result = uploadSingleFile(file.getOriginalFilename(), convertedFile, inputMetadata, url);
                        resultList.add(result);
                        createRowForUploadTable(file, result, metadata.getStixed(), "upload");
                }
                log.severe("ResultList: ");
                resultList.stream().forEach(el -> log.severe(el));
                return resultList;
	}

	private void createRowForUploadTable(MultipartFile file, String result, String stixed, String action) {

		DpoResponses dpoResponse = new DpoResponses();
		List<Result> results = new ArrayList<Result>();
		dpoResponse.setResults(results);

		try {
			results.add(new ObjectMapper().readValue(result, Result.class));
		} catch (IOException e) {
			log.severe(e.getMessage());
			return;
		}

		String[] splittedFilename = file.getOriginalFilename().split("\\.");
		String extension = splittedFilename[splittedFilename.length - 1];
		String filename = file.getOriginalFilename().split("\\." + extension)[0];

		log.severe("filename to put in table: " + filename);
		log.severe("extension to put in table: " + extension);

		UploadTable uploadRow = new UploadTable();
		uploadRow.setDpo(dpoResponse.getDpoId());
		uploadRow.setFilename(filename);
		uploadRow.setExstension(extension);
		uploadRow.setIsStixed(stixed);
		uploadRow.setAction(action);

		commonDatabase.createOrUpdateEntry(uploadRow);

	}

	private List<String> uploadMailData(MultipartFile[] files, Metadata metadata, Map<String, String> attributeMap)
			throws UploadExceptions, IOException {
		String url = isiUri + "?norm=false";
		StringBuilder responses = new StringBuilder();
		JSONArray jsonArray = new JSONArray();
		List<String> resultList = new ArrayList<String>();

		for (MultipartFile file : files) {
			jsonArray = encodeData(jsonArray, file, metadata);
		}

		RequestContainer inputMetadata = buildMinimumInputMetadata(files[0].getOriginalFilename(), metadata,
				attributeMap);
		String result = uploadSingleFile(files[0].getOriginalFilename(), jsonArray.toString().getBytes(), inputMetadata,
				url);
		System.out.println("responses.toString()=" + responses.toString());
		resultList.add(result);

		for (MultipartFile file : files) {
			createRowForUploadTable(file, result, metadata.getStixed(), "upload_mail");
		}

		log.severe("ResultList: ");
		resultList.stream().forEach(el -> log.severe(el));
		return resultList;
	}

	private JSONArray encodeData(JSONArray jsonArray, MultipartFile file, Metadata metadata) throws IOException {

		JSONObject item = new JSONObject();
		String encodedContent = "";
		item.put("filename", file.getOriginalFilename());
		System.out.println("The file is valid");
		encodedContent = Base64.getEncoder().encodeToString(file.getBytes());
//		byte[] encodedContent = Base64.getEncoder().encode(file.getBytes());
		item.put("content", encodedContent);
		jsonArray.put(item);
		return jsonArray;
	}

	private String uploadSingleFile(String filename, File file, RequestContainer metadata, String url)
			throws UploadExceptions {
		try {
			LinkedMultiValueMap<String, Object> toUpload = new LinkedMultiValueMap<>();
			toUpload.add(fileToSubmit, new FileSystemResource(file));
			toUpload.add(metadataForm, metadata);
                        
			RequestEntity<MultiValueMap<String, Object>> requestEntity;
			requestEntity = RequestEntity.post(new URI(url)).contentType(MediaType.MULTIPART_FORM_DATA).body(toUpload);
                        
			//ResponseEntity<String> response = restTemplate.exchange(requestEntity, String.class);
                        
                        ResponseEntity<String> response = restTemplate.postForEntity(url, requestEntity, String.class);
                        log.severe("in uploadSingleFile, response = " + response.getBody());

			if (response.getBody().contains("unauthorised")) {
				throw new UploadExceptions("You are not authorised to create a DPO with the selected DSA");
			}

			return response.getBody();
		} catch (URISyntaxException e) {
			log.severe("URI Syntax Exception");
		} catch (HttpClientErrorException e) {
                        log.severe(String.format("Upload request error. [code = %s, status = %s]", e.getStatusCode(), e.getStatusText()));
                        if (e.getStatusCode() == HttpStatus.FORBIDDEN) {
                            throw new UploadExceptions("You are not authorised to create a DPO with the selected DSA");
			}
		}
		return null;
	}

	private String uploadSingleFile(String filename, byte[] file, RequestContainer metadata, String url)
			throws UploadExceptions {
		try {
			File tmp = createFileFromString(filename, file);
			LinkedMultiValueMap<String, Object> toUpload = new LinkedMultiValueMap<>();
			toUpload.add(fileToSubmit, new FileSystemResource(tmp));
			toUpload.add(metadataForm, metadata);
			RequestEntity<MultiValueMap<String, Object>> requestEntity;
			requestEntity = RequestEntity.post(new URI(url)).contentType(MediaType.MULTIPART_FORM_DATA).body(toUpload);
			ResponseEntity<String> response = restTemplate.exchange(requestEntity, String.class);
			log.severe("in uploadSingleFile, response = " + response.getBody());

			if (response.getBody().contains("unauthorised")) {
				throw new UploadExceptions("You are not authorised to create a DPO with the selected DSA");
			}

			return response.getBody();
		} catch (URISyntaxException e) {
			log.severe("URI Syntax Exception");
		} catch (IOException e) {
			log.severe("IOException:"+e.getMessage());
		} catch (HttpClientErrorException e) {
                        log.severe(String.format("Upload request error. [code = %s, status = %s]", e.getStatusCode(), e.getStatusText()));
                        if (e.getStatusCode() == HttpStatus.FORBIDDEN) {
                            throw new UploadExceptions("You are not authorised to create a DPO with the selected DSA");
			}
		}

		return null;
	}

	private RequestContainer buildMinimumInputMetadata(String filePath, Metadata metadata,
			Map<String, String> attributeMap) throws JsonProcessingException {
		RequestContainer inputMetadata = new RequestBuilder().build(metadata, attributeMap);
		inputMetadata.getRequest().getAttributes().stream()
				.forEach(s -> System.out.println(s.getAttributeId() + "=" + s.getValue()));
		log.info("DPO: " + new ObjectMapper().writeValueAsString(inputMetadata));

		return inputMetadata;
	}

	private File createFileFromString(String filePath, byte[] content) throws IOException {

		File file;
		file = File.createTempFile(filePath, "tmp");
		file.deleteOnExit();
		FileUtils.forceDeleteOnExit(file);
		FileUtils.writeByteArrayToFile(file, content);
		return file;

	}

	private byte[] addFirstLine(Metadata metadata, String info, MultipartFile file) {
		try {
			String firstLine = "#dataType=" + info.split(";")[0] + "; startTime=" + metadata.getStartTime()
					+ "; endTime=" + metadata.getEndTime() + "; organization=" + metadata.getOrganization()
					+ "; hostname=" + System.getProperty("user.name") + "; vendor=" + info.split(";")[1]
					+ "; severity=5\n";

			File folder = new File("/tmp/c3isp");
			if (!folder.exists()) {
				folder.mkdirs();
			}

			File toCopy = new File("/tmp/c3isp/" + file.getOriginalFilename());

			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(toCopy.getPath())));
			bw.write(firstLine);
			InputStream is = file.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			String line = "";
			while ((line = br.readLine()) != null) {
				bw.append(line + "\n");
			}

			is.close();
			bw.close();

			byte[] fileContent = Files.readAllBytes(toCopy.toPath());
			toCopy.delete();

			return fileContent;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "error".getBytes();
	}

	public File convertToBase64(MultipartFile file) {
		try {
			Path tempFile = Files.createTempFile(file.getOriginalFilename().split("\\.")[0], ".tmp");
//			FileOutputStream outStream = new FileOutputStream(tempFile.toFile());
//			try {
//				encode(file.getInputStream(), outStream);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}

			BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile.toFile()));
			writer.write(new String(Base64.getEncoder().encode(file.getBytes())));
			writer.close();

//			byte[] tmpToByteArray = FileUtils.readFileToByteArray(tempFile.toFile());
			return tempFile.toFile();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void encode(InputStream is, OutputStream base64OutputStream) throws Exception {
		OutputStream out = new Base64OutputStream(base64OutputStream);
		IOUtils.copy(is, out);
		is.close();
		out.close();
	}
}
