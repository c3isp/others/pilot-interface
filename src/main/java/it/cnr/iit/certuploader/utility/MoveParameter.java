package it.cnr.iit.certuploader.utility;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MoveParameter {
	@JsonProperty("dpoid")
	private String dpoid;
	@JsonProperty("accessPurpose")
	private String accessPurpose;

	public String getDpoid() {
		return dpoid;
	}

	public void setDpoid(String dpoid) {
		this.dpoid = dpoid;
	}

	public String getAccessPurpose() {
		return accessPurpose;
	}

	public void setAccessPurpose(String accessPurpose) {
		this.accessPurpose = accessPurpose;
	}
}
