package it.cnr.iit.certuploader.proxy;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import it.cnr.iit.certuploader.utility.JSONCreator;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;

@Component
public class DSAProxy {

	// public static RestTemplate restTemplate = new RestTemplate();
	/*
	 * public static String url = "https://dsamgrc3isp.iit.cnr.it/dsa-store-api/v1";
	 * 
	 * public static RestTemplateBuilder restTemplateBuilder = new
	 * RestTemplateBuilder(); public static RestTemplate restTemplate =
	 * restTemplateBuilder.basicAuthorization("user", "password").build();
	 */

	private static final Logger LOG = LoggerFactory.getLogger(DSAProxy.class);

	private String dsastoreAPIURL;
        
	@Value("${security.user.name}")
	private String restUser;

        @Value("${security.user.password}")
	private String restPassword;
	
        private RestTemplate restTemplate;
        
	@Value("${dsa.store-api.url}")
	private String dsaStoreApiUrl;

	private DSAProxy() {
		//RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
		//this.restTemplate = restTemplateBuilder.basicAuthentication(restUser, restPassword).build();
	}

	public DSAProxy(String dsastoreapiURL, String restUser, String restPassword) {
		this.dsastoreAPIURL = dsastoreapiURL;
		this.restUser = restUser;
		this.restPassword = restPassword;
		RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
		this.restTemplate = restTemplateBuilder.basicAuthentication(restUser, restPassword).build();
	}
        
        @PostConstruct        
        public void init() {
            String logPass = restPassword == null ? "null" : restPassword.replaceAll(".", "*");
            LOG.info(String.format("Initialized DSAProxy(dsaStoreApi=%s, user=%s, password=%s)", dsaStoreApiUrl, restUser, logPass));
            RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
            this.restTemplate = restTemplateBuilder.basicAuthentication(restUser, restPassword).build();
        }

	public ResponseEntity<byte[]> readDSA(String dsa_id) {
		ResponseEntity<byte[]> response = restTemplate.getForEntity(dsastoreAPIURL + "/readDSA/{dsaId}", byte[].class,
				dsa_id);
		return response;
	}

	public List<String> searchDSA(String searchString) {

		try {

			List<String> searchResult = new ArrayList<String>();
			// temporary the search string is hardcoded
			JSONCreator jsonCreator = new JSONCreator();
			searchString = searchString.replace("\\", "");
			String searchString_dsa = jsonCreator.forSearchDSA(searchString).toString();
			searchString_dsa = searchString;
			// ResponseEntity<List> response =
			// restTemplate.postForEntity(dsastoreAPIURL
			// + "/searchDSA/{longResultFlag}", searchString, List.class,
			// longResultFlag);
			// ResponseEntity<List> response =
			// restTemplate.postForEntity(dsastoreAPIURL
			// + "/search/{store}/{longResultFlag}", searchString_dsa, List.class,
			// "dsas", false);
			String url = dsaStoreApiUrl + "/v1/searchDSA/true";

			// MultiValueMap<String, Object> parameters = new
			// LinkedMultiValueMap<String, Object>();
			// parameters.add("searchString", searchString_dsa);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			// HttpEntity<MultiValueMap<String, Object>> requestEntity = new
			// HttpEntity<MultiValueMap<String, Object>>(parameters, headers);
			HttpEntity<String> requestEntity = new HttpEntity<String>(searchString_dsa, headers);
                        LOG.info("Fetching DSAs...");
                        // log.info("searchstring_dsa = " + searchString_dsa);
                        // log.info("headers = " + headers.toString());
                        
			ResponseEntity<List> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, List.class,
					"dsas", false);

			if (response.getStatusCode() == HttpStatus.OK) {
				searchResult = response.getBody();
				LOG.info("return string: " + searchResult);
			}
			if (searchResult == null || searchResult.size() == 0 || searchResult.get(0) == null) {
				return new ArrayList<String>();
			}
			return searchResult;
		} catch (Exception e) {
			e.printStackTrace();
			// return null;
		}
                return new ArrayList<String>();
	}

}
