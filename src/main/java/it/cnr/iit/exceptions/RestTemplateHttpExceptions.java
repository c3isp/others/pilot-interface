package it.cnr.iit.exceptions;

import java.io.IOException;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus.Series;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

@Component
public class RestTemplateHttpExceptions implements ResponseErrorHandler {

	@Autowired
	private HttpExceptions exception;

	private static final Logger LOGGER = Logger.getLogger(RestTemplateHttpExceptions.class.getName());

	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		return (response.getStatusCode().series() == Series.CLIENT_ERROR
				|| response.getStatusCode().series() == Series.SERVER_ERROR);
	}

	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		if (response.getStatusCode().is4xxClientError()) {
			exception.setErrorCode(response.getStatusCode());
			exception.setError(true);
			switch (response.getStatusCode()) {
			case BAD_REQUEST:
				exception.setMessage("The input parameter is not in the correct form");
				break;
			case FORBIDDEN:
				exception.setMessage("You are not allowed to perform this action");
				break;
			default:
				exception.setMessage("Something went wrong...");
				break;
			}
		}
	}

}
