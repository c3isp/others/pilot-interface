package it.cnr.iit.exceptions;

public class UploadExceptions extends Exception {

	public UploadExceptions() {
	}

	public UploadExceptions(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UploadExceptions(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UploadExceptions(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}


}
