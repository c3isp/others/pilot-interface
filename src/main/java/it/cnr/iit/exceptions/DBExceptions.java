package it.cnr.iit.exceptions;

public class DBExceptions extends Exception {

	public DBExceptions() {
	}

	public DBExceptions(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DBExceptions(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DBExceptions(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}


}
