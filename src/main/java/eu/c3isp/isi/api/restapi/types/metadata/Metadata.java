package eu.c3isp.isi.api.restapi.types.metadata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Pojo class for metadata to be stored in the dpo
 *
 * @author antonio
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Metadata {

	@JsonProperty("id")
	private String id;
	@JsonProperty("event_type")
	private String eventType;
	@JsonProperty("start_time")
	private String startTime;
	@JsonProperty("end_time")
	private String endTime;
	@JsonProperty("organization")
	private String organization;
	@JsonProperty("dsa_id")
	private String dsaId;
	@JsonProperty("subject_id")
	private String subjectId;
	@JsonProperty("action_id")
	private String actionId;
	@JsonProperty("access_purpose")
	private String accessPurpose;
	@JsonProperty("stixed")
	private String stixed;

	public Metadata() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDsaId() {
		return dsaId;
	}

	public void setDsaId(String dsaId) {
		this.dsaId = dsaId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public String getActionId() {
		return actionId;
	}

	public void setActionId(String actionId) {
		this.actionId = actionId;
	}

	public String getAccessPurpose() {
		return accessPurpose;
	}

	public void setAccessPurpose(String accessPurpose) {
		this.accessPurpose = accessPurpose;
	}

	public String getStixed() {
		return this.stixed;
	}

	public void setStixed(String stixed) {
		this.stixed = stixed;
	}

	@Override
	public String toString() {
		try {
			String string = new ObjectMapper().writeValueAsString(this);
			System.out.println(string);
			string = string.replaceAll("\":\"", "\" : \"");
			return string;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
