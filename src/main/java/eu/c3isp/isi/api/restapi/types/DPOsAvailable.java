package eu.c3isp.isi.api.restapi.types;

import java.util.ArrayList;

/**
 * This is the POJO returned by the searchDPO API
 * <p>
 * This object is formed by a list of DPO characteristics
 * </p>
 * 
 * @author antonio
 *
 */
public class DPOsAvailable {
	ArrayList<DPOCharacteristic> dpos;

	public DPOsAvailable() {
		dpos = new ArrayList<>();
	}
}
