package eu.c3isp.isi.api.restapi.types.xacml;

import java.io.IOException;
import java.io.StringWriter;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "RequestContainer", description = "Mapping request attributes in a XACML-like fashon")
// @XmlRootElement(name ="Request")
public class RequestContainer {

	// @XmlElement(name = "Request", nillable = false, required = true)
	@JsonProperty("Request")
	RequestElement request;

	@ApiModelProperty(allowEmptyValue = false, required = true, name = "Request")
	public RequestElement getRequest() {
		return request;
	}

	public void setRequest(RequestElement requestElement) {
		this.request = requestElement;
	}

	public RequestContainer() {

		request = new RequestElement();

	}

	/**
	 * serches for an attributeValue in a {@link RequestContainer}
	 * 
	 * @param attributeName
	 * @return a String with the attributeValue or <code>null</code> otherwise
	 */
	public String search(String attributeName) {
		if (request == null) {
			return null;
		}
		if (request.getAttributes().size() < 1) {
			return null;
		}
		for (RequestAttributes attr : request.getAttributes()) {
			if (attr.getAttributeId().compareToIgnoreCase(attributeName) == 0) {
				return attr.getValue();
			}
		}
		return null;
	}

	@Override
	public String toString() {
		StringWriter toReturn = new StringWriter();
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(toReturn, this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return toReturn.toString();
	}

}