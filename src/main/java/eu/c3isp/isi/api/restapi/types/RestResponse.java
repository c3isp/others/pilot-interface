package eu.c3isp.isi.api.restapi.types;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "RestResponse", description = "Required metadata to complete the operation")
public class RestResponse {

	/**
	 * (Required)
	 * 
	 */

	private String sessionId;

	// @ApiModelProperty(required=true, value="additionalProperties to be used for
	// the operation", allowEmptyValue=true)
	private Map<String, String> additionalProperties;

	@ApiModelProperty(required = false, value = "session_ID to be used for the operation", allowEmptyValue = false)
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String dsaID) {
		this.sessionId = dsaID;
	}

	public RestResponse withSessionId(String sessionId) {
		this.sessionId = sessionId;
		return this;
	}

	// @XmlElementWrapper(name = "additionalProperties")
	// @XmlElement(name = "property")
	@ApiModelProperty(value = "an array of arbitrary elements ")
	public Map<String, String> getAdditionalProperties() {
		return this.additionalProperties;
	}

	public void setAdditionalProperties(Map<String, String> additonalProperties) {
		this.additionalProperties = additonalProperties;
	}

	@Override
	public String toString() {

		StringWriter toReturn = new StringWriter();
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(toReturn, this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return toReturn.toString();

	}

}