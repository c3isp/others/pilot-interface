package eu.c3isp.isi.api.restapi.types.xacml;

import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "BaseCategory", description = "Base class for AccessSubject, Action, Resource, Environment categories")
/* abstract */ class BaseCategory {

	// @XmlElement(name = "Attribute", nillable = true, required = false)

	@ApiModelProperty(allowEmptyValue = true, required = true, value = "Attribute")
	List<RequestAttributes> attribute;

	public BaseCategory() {
		attribute = new LinkedList<RequestAttributes>();
	}

	@JsonProperty("Attribute")
	public List<RequestAttributes> getAttributes() {
		return attribute;
	}

	public void setAttributes(List<RequestAttributes> attributes) {
		this.attribute = attributes;
	}
}