package eu.c3isp.isi.api.restapi.types.xacml;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "RequestAttributes", description = "Container for attributes in a XACML-like fashon")
// @XmlRootElement(name ="Request")
public class RequestAttributes {

	@XmlElement(name = "AttributeId", nillable = false, required = true)
	@JsonProperty(value = "AttributeId", required = true)
	String attributeId;
	@XmlElement(name = "Value", nillable = false, required = true)
	@JsonProperty(value = "Value", required = true)
	String value;
	@XmlElement(name = "DataType", nillable = false, required = true)
	@JsonProperty(value = "DataType", required = true)
	String datatype;

	public RequestAttributes() {
		attributeId = "";
		value = "";
		datatype = "string";

	}

	public String getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(String attributeId) {
		this.attributeId = attributeId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDatatype() {
		return datatype;
	}

	public void setDatatype(String datatype) {
		this.datatype = "string";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof RequestAttributes) {
			RequestAttributes input = (RequestAttributes) obj;

			return input.attributeId.equals(attributeId) && input.value.equals(value)
					&& input.datatype.equals(datatype);

		} else {
			return super.equals(obj);
		}
	}
}