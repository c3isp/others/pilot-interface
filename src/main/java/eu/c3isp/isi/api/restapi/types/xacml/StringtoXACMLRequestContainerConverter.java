package eu.c3isp.isi.api.restapi.types.xacml;

import java.io.IOException;

import org.springframework.core.convert.converter.Converter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;

public class StringtoXACMLRequestContainerConverter implements Converter<String, RequestContainer> {

	// private final StringtoXACMLConverter specialtyService;

	// public ShortNameToFaculty(SpecialtyService specialtyService)
	// {
	// this.StringtoXACMLConverter = specialtyService;
	// }

	@Override
	public RequestContainer convert(String text) {
		// text = text.toLowerCase();
		RequestContainer toReturn = new RequestContainer();
		ObjectMapper mapper = new ObjectMapper();

		try {

			JsonNode inputJson = mapper.readTree(text);
			// JsonNode tenantAttributeNode =
			// inputJson.path("Request").path("AccessSubject").path("Attribute");

			// JsonNode tenantAttributeNode = inputJson.path("Request");

			JsonNode innerRequest = inputJson.at("/Request/");

			if (innerRequest.isMissingNode() != false) {
				toReturn = mapper.readValue(text, RequestContainer.class);
				// toReturn = mapper.readValues(inputJson.at("/request/"),
				// RequestContainer.class);
				// toReturn =
				// mapper.readValue(mapper.writeValueAsString(inputJson.at("/Request/")),
				// RequestContainer.class);
			}

		} catch (UnrecognizedPropertyException unkProp) {
			// TODO Auto-generated catch block
			unkProp.printStackTrace();
			return null;
		}

		catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (JsonParseException ex) {
			ex.printStackTrace();
			return null;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return null;
		}

		return toReturn;
	}

}