package eu.c3isp.isi.api.restapi.types;

import io.swagger.annotations.ApiModelProperty;

public class Criterion {

	String attribute; // name of attribute
	String operator; // logic operator [eq, ne, ...]
	String value; // value of attribute

	@ApiModelProperty(required = true, value = "attribute to consider", allowEmptyValue = false)
	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	@ApiModelProperty(required = true, value = "operator to use", allowEmptyValue = false)
	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	@ApiModelProperty(required = true, value = "value to use as reference for the operator", allowEmptyValue = false)
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}