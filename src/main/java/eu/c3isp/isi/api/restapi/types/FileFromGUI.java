package eu.c3isp.isi.api.restapi.types;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

public class FileFromGUI implements MultipartFile {

	private String filename;
	private byte[] multipart;

	public FileFromGUI() {
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public byte[] getMultipart() {
		return multipart;
	}

	public void setMultipart(byte[] multipart) {
		this.multipart = multipart;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getOriginalFilename() {
		return filename;
	}

	public void setOriginalFilename(String filename) {
		this.filename = filename;
	}

	@Override
	public String getContentType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isEmpty() {
		if (getSize() == 0)
			return true;
		return false;
	}

	@Override
	public long getSize() {
		return multipart.length;
	}

	@Override
	public byte[] getBytes() throws IOException {
		return multipart;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return new ByteArrayInputStream(multipart);
	}

	@Override
	public void transferTo(File dest) throws IOException, IllegalStateException {
		// TODO Auto-generated method stub
	}
}
