package eu.c3isp.isi.api.restapi.types;

import java.util.HashMap;

/**
 * This is the POJO representing the characteristics of a DPO
 * 
 * @author antonio
 *
 */
public class DPOCharacteristic {
	String id;

	// this field is null if longResult flag is false
	HashMap<String, String> additionalProperties;

	public DPOCharacteristic() {
		additionalProperties = new HashMap<>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public HashMap<String, String> getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(HashMap<String, String> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

}
