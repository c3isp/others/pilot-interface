package eu.c3isp.isi.api.restapi.types.metadata;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FormMetadata extends Metadata {

//	@JsonProperty("localisi")
//	private String localISI;
	HashMap<String, String> additionalProperties;

	public FormMetadata() {

	}

	@Override
	public String toString() {
		try {
			String string = new ObjectMapper().writeValueAsString(this);
			System.out.println(string);
			string = string.replaceAll("\":\"", "\" : \"");
			return string;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

//	public String getLocalISI() {
//		return localISI;
//	}

//	public void setLocalISI(String localISI) {
//		this.localISI = localISI;
//	}

	public Metadata toMetadata() {
		Metadata metadata = new Metadata();
		metadata.setDsaId(getDsaId().replace(".xml", ""));
		metadata.setEndTime(getEndTime());
		metadata.setStartTime(getStartTime());
		metadata.setId(getId());
		metadata.setOrganization(getOrganization());
		metadata.setEventType(getEventType());
		metadata.setSubjectId(getSubjectId());
		return metadata;
	}

}
