package eu.c3isp.isi.api.restapi.types;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import it.cnr.iit.common.types.Metadata;

/**
 * This is the only object accepted by IAI-API.
 * <p>
 * As attributes this object must have:
 * <ol>
 * <li>METADATA: these are additional informations that can be put by the user
 * </li>
 * <li>SERVICE_NAME: this is the name of the service requested by the user, it
 * is considered only in the case in which the user calls the general API</li>
 * <li>SERVICE_PARAMS: these are the params that are required by the analytics
 * in order to run. In fact we want to give the user the possibility of refining
 * their analytics</li>
 * <li>SEARCH_CRITERIA: this is a JSON object already defined in the ISI-API and
 * its aim is to force the user to establish criteria by which the IS-API can
 * find the data the user requires</li>
 * <li>DPO_IDS: this is the list of additional dpoids to be considered in the
 * analytic, it can be used instead of or with the searchCriteria</li>
 * </ol>
 * </p>
 *
 * @author antonio
 *
 */
public class IAIParam {
	@JsonProperty(value = "metadata", required = true)
	private Metadata metadata; // access purpose inside this
	@JsonProperty(value = "serviceParams", required = true)
	private HashMap<String, String> serviceParams = new HashMap<>();
	@JsonProperty(value = "serviceName", required = true)
	private String serviceName; // analytic name
	@JsonProperty(value = "searchCriteria", required = true)
	private SearchParams searchCriteria;
	@JsonProperty(value = "subjectId", required = false, defaultValue = "user")
	private String subjectId;
	@JsonProperty(value = "additionalAttribute", required = false)
	private Map<String, String> additionalAttribute;

	// private ArrayList<String> dpoIds;

	// public ArrayList<String> getDpoIds() {
	// return dpoIds;
	// }
	//
	// public void setDpoIds(ArrayList<String> dpoIds) {
	// this.dpoIds = dpoIds;
	// }

	public Metadata getMetadata() {
		return metadata;
	}

	public void setMetadata(Metadata metadata) {
		this.metadata = metadata;
	}

	public HashMap<String, String> getServiceParams() {
		return serviceParams;
	}

	public void setServiceParams(HashMap<String, String> serviceParams) {
		this.serviceParams = serviceParams;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public SearchParams getSearchCriteria() {
		return searchCriteria;
	}

	public void setSearchCriteria(SearchParams searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public Map<String, String> getAdditionalAttribute() {
		return additionalAttribute;
	}

	public void setAdditionalAttribute(Map<String, String> additionalAttribute) {
		this.additionalAttribute = additionalAttribute;
	}

}
