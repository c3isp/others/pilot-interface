package eu.c3isp.isi.api.restapi.types;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "RestMetadata", description = "Required metadata to complete the operation")
@XmlRootElement(name = "RestMetadata")
public class RestMetadata {

	// private String sessionId;

	private Map<String, String> additionalProperties;
	// private List<KeyValueElement> additionalProperties;

	@ApiModelProperty(required = false, value = "DSA_ID to be used for the operation")
	private String dsaId;

	/**
	 * Methods
	 */
	// @XmlElement(name = "sessionId")
	// @ApiModelProperty(required=false, value = "Session_ID to be used for the
	// operation", allowEmptyValue=true)
	// public String getSessionID() {
	// return sessionId;
	// }
	//
	// public void setSessionID(String sessionID) {
	// this.sessionId = sessionID;
	// }
	//
	// public RestMetadata withSessionID(String sessionID) {
	// this.sessionId = sessionID;
	// return this;
	// }

	// @XmlElementWrapper(name = "additionalProperties")
	// @XmlElement()
	@ApiModelProperty(required = true, value = "additionalProperties to be used for the operation", allowEmptyValue = true)
	public Map<String, String> getAdditionalProperties() {
		return this.additionalProperties;
	}

	public void setAdditionalProperties(Map<String, String> additonalProperties) {
		this.additionalProperties = additonalProperties;
	}

	@Override
	public String toString() {

		StringWriter toReturn = new StringWriter();
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(toReturn, this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return toReturn.toString();

	}

	public String getDsaId() {
		return dsaId;
	}

	public void setDsaId(String dsaId) {
		this.dsaId = dsaId;
	}

}