package eu.c3isp.isi.api.restapi.types;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModelProperty;

//Crazy! if uncommented, the two classes get "merged" in the swagger view!
//@ApiModel(value="RestMetadata",description="Required metadata to complete the operation")
//@XmlRootElement(name ="RestMetadata")
public class SearchParams {

	@JsonProperty("combining_rule")
	private String combiningRule;

	private List<Criterion> criteria;

	/**
	 * Methods
	 */

	@JsonProperty("combining_rule")
	@ApiModelProperty(required = true, value = "combining rule used to compute the specified criteria", allowEmptyValue = false)
	public String getCombiningRule() {
		return combiningRule;
	}

	@JsonProperty("combining_rule")
	public void setCombiningRule(String combiningRule) {
		this.combiningRule = combiningRule;
	}

	@ApiModelProperty(required = true, value = "List of criteria for the search operation", allowEmptyValue = true)
	public List<Criterion> getCriteria() {
		return criteria;
	}

	public void setCriteria(List<Criterion> criteria) {
		this.criteria = criteria;
	}

	@Override
	public String toString() {

		StringWriter toReturn = new StringWriter();
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(toReturn, this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return toReturn.toString();

	}

	public static final SearchParams createEmpty() {
		SearchParams searchParams = new SearchParams();
		searchParams.combiningRule = "or";
		searchParams.criteria = new ArrayList<Criterion>();
		return searchParams;
	}
}
