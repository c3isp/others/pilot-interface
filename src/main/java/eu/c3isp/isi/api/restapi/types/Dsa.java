package eu.c3isp.isi.api.restapi.types;

import java.util.ArrayList;

public class Dsa {
	private ArrayList<String> partyIDs;
	private String purpose;
	private String description;
	private ArrayList<String> partyNames;
	private String id;
	private String status;
	private String title;

	public Dsa() {
	}

	public ArrayList<String> getPartyIDs() {
		return partyIDs;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<String> getPartyNames() {
		return partyNames;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
