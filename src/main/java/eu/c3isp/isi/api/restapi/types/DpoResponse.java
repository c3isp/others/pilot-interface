package eu.c3isp.isi.api.restapi.types;

import java.util.HashMap;

/**
 * This is the response provided by the IAI api.
 * <p>
 * Basically after the evaluation of the IAI, the result will be stored inside a
 * DPO in the ISI. In order to retrieve this dpo for sure the user requires the
 * id of the dpo. Although more informations can be required, that's why we
 * decided to use an object and not a simple string,
 * </p>
 * 
 * @author antonio
 *
 */
public class DpoResponse {
	private String dpoId;
	private HashMap<String, String> additonalProperties = new HashMap<>();

	public String getDpoId() {
		return dpoId;
	}

	public void setDpoId(String dpoId) {
		this.dpoId = dpoId;
	}

	public HashMap<String, String> getAdditonalProperties() {
		return additonalProperties;
	}

	public void setAdditonalProperties(HashMap<String, String> additonalProperties) {
		this.additonalProperties = additonalProperties;
	}

}
