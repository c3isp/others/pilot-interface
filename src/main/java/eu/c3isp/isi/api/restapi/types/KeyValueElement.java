package eu.c3isp.isi.api.restapi.types;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "KeyValue", description = "a simple key-value tuple")
public class KeyValueElement {

	@ApiModelProperty(required = true, value = "a touple key", allowEmptyValue = true)
	private String key;
	@ApiModelProperty(required = true, value = "a touple value", allowEmptyValue = true)
	private String value;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}