package eu.c3isp.isi.api.restapi.types.xacml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "RequestElement", description = "Holds the request elements")
@XmlRootElement(name = "Request")
public class RequestElement {

	// @XmlElement(name = "AccessSubject", nillable = false, required = true)
	// BaseCategory accesssubject;
	// @XmlElement(name = "Action", nillable = false, required = true)
	// @ApiModelProperty(allowEmptyValue = false, required = true, position = 2)
	// Action action;
	// @JsonProperty("Action")
	// BaseCategory action;
	// @ApiModelProperty(allowEmptyValue = false, required = true, position = 3)
	// Resource resource;
	// @XmlElement(name = "Resource", nillable = false, required = true)
	// @JsonProperty("Resource")
	// BaseCategory resource;
	// @XmlElement(name = "Environment", nillable = false, required = true)
	// @ApiModelProperty(allowEmptyValue = false, required = true, position = 4)
	// @JsonProperty("Environment")
	// Environment environment;
	// BaseCategory environment;

	@ApiModelProperty(allowEmptyValue = true, required = true, value = "Attribute")
	List<RequestAttributes> attribute = new ArrayList<>();

	@JsonProperty("Attribute")
	public List<RequestAttributes> getAttributes() {
		return attribute;
	}

	public void setAttributes(List<RequestAttributes> attributes) {
		this.attribute = attributes;
	}

	public RequestElement() {
		// accesssubject = new BaseCategory();
		// action = new BaseCategory();
		// resource = new BaseCategory();
		// environment = new BaseCategory();
	}

	// @ApiModelProperty(allowEmptyValue = false, required = true, position = 1)
	// @JsonProperty("AccessSubject")
	// public BaseCategory getAccessSubject() {
	// return accesssubject;
	// }

	// public void setAccessSubject(AccessSubject accessSubject) {
	// public void setAccessSubject(BaseCategory accessSubject) {
	// this.accesssubject = accessSubject;
	// }

	// public Action getAction() {
	// @ApiModelProperty(allowEmptyValue = false, required = true, position = 2)
	// @JsonProperty("Action")
	// public BaseCategory getAction() {
	// return action;
	// }

	// public void setAction(/* Action */ BaseCategory action) {
	// this.action = action;
	// }

	// public Resource getResource() {
	// @ApiModelProperty(allowEmptyValue = false, required = true, position = 3)
	// @XmlElement(name = "Resource", nillable = false, required = true)
	// @JsonProperty("Resource")
	// public BaseCategory getResource() {
	// return resource;
	// }

	// public void setResource(Resource resource) {
	// public void setResource(BaseCategory resource) {
	// this.resource = resource;
	// }

	// public Environment getEnvironment() {
	// @XmlElement(name = "Environment", nillable = false, required = true)
	// @ApiModelProperty(allowEmptyValue = false, required = true, position = 4)
	// @JsonProperty("Environment")
	// public BaseCategory getEnvironment() {
	// return environment;
	// }

	// public void setEnvironment(Environment environment) {
	// public void setEnvironment(BaseCategory environment) {
	// this.environment = environment;
	// }

}