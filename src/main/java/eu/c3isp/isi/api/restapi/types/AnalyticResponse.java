package eu.c3isp.isi.api.restapi.types;

public class AnalyticResponse {

	private String value = "";
	private String message = "";

	public AnalyticResponse() {
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
