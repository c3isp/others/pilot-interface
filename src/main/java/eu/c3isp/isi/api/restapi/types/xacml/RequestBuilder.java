package eu.c3isp.isi.api.restapi.types.xacml;

import java.util.Map;
import java.util.logging.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.c3isp.isi.api.restapi.types.metadata.MetadataForCreate;
import it.cnr.iit.common.attributes.AttributeIds;
import it.cnr.iit.common.attributes.AttributesUtility;
import it.cnr.iit.common.reject.Reject;
import it.cnr.iit.common.types.Metadata;
import it.cnr.iit.xacml.attribute.AdditionalAttribute;

/**
 * Builder class for the request container
 * <p>
 *
 * </p>
 *
 * @author antonio
 *
 */
public class RequestBuilder {

	private RequestContainer requestContainer;
	private Logger LOGGER = Logger.getLogger(RequestBuilder.class.getName());

	public RequestBuilder() {
		requestContainer = new RequestContainer();
	}

	public void parseMetadata(Metadata metadata) throws JsonProcessingException {
		AdditionalAttribute[] times = AttributesUtility.parseTime(metadata.getStartTime(), metadata.getEndTime());
		addAttribute(AttributeIds.START_DATE, "date", times[0].getValue());
		addAttribute(AttributeIds.START_TIME, "time", times[1].getValue());
		addAttribute(AttributeIds.END_DATE, "date", times[2].getValue());
		addAttribute(AttributeIds.END_TIME, "time", times[3].getValue());
		addAttribute(AttributeIds.DSA_ID, "string", metadata.getDsaId());
		addAttribute(AttributeIds.RESOURCE_TYPE, "string", metadata.getEventType());
		addAttribute(AttributeIds.RESOURCE_OWNER, "string", metadata.getOrganization());
		addAttribute(AttributeIds.EXTENSION, "string", metadata.getExtension());
	}

	public void setRequestAttributes(Map<String, String> attributeMap) {
		addAttribute(AttributeIds.SUBJECT_ID, "string", attributeMap.get(AttributeIds.SUBJECT_ID));
		addAttribute(AttributeIds.SUBJECT_ORGANIZATION, "string", attributeMap.get(AttributeIds.SUBJECT_ORGANIZATION));
		addAttribute(AttributeIds.ACTION_ID, "string", attributeMap.get(AttributeIds.ACTION_ID));
		addAttribute(AttributeIds.ACCESS_PURPOSE, "string", attributeMap.get(AttributeIds.ACCESS_PURPOSE));
	}

	private void addAttribute(String attributeId, String dataType, String value) {
		if (value == null || value.isEmpty()) {
			return;
		}

		System.out.println("value=" + value);
		Reject.ifBlank(attributeId);
		Reject.ifBlank(dataType);
		RequestAttributes requestAttributes = new RequestAttributes();
		requestAttributes.setAttributeId(attributeId);
		requestAttributes.setDatatype(dataType);
		requestAttributes.setValue(value);
		requestContainer.request.attribute.add(requestAttributes);
	}

	private String generateValidDpoMetadataForCreate(Metadata metadata) throws JsonProcessingException {

		MetadataForCreate metadataForCreate = new MetadataForCreate();
		metadataForCreate.setDsaId(metadata.getDsaId());
		metadataForCreate.setEndTime(metadata.getEndTime());
		metadataForCreate.setEventType(metadata.getEventType());
//		metadataForCreate.setId(metadata.getId());
		metadataForCreate.setOrganization(metadata.getOrganization());
		metadataForCreate.setStartTime(metadata.getStartTime());

		return new ObjectMapper().writeValueAsString(metadataForCreate);

	}

	public RequestContainer build(Metadata metadata, Map<String, String> attributeMap) throws JsonProcessingException {
		parseMetadata(metadata);
		setRequestAttributes(attributeMap);
		return requestContainer;
	}

	public RequestContainer build(Map<String, String> attributeMap) throws JsonProcessingException {
		setRequestAttributes(attributeMap);
		return requestContainer;
	}

}