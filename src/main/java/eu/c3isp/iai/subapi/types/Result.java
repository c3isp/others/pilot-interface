package eu.c3isp.iai.subapi.types;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Result {
	@JsonProperty("sessionId")
	private String sessionId;
	@JsonProperty("additionalProperties")
	private HashMap<String, String> additionalProperties = new HashMap<>();

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public HashMap<String, String> getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(HashMap<String, String> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

}
