package eu.c3isp.iai.subapi.types;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This is the response provided by the IAI api.
 * <p>
 * Basically after the evaluation of the IAI, the result will be stored inside a
 * DPO in the ISI. In order to retrieve this dpo for sure the user requires the
 * id of the dpo. Although more informations can be required, that's why we
 * decided to use an object and not a simple string,
 * </p>
 * 
 * @author antonio
 *
 */
public class DpoResponse {
	@JsonProperty("result")
	private String result;

	private final static String DPO_ID = "dposId";

	public DpoResponse() {

	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}
