package eu.c3isp.iai.subapi.types;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties({ "utility" })
public class DpoResponses {
	public static final String ERROR_KEY = "error";

	@JsonProperty("result")
	private List<Result> results = new ArrayList<Result>();

	public List<Result> getResults() {
		return results;
	}

	public void setResults(List<Result> results) {
		this.results = results;
	}

	@JsonIgnore
	public String getDpoId() {
		String dpoId = null;
		Result result = results.get(0);
		if (result.getAdditionalProperties() != null) {
			dpoId = result.getAdditionalProperties().get("dposId");
		}

		return dpoId;
	}

}
