package eu.c3isp.iai.subapi.types;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.security.core.Authentication;

import eu.c3isp.isi.api.restapi.types.xacml.RequestAttributes;
import eu.c3isp.isi.api.restapi.types.xacml.RequestContainer;
import eu.c3isp.isi.api.restapi.types.xacml.RequestElement;

/**
 * This class contains infos that the IAI API has to pass to the ISI.
 * <p>
 * Such infos are:
 * <ol>
 * <li>Username of the user that is logged in</li>
 * <li>Type of authentication used by the user</li>
 * <li>Analytic that has been invoked</li>
 * <li>ACCESS_PURPOSE (passed by the user in the metadata of the API)</li>
 * <li>AGGREGATION_OPERATION (passed by the user in the metadata of the
 * API)</li>
 * </ol>
 * </p>
 *
 * @author antonio
 *
 */
public class AnalyticMetadata {
	private String username;
	private String authenticationType;
	private String action;
	private String accessPurpose;
	private String aggregationOperation;

	// FACTORY METHODS

	public static AnalyticMetadata createAnalyticMetadataForChart(Authentication authentication) {
		AnalyticMetadata analyticMetadata = new AnalyticMetadata();
		analyticMetadata.accessPurpose = "generic";
		analyticMetadata.aggregationOperation = "empty";
		analyticMetadata.authenticationType = "low";
		analyticMetadata.username = authentication.getName();
		analyticMetadata.action = "read";
		return analyticMetadata;
	}

	public static AnalyticMetadata createAnalyticForMove(String username, String accessPurpose) {
		AnalyticMetadata analyticMetadata = new AnalyticMetadata();
		analyticMetadata.accessPurpose = accessPurpose;
		analyticMetadata.aggregationOperation = "empty";
		analyticMetadata.authenticationType = "low";
		analyticMetadata.username = username;
		analyticMetadata.action = "Move";
		return analyticMetadata;
	}

	public static AnalyticMetadata createAnalyticMetadataForRead(Authentication authentication) {
		AnalyticMetadata analyticMetadata = new AnalyticMetadata();
		analyticMetadata.accessPurpose = "generic";
		analyticMetadata.aggregationOperation = "empty";
		analyticMetadata.authenticationType = "low";
		analyticMetadata.username = authentication.getName();
		analyticMetadata.action = "read";
		return analyticMetadata;
	}

	public static AnalyticMetadata createAnalyticMetadataForIAI(Authentication authentication, String action,
			HashMap<String, String> metadata) {
		AnalyticMetadata analyticMetadata = new AnalyticMetadata();
		if (metadata.containsKey("accessPurpose")) {
			analyticMetadata.accessPurpose = metadata.get("accessPurpose");
		} else {
			analyticMetadata.accessPurpose = "generic";
		}
		if (metadata.containsKey("aggregationOperation")) {
			analyticMetadata.aggregationOperation = metadata.get("aggregationOperation");
		} else {
			analyticMetadata.aggregationOperation = "empty";
		}
		analyticMetadata.authenticationType = "low";
		analyticMetadata.username = authentication.getName();
		System.out.println(analyticMetadata.username + "\t" + analyticMetadata.username.length());
		if (authentication.getName().equals("user")) {
			analyticMetadata.username = (metadata.get("subjectId") == null) ? "user" : metadata.get("subjectId");
		}
		analyticMetadata.action = action;
		return analyticMetadata;
	}

	// GETTERS AND SETTERS

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAuthenticationType() {
		return authenticationType;
	}

	public void setAuthenticationType(String authenticationType) {
		this.authenticationType = authenticationType;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getAggregationOperation() {
		return aggregationOperation;
	}

	public void setAggregationOperation(String aggregationOperation) {
		this.aggregationOperation = aggregationOperation;
	}

	public String getAccessPurpose() {
		return accessPurpose;
	}

	public void setAccessPurpose(String accessPurpose) {
		this.accessPurpose = accessPurpose;
	}

	public RequestContainer convertToRequestContainer() {
		RequestContainer requestContainer = new RequestContainer();
		RequestElement requestElement = new RequestElement();
		ArrayList<RequestAttributes> attributes = new ArrayList<>();
		attributes.add(createUsernameRequestAttribute());
		attributes.add(createAuthenticationTypeRequestAttribute());
		attributes.add(createActionRequestAttribute());
		attributes.add(createAccessPurposeRequestAttribute());
		attributes.add(createAggregationOperationRequestAttribute());
		requestElement.setAttributes(attributes);
		requestContainer.setRequest(requestElement);
		return requestContainer;
	}

	private RequestAttributes createUsernameRequestAttribute() {
		if (username == null || username.length() < 1) {
			username = "user";
		}
		RequestAttributes reqAttributes = new RequestAttributes();
		reqAttributes.setAttributeId("urn:oasis:names:tc:xacml:1.0:subject:subject-id");
		reqAttributes.setDatatype("string");
		reqAttributes.setValue(username);
		return reqAttributes;

	}

	private RequestAttributes createAuthenticationTypeRequestAttribute() {
		if (authenticationType == null || authenticationType.length() < 1) {
			authenticationType = "LOW";
		}
		RequestAttributes reqAttributes = new RequestAttributes();
		reqAttributes.setAttributeId("urn:oasis:names:tc:xacml:1.0:subject:authentication-type");
		reqAttributes.setDatatype("string");
		reqAttributes.setValue(authenticationType);
		return reqAttributes;
	}

	private RequestAttributes createActionRequestAttribute() {
		if (action == null || action.length() < 1) {
			action = "read";
		}
		RequestAttributes reqAttributes = new RequestAttributes();
		reqAttributes.setAttributeId("urn:oasis:names:tc:xacml:1.0:action:action-id");
		reqAttributes.setDatatype("string");
		reqAttributes.setValue(action);
		return reqAttributes;
	}

	private RequestAttributes createAccessPurposeRequestAttribute() {
		if (accessPurpose == null || accessPurpose.length() < 1) {
			accessPurpose = "generic";
		}
		RequestAttributes reqAttributes = new RequestAttributes();
		reqAttributes.setAttributeId("urn:oasis:names:tc:xacml:3.0:subject:access-purpose");
		reqAttributes.setDatatype("string");
		reqAttributes.setValue(accessPurpose);
		return reqAttributes;
	}

	private RequestAttributes createAggregationOperationRequestAttribute() {
		if (aggregationOperation == null || aggregationOperation.length() < 1) {
			aggregationOperation = "empty";
		}
		RequestAttributes reqAttributes = new RequestAttributes();
		reqAttributes.setAttributeId("ns:c3isp:aggregation-operation");
		reqAttributes.setDatatype("string");
		reqAttributes.setValue(aggregationOperation);
		return reqAttributes;
	}

}
